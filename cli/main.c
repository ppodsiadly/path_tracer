#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <wdf_parser.h>
#include <world.h>
#include <console.h>
#include <time.h>

static void usage()
{
    printf("Usage:\n");
    printf("\trender_cli world_file.wdf WxHxN output_file.bmp\n");
    printf("\tExample: renders cornell_box.wdf to 1280 on 800 pixels with 128 rays per pixel\n");
    printf("\t\trender_cli cornell_box.wdf 1280x800x128 result.bmp\n");
}

typedef struct
{
    int image_w;
    int image_h;
    int num_rays;
} render_params_t;

static bool parse_render_params(const char* str, render_params_t* params)
{
    if (sscanf(str, "%dx%dx%d", &params->image_w, &params->image_h, &params->num_rays) != 3)
    {
        return false;
    }

    return params->image_w > 0 && params->image_h > 0 && params->num_rays > 0;
}

static void progress_callback(uint32_t num_done, uint32_t total, void* user_param)
{
    const cursor_pos_t* pos = (const cursor_pos_t*)user_param;

	const double progress = 100.0 * (double)num_done / total;

	console_set_cursor_pos(pos->row, pos->column);
	console_write("Rendering: %.2lf%% done", progress);
}

int main(int argc, char** argv)
{
    if (argc != 4)
    {
        usage();
        return 1;
    }

    const char* wdf_file = argv[1];
    const char* render_params_str = argv[2];
    const char* bmp_file = argv[3];

    render_params_t render_params;
    if (!parse_render_params(render_params_str, &render_params))
    {
        printf("Error: incorrect render parameters!\n\n");
        usage();
        return 1;
    }

    const clock_t scene_build_start = clock();

    wdf_node_t* world_desc = wdf_parse(wdf_file);
    if (!world_desc)
    {
        printf("Error: failed to load WDF file!\n\n");
        usage();
        return 1;
    }

    camera_t* camera = wdf_create_camera(world_desc);
    if (!camera)
    {
        printf("Error failed to create camera from the WDF file!\n\n");
        usage();
        return 1;
    }

    world_t* world = wdf_create_world(world_desc);

    if (!world)
    {
        printf("Error: failed to build world from the WDF file!\n\n");
        usage();
        return 1;
    }

    wdf_free(world_desc);

    const clock_t scene_build_end = clock();
    printf("The scene was built in %.2f s\n", ((double)scene_build_end - scene_build_start) / CLOCKS_PER_SEC);

    render_buf_t* render_buf = render_buf_create((uint32_t)render_params.image_w, (uint32_t)render_params.image_h);
    if (!render_buf)
    {
        world_free(world);
        printf("Error: could not allocate %dx%d render buffer!\n\n", render_params.image_w, render_params.image_h);
        usage();
        return 1;
    }

    prng_t prng = prng_init(time(NULL));

    const clock_t render_start = clock();

    cursor_pos_t cursor_pos = console_cursor_pos();
    
    progress_callback_t cb;
    cb.callback = &progress_callback;
    cb.user_ptr = &cursor_pos;

    world_pathtrace(world, camera, &prng, render_params.num_rays, render_buf, &cb);

    const clock_t render_end = clock();
    printf("\nThe scene was rendered in %.2f s\n", ((double)render_end - render_start) / CLOCKS_PER_SEC);
    
    camera_free(camera);
    world_free(world);

    image_t* img = render_buf_to_image(render_buf);
    render_buf_free(render_buf);

    if (!bmp_write_image(bmp_file, img))
    {
        image_free(img);
        printf("Error: failed to write the image to %s!\n\n", bmp_file);
        usage();
        return 1;
    }

    image_free(img);

    printf("Done.\n");

    return 0;
}
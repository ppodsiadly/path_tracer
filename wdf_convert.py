import copy
import xml.etree
from xml.etree.ElementTree import ElementTree, Element
import xml.dom.minidom
import math
import sys
import os.path

class WdfNode:
    def __init__(self, name = None, value = None):
        self.parent = None
        self.indent = 0
        self.name = name
        self.value = value
        self.children = []

    def require_name(self, name):
        if self.name != name:
            raise Exception("expected node with name = \"{}\"".format(name))

    def child_by_name(self, name):
        for child in self.children:
            if child.name == name:
                return child
        
        return None

    def write(self, stream, indent=0):
        if self.value is None:
            stream.write("{}{}:\n".format(indent * " ", self.name))
        else:
            stream.write("{}{}: {}\n".format(indent * " ", self.name, self.value))

        for child in self.children:
            child.write(stream, indent+4)

class WdfParseCtx:
    def __init__(self, file_path, parent_ctx, base_indent):
        self.parent_ctx = parent_ctx
        self.lines = open(file_path, "r").readlines()
        self.file_path = file_path
        self.base_indent = base_indent

def parse_wdf(file_path):

    parse_ctx = WdfParseCtx(file_path, None, 0)

    root = None
    parent = None

    while not parse_ctx is None:

        while not parse_ctx is None and len(parse_ctx.lines) == 0:
            parse_ctx = parse_ctx.parent_ctx

        if parse_ctx is None:
            return root

        line = parse_ctx.lines[0]
        parse_ctx.lines = parse_ctx.lines[1:]

        stripped_line = line.lstrip()
        if len(stripped_line) == 0:
            continue

        name, value = stripped_line.split(":")
        name = name.strip()
        value = value.strip()

        if len(name) == 0:
            raise Exception("empty name")
            
        if name == "include":
			
            if value is None:
                raise Exception("\"include\" node does not specify file path")

            wdf_dir = os.path.dirname(file_path)
            included_file_path = os.path.join(wdf_dir, value)

            parse_ctx = WdfParseCtx(included_file_path, parse_ctx, parse_ctx.base_indent + 1)

            continue

        node = WdfNode()
        node.indent = (len(line) - len(stripped_line)) + parse_ctx.base_indent * 4
        node.name = name
        if len(value) > 0:
            node.value = value

        while parent and parent.indent >= node.indent:
            parent = parent.parent

        if parent:
            parent.children.append(node)
            node.parent = parent
            parent = node
        elif not root:
            root = node
            parent = node
        else:
            raise Exception("more than one top level node in the file!")
    
    return root

def point_to_mitsuba(name, xyz):
    return Element("point", {
        "name" : name,
        "x" : str(xyz[0]),
        "y" : str(xyz[1]),
        "z" : str(xyz[2])
    })

def rgb_to_mitsuba(name, rgb):
    value = "{}, {}, {}".format(rgb[0], rgb[1], rgb[2])
    return Element("rgb", {"name" : name, "value" : value})

def rgb_from_mitsuba(elem):
    rgb = elem.attrib.get("value", "1,1,1").split(",")
    return [float(rgb[0]), float(rgb[1]), float(rgb[2])]

class Material:
    def __init__(self):
        self.name = None
        self.R = (1, 1, 1)

    @staticmethod
    def from_wdf(wdf_node):
        wdf_node.require_name("material")
        
        mtrl = Material()
        mtrl.name = wdf_node.value

        brdf_type = wdf_node.child_by_name("brdf").value
        if brdf_type == "lambert":
            mtrl.R = parse_vec3(wdf_node.child_by_name("R").value)
        else:
            raise Exception("unsupported brdf \"{}\"!".format(brdf_type))

        return mtrl

    @staticmethod
    def _from_mitsuba_diffuse(bsdf_elem):
        mtrl = Material()
        
        for child in bsdf_elem:
            if child.tag == "rgb" and child.attrib.get("name") == "reflectance":
                mtrl.R = rgb_from_mitsuba(child)

        return mtrl

    @staticmethod
    def from_mitsuba(bsdf_elem):
        mtrl_id = bsdf_elem.attrib.get("id")

        actual_bsdf_elem = bsdf_elem
        while actual_bsdf_elem.attrib.get("type") == "twosided":
            actual_bsdf_elem = actual_bsdf_elem.find("bsdf")
        
        mtrl = None
        bsdf_type = actual_bsdf_elem.attrib.get("type") 

        if bsdf_type == "diffuse":
            mtrl = Material._from_mitsuba_diffuse(actual_bsdf_elem)
        else:
            mtrl = Material()
            mtrl.R = [0.8, 0.0, 0.8]
            print("Unsupported Mitsuba BSDF \"{}\", using diffuse with R=({}, {}, {}) as a placeholder!".format(bsdf_type, *mtrl.R))

        mtrl.name = mtrl_id

        return mtrl

    def to_wdf(self):
        n = WdfNode("material", self.name)
        n.children.append(WdfNode("brdf", "lambert"))
        n.children.append(WdfNode("R", "{} {} {}".format(self.R[0], self.R[1], self.R[2])))
        return n

    def to_mitsuba(self):
        ts_elem = Element("bsdf", {"id" : self.name, "type" : "twosided"})
        
        elem = Element("bsdf", {"type" : "diffuse"})
        elem.append(rgb_to_mitsuba("reflectance", self.R))
        ts_elem.append(elem)

        return ts_elem

    def to_mitsuba_ref(self):
        return Element("ref", {"id" : self.name})

def parse_vec3(s):
    xyz = s.split()
    if len(xyz) != 3:
        raise Exception("could not parse \"{}\" as vec3!".format(s))
    return (float(xyz[0]), float(xyz[1]), float(xyz[2]))

def parse_vec4(s):
    xyzw = s.split()
    if len(xyzw) != 4:
        raise Exception("could not parse \"{}\" as vec4!".format(s))
    return (float(xyzw[0]), float(xyzw[1]), float(xyzw[2]), float(xyzw[3]))

def parse_vec3_node(wdf_node, def_val = (0, 0, 0)):
    if wdf_node is None:
        return copy.copy(def_val)
    else:
        return parse_vec3(wdf_node.value)

class TranslationTform:
    def __init__(self, vector = None):
        if vector is not None:
            self.vector = copy.copy(vector)
        else:
            self.vector = [0, 0, 0]

    @staticmethod
    def from_wdf(node):
        node.require_name("translation")
        
        tr = TranslationTform()
        tr.vector = parse_vec3(node.value)

        return tr

    @staticmethod
    def from_mitsuba(elem):
        tr = TranslationTform()
        tr.vector[0] = float(elem.attrib.get("x", "0"))
        tr.vector[1] = float(elem.attrib.get("y", "0"))
        tr.vector[2] = float(elem.attrib.get("z", "0"))
        return tr

    def to_wdf(self):
        return WdfNode("translation", "{} {} {}".format(*self.vector))

    def to_mitsuba(self):
        return Element("translate", { "x" : str(self.vector[0]), "y" : str(self.vector[1]), "z" : str(self.vector[2])})

class RotationTform:
    def __init__(self):
        self.axis = [1, 0, 0]
        self.angle = 0

    @staticmethod
    def from_wdf(node):
        node.require_name("rotation")
        
        xyzw = parse_vec4(node.value)

        rt = RotationTform()
        rt.axis = (xyzw[0], xyzw[1], xyzw[2])
        rt.angle = xyzw[3]

        return rt

    @staticmethod
    def from_mitsuba(elem):
        rt = RotationTform()
        rt.axis[0] = float(elem.attrib.get("x", "1"))
        rt.axis[1] = float(elem.attrib.get("y", "0"))
        rt.axis[2] = float(elem.attrib.get("z", "0"))
        rt.angle = float(elem.attrib.get("angle", "0"))
        return rt

    def to_wdf(self):
        return WdfNode("rotation", "{} {} {} {}".format(*self.axis, self.angle))

    def to_mitsuba(self):
        return Element("rotate", { "x" : str(self.axis[0]), "y" : str(self.axis[1]), "z" : str(self.axis[2]), "angle" : str(self.angle) })

class ScaleTform:
    def __init__(self):
        self.scale = [0, 0, 0]

    @staticmethod
    def from_wdf(node):
        node.require_name("scale")
        sc = ScaleTform()
        sc.scale = parse_vec3(node.value)
        return sc

    @staticmethod
    def from_mitsuba(elem):
        st = ScaleTform()
        
        if "value" in elem.attrib:
            v = float(elem.attrib["value"])
            st.scale = [v, v, v]
        else:
            st.scale[0] = float(elem.attrib.get("x", "1"))
            st.scale[1] = float(elem.attrib.get("y", "1"))
            st.scale[2] = float(elem.attrib.get("z", "1"))

        return st

    def to_wdf(self):
        return WdfNode("scale", "{} {} {}".format(*self.scale))

    def to_mitsuba(self):
        return Element("scale", { "x" : str(self.scale[0]), "y" : str(self.scale[1]), "z" : str(self.scale[2]) })

class MatrixTform:
    def __init__(self):
        self.matrix = [1, 0, 0, 0,
                       0, 1, 0, 0,
                       0, 0, 1, 0,
                       0, 0, 0, 1]

    @staticmethod
    def from_wdf(wdf_node):
        wdf_node.require_name("matrix")

        values = wdf_node.value.split()
        if len(values) != 16:
            raise Exception("Matrix must have 16 elements!")
        
        mt = MatrixTform()
        for i, value_str in enumerate(values):
            mt.matrix[i] = float(value_str)

        return mt
    
    @staticmethod
    def from_mitsuba(elem):
        values = elem.attrib["value"].split()

        if len(values) != 16:
            raise Exception("Matrix must have 16 elements!")

        mt = MatrixTform()
        for i, value_str in enumerate(values):
            mt.matrix[i] = float(value_str)

        return mt

    def to_wdf(self):
        return WdfNode("matrix", " ".join(str(v) for v in self.matrix))

    def to_mitsuba(self):
        return Element("matrix", {"value" : " ".join(str(v) for v in self.matrix)})

class Transform:
    def __init__(self):
        self.transforms = []

    @staticmethod
    def from_wdf(node):
        if node is None:
            return Transform()

        node.require_name("transform")

        tr = Transform()
        
        for child in node.children:
            if child.name == "translation":
                tr.transforms.append(TranslationTform.from_wdf(child))
            elif child.name == "rotation":
                tr.transforms.append(RotationTform.from_wdf(child))
            elif child.name == "scale":
                tr.transforms.append(ScaleTform.from_wdf(child))
            elif child.name == "matrix":
                tr.transforms.append(MatrixTform.from_wdf(child))

        return tr

    @staticmethod
    def from_mitsuba(elem):
        tr = Transform()

        if elem is None:
            return tr

        for child in elem:
            if child.tag == "translate":
                tr.transforms.append(TranslationTform.from_mitsuba(child))
            elif child.tag == "rotate":
                tr.transforms.append(RotationTform.from_mitsuba(child))
            elif child.tag == "scale":
                tr.transforms.append(ScaleTform.from_mitsuba(child))
            elif child.tag == "matrix":
                tr.transforms.append(MatrixTform.from_mitsuba(child))
        
        return tr

    def to_wdf(self):
        n = WdfNode("transform")
        for tf in self.transforms:
            n.children.append(tf.to_wdf())
        return n

    def to_mitsuba(self, name):
        elem = Element("transform", { "name" : name })
        for tf in self.transforms:
            elem.append(tf.to_mitsuba())
        return elem

class Sphere:
    def __init__(self):
        self.radius = 1
        self.material = Material()
        self.transform = Transform()
        self.luminance = [0, 0, 0]

    def _is_emissive(self):
        return any([v > 0.0 for v in self.luminance])

    @staticmethod
    def from_wdf(world, wdf_node):
        wdf_node.require_name("sphere")
        
        sp = Sphere()

        radius_node = wdf_node.child_by_name("radius")
        if radius_node is not None:
            sp.radius = float(radius_node.value)
        
        sp.material = world.materials[wdf_node.child_by_name("material").value]
        sp.transform = Transform.from_wdf(wdf_node.child_by_name("transform"))
        sp.luminance = parse_vec3_node(wdf_node.child_by_name("luminance"))
    
        return sp

    @staticmethod
    def from_mitsuba(world, shape_elem):
        sp = Sphere()
        
        for elem in shape_elem:
            if elem.tag == "float" and elem.attrib.get("name") == "radius":
                sp.radius = float(elem.attrib.get("value", "1"))
            elif elem.tag == "transform" and elem.attrib.get("name") == "toWorld":
                sp.transform = Transform.from_mitsuba(elem)
            elif elem.tag == "ref" and (elem.attrib.get("id") in world.materials):
                sp.material = world.materials[elem.attrib["id"]]
            elif elem.tag == "emitter" and (elem.attrib.get("type") == "area"):
                rgb_elem = elem.find("rgb")
                if rgb_elem is not None:
                    sp.luminance = rgb_from_mitsuba(rgb_elem)
        
        return sp

    def to_wdf(self):
        n = WdfNode("sphere")
        n.children.append(WdfNode("radius", str(self.radius)))
        n.children.append(WdfNode("material", self.material.name))
        n.children.append(self.transform.to_wdf())
        if self._is_emissive():
            n.children.append(WdfNode("luminance", str("{} {} {}").format(*self.luminance)))
        return n

    def to_mitsuba(self):
        elem = Element("shape", {"type" : "sphere"})
        elem.append(Element("float", {"name" : "radius", "value" : str(self.radius)}))
        elem.append(self.material.to_mitsuba_ref())
        elem.append(self.transform.to_mitsuba("toWorld"))
        
        if self._is_emissive():
            area_emitter = Element("emitter", {"type" : "area"})
            area_emitter.append(rgb_to_mitsuba("radiance", self.luminance))
            elem.append(area_emitter)

        return elem

class Rectangle:
    def __init__(self):
        self.material = Material()
        self.transform = Transform()

    @staticmethod
    def from_wdf(world, wdf_node):
        wdf_node.require_name("rectangle")

        rect = Rectangle()
        rect.material = world.materials[wdf_node.child_by_name("material").value]
        rect.transform = Transform.from_wdf(wdf_node.child_by_name("transform"))

        return rect

    @staticmethod
    def from_mitsuba(world, elem):
        rect = Rectangle()

        for child in elem:
            if child.tag == "transform" and child.attrib.get("name") == "toWorld":
                rect.transform = Transform.from_mitsuba(child)
            elif child.tag == "ref" and (child.attrib.get("id") in world.materials):
                rect.material = world.materials[child.attrib["id"]]
        
        return rect

    def to_wdf(self):
        n = WdfNode("rectangle")
        n.children.append(WdfNode("material", self.material.name))
        n.children.append(self.transform.to_wdf())
        return n

    def to_mitsuba(self):
        elem = Element("shape", {"type" : "rectangle"})
        elem.append(self.material.to_mitsuba_ref())
        elem.append(self.transform.to_mitsuba("toWorld"))
        return elem

class Trimesh:
    def __init__(self):
        self.obj_path = None
        self.material = Material()
        self.transform = Transform()

    @staticmethod
    def from_wdf(world, dir_path, wdf_node):
        wdf_node.require_name("trimesh")
        
        tm = Trimesh()
        tm.obj_path = os.path.join(dir_path, wdf_node.child_by_name("obj_file").value)
        tm.material = world.materials[wdf_node.child_by_name("material").value]
        tm.transform = Transform.from_wdf(wdf_node.child_by_name("transform"))

        return tm
    
    @staticmethod
    def from_mitsuba(world, dir_path, elem):
        tm = Trimesh()
        
        for child in elem:
            if child.tag == "string" and child.attrib.get("name") == "filename":
                tm.obj_path = os.path.join(dir_path, child.attrib["value"])
            elif child.tag == "transform" and child.attrib.get("name") == "toWorld":
                tm.transform = Transform.from_mitsuba(child)
            elif child.tag == "ref" and (child.attrib.get("id") in world.materials):
                tm.material = world.materials[child.attrib["id"]]

        return tm

    def to_wdf(self):
        n = WdfNode("trimesh")
        n.children.append(WdfNode("obj_file", self.obj_path))
        n.children.append(WdfNode("material", self.material.name))
        n.children.append(self.transform.to_wdf())
        return n

    def to_mitsuba(self):
        elem = Element("shape", {"type" : "obj"})
        elem.append(Element("string", {"name" : "filename", "value" : self.obj_path}))
        elem.append(self.material.to_mitsuba_ref())
        elem.append(self.transform.to_mitsuba("toWorld"))
        return elem

class PointLight:
    def __init__(self):
        self.transform = Transform()
        self.intensity = (100, 100, 100)

    @staticmethod
    def from_wdf(wdf_node):
        wdf_node.require_name("point_light")
        
        pl = PointLight()

        pos_node = wdf_node.child_by_name("position")
        if pos_node is not None:
            pl.transform.transforms.append(TranslationTform(parse_vec3_node(pos_node)))
        else:
            pl.transform = Transform.from_wdf(wdf_node.child_by_name("transform"))
        
        pl.intensity = parse_vec3_node(wdf_node.child_by_name("intensity"), pl.intensity)

        return pl

    @staticmethod
    def from_mitsuba(elem):
        pl = PointLight()
        
        for child in elem:
            if child.tag == "transform" and child.attrib.get("name") == "toWorld":
                pl.transform = Transform.from_mitsuba(child)
            elif child.tag == "rgb" and child.attrib.get("name") == "intensity":
                pl.intensity = rgb_from_mitsuba(child)
        
        return pl

    def to_wdf(self):
        n = WdfNode("point_light")
        n.children.append(WdfNode("intensity", "{} {} {}".format(*self.intensity)))
        n.children.append(self.transform.to_wdf())
        return n

    def to_mitsuba(self):
        elem = Element("emitter", {"type" : "point"})
        elem.append(self.transform.to_mitsuba("toWorld"))
        elem.append(rgb_to_mitsuba("intensity", self.intensity))

        return elem

class Camera:
    def __init__(self):
        self.position = (0, 0, 1)
        self.look_at = (0, 0, 0)
        self.f_number = 1.4

    @staticmethod
    def from_wdf(wdf_node):
        wdf_node.require_name("camera")
        
        cam = Camera()
        cam.position = parse_vec3_node(wdf_node.child_by_name("position"), (0, 0, 1))
        cam.look_at = parse_vec3_node(wdf_node.child_by_name("look_at"))
        
        f_num_node = wdf_node.child_by_name("f-number")
        if f_num_node is not None:
            cam.f_number = float(f_num_node.value)

        return cam

    @staticmethod
    def from_mitsuba(elem):

        # TODO
        cam = Camera()
        cam.position = [0, 0, 14]
        cam.f_number = 0.5

        return cam

    def to_wdf(self):
        n = WdfNode("camera")
        n.children.append(WdfNode("position", "{} {} {}".format(*self.position)))
        n.children.append(WdfNode("look_at", "{} {} {}".format(*self.look_at)))
        n.children.append(WdfNode("f-number", str(self.f_number)))
        return n

    def to_mitsuba(self):
        elem = Element("sensor", {"type" : "perspective"})
        
        tform = Element("transform", {"name" : "toWorld"})
        tform.append(Element("lookat", {
            "origin" : "{}, {}, {}".format(self.position[0], self.position[1], self.position[2]),
            "target" : "{}, {}, {}".format(self.look_at[0], self.look_at[1], self.look_at[2]),
            "up" : "0, 1, 0"
        }))

        elem.append(tform)

        focal_len = 35.0 * self.f_number
        elem.append(Element("string", {"name" : "focalLength", "value" : "{}mm".format(focal_len)}))

        film = Element("film", {"type" : "ldrfilm"})
        film.append(Element("integer", {"name" : "width", "value" : "$width"}))
        film.append(Element("integer", {"name" : "height", "value" : "$height"}))
        elem.append(film)

        sampler = Element("sampler", {"type" : "independent"})
        sampler.append(Element("integer", {"name" : "sampleCount", "value" : "$num_samples"}))
        elem.append(sampler)

        return elem

class World:
    def __init__(self):
        self.materials = {}
        self.objects = []
        self.lights = []
        self.camera = None

    @staticmethod
    def from_wdf(file_path):
        root = parse_wdf(file_path)

        if not root or root.name != "world":
            raise Exception("the file does not contain world description!")

        dir_path = os.path.dirname(file_path)

        w = World()

        for node in root.children:
            if node.name == "material":
                mtrl = Material.from_wdf(node)
                w.materials[mtrl.name] = mtrl
        
        for node in root.children:
            if node.name == "rectangle":
                w.objects.append(Rectangle.from_wdf(w, node))
            if node.name == "sphere":
                w.objects.append(Sphere.from_wdf(w, node))
            elif node.name == "trimesh":
                w.objects.append(Trimesh.from_wdf(w, dir_path, node))
            elif node.name == "point_light":
                w.lights.append(PointLight.from_wdf(node))
        
        w.camera = Camera.from_wdf(root.child_by_name("camera"))

        return w

    @staticmethod
    def from_mitsuba(file_path):
        tree = xml.etree.ElementTree.parse(file_path)
        root = tree.getroot()

        if root.tag != "scene" or root.attrib.get("version") != "0.5.0":
            raise Exception("input file is not a Mitsuba 0.5.0 file!")

        dir_path = os.path.dirname(file_path)

        w = World()

        for bsdf_elem in root.iter("bsdf"):
            mtrl = Material.from_mitsuba(bsdf_elem)
            w.materials[mtrl.name] = mtrl

        for shape_elem in root.iter("shape"):
            shape_type = shape_elem.attrib.get("type")
            if shape_type == "sphere":
                w.objects.append(Sphere.from_mitsuba(w, shape_elem))
            elif shape_type == "rectangle":
                w.objects.append(Rectangle.from_mitsuba(w, shape_elem))
            elif shape_type == "obj":
                w.objects.append(Trimesh.from_mitsuba(w, dir_path, shape_elem))

        for emitter_elem in root.iter("emitter"):
            emitter_type = emitter_elem.attrib.get("type")
            if emitter_type == "point":
                w.lights.append(PointLight.from_mitsuba(emitter_elem))

        w.camera = Camera.from_mitsuba(root.find("sensor"))

        return w

    def to_wdf(self, file_path):
        root = WdfNode("world")

        for mtrl in self.materials.values():
            root.children.append(mtrl.to_wdf())

        for obj in self.objects:
            root.children.append(obj.to_wdf())

        for light in self.lights:
            root.children.append(light.to_wdf())

        root.children.append(self.camera.to_wdf())

        root.write(open(file_path, "w"))

    def to_mitsuba(self, file_path):

        scene_elem = Element("scene", {"version" : "0.5.0"})

        scene_elem.append(Element("default", { "name" : "width", "value" : "512" }))
        scene_elem.append(Element("default", { "name" : "height", "value" : "512" }))
        scene_elem.append(Element("default", { "name" : "num_samples", "value" : "32" }))

        integrator = Element("integrator", {"type" : "path"})
        scene_elem.append(integrator)

        for mtrl in self.materials.values():
            scene_elem.append(mtrl.to_mitsuba())

        for obj in self.objects:
            scene_elem.append(obj.to_mitsuba())

        for light in self.lights:
            scene_elem.append(light.to_mitsuba())
        
        scene_elem.append(self.camera.to_mitsuba())

        xml_str = xml.etree.ElementTree.tostring(scene_elem, "utf-8")
        reparesed = xml.dom.minidom.parseString(xml_str)
        pretty_xml_str = reparesed.toprettyxml()
        open(file_path, "w").write(pretty_xml_str)

if __name__ == "__main__":

    input_path = sys.argv[1]
    output_path = sys.argv[2]

    input_ext = os.path.splitext(input_path)[1]
    output_ext = os.path.splitext(output_path)[1]

    if input_ext == ".wdf" and output_ext == ".xml":
        print("Converting WDF to Mitsuba...")
        world = World.from_wdf(input_path)
        world.to_mitsuba(output_path)
    elif input_ext == ".xml" and output_ext == ".wdf":
        print("Converting Mitsuba to WDF...")
        world = World.from_mitsuba(input_path)
        world.to_wdf(output_path)
    else:
        print("cannot convert from \"{}\" to \"{}\"".format(input_ext, output_ext))

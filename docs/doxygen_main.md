[TOC]

## Modules

* [World](@ref world)

* [Render buffers](@ref render_buf) and [Image I/O](@ref image_io)

* [WDF parser](@ref wdf)

* [Utilities](@ref Utilities)

## Suzanne in a Cornell box

Image below was rendered using the Path Tracer with 16 samples per pixel.

Suzanne is [Blender's](https://blender.org) mascott.

![Suzanne in a Cornell box](/suzzy.png)

.. Path Tracer documentation master file, created by
   sphinx-quickstart on Thu Jan 23 12:05:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Path Tracer's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   usage/index
   source_code/index

Path Tracer version |version|

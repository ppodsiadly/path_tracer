-------------------------
Source code documentation
-------------------------

This section describes implementation of Path Tracer.

.. toctree::

   dirs
   architecture
   path_tracing

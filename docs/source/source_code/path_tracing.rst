Path Tracing Algorithm
======================

Overview of the algorithm
-------------------------

Implementation of the algorithm
-------------------------------

Optimization: Broad phase in intersection tests
-----------------------------------------------

A path tracer spends most of execution time
checking for intersections between rays and geometry
in the scene. This can be accelerated by first checking
for intersection with a `bounding volume`_, and then
testing for actual intersection only if a ray intersects
the bounding volume.

This path tracer uses axis-aligned bounding boxes
(AABBs) as bounding volumes in the broad phase.
Testing for ntersection between a ray and AABB
is much faster than, for example, testing for
intersection between a ray and a triangle.

In the soruce code, AABBs are represented using
:code:`bbox_t` type defined in `vec_math.h`.

Optimiation: Bounding Volume Hierarchy
--------------------------------------

`Bounding Volume Hierarchy`_ is a spatial data structure
which speeds up intersection and visibility tests.

Path Tracer uses BVH to accelerate intersection tests
between rays and triangles in triangle meshes and in
broad phase intersection tests.

BVH is implemented in `bvh.h` and `bvh.c`.

Optimization: Parallel execution using OpenMP
---------------------------------------------

The main path tracing loop can be executed in parallel
on multi-core CPUs. Path Tracer splits the destination
render buffer into tiles, so that each tile can
be rendererd independently, and possibly in parallel, on
different cores. This is implemented in `world_pathtrace.c`,
using `OpenMP`_'s :code:`#pragma` preprocessor directives.

.. _`bounding volume`: https://en.wikipedia.org/wiki/Bounding_volume
.. _`Bounding Volume Hierarchy`: https://en.wikipedia.org/wiki/Bounding_volume_hierarchy
.. _OpenMP: https://www.openmp.org/

Architecture
============

Overview
--------

Common conventions
~~~~~~~~~~~~~~~~~~

Source code of Path Tracer uses object-orientent conventions
for representing data.

**Creating and destroying objects**

Data structures are created with :code:`_create` functions and
destroyed with :code:`_free` functions. For example:

* :code:`world_t* world_create(void);` and :code:`void world_free(world_t* w);`

* :code:`image_t* image_create(uint32_t width, uint32_t height);`
  and :code:`void image_free(image_t* img);`

**Reference-counted objects**

Some data structures, for example triangle meshes (:code:`trimesh_t`) are
reference-counted. In these cases, resources allocated for an object
are released by calling :code:`release` function, instead of :code:`free`
function. For example:

* :code:`trimesh_t* trimesh_create(void);`
  and :code:`void trimesh_release(trimesh_t* tm);`

For such objects, :code:`release` functions first decrement a reference
counter, and only if it reaches 0, allocated resources are freed.

Reference counters can be incremented by calling :code:`add_ref` functions
(e. g. :code:`void trimesh_add_ref(trimesh_t* tm);`).

**Invalid arguments**

In general, Path Tracer functions try to silently handle invalid arguments
(for example, number out of range, or null pointers).

When an invalid argument is passed, functions will typically return as soon
as possible.

If a function returns a value, it will return a default value, when called with
an invalid value. For example, :code:`image_get_width` return 0, when called
with a null :code:`image_t` pointer as its argument.

World
~~~~~

Each instance of :code:`world_t` type represents a single scene
with one, or more, geometric objects and one, or more, light sources.

Render Buffer and Image Output
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

WDF Parser
~~~~~~~~~~

Worlds can be loaded from simple text files in a custom format called WDF
(World Description Format). It is a hierarchical format with one, root node.

Each node has a required name (followed by a colon) an optional value and
zero, or more, child nodes (in subsequent lines).

Hierarchy is determined based on indentation of lines. Child nodes have longer
indentation than parents while sibling nodes must have the same indentation.

All lines in a single file must use the same character for indentation
(space or tab), otherwise an error is reported. This is similar to Python.

Typical WDF file looks like this
(this is `emissibe_rectangle.wdf` used in tests):

.. code-block::

  world:
    material: rectangle
        brdf: lambert
        R: 0.8 0.8 0.8

    rectangle:
        material: rectangle
        luminance: 10 10 10
        transform:
            scale: 0.5 0.5 0.5
            translation: 0 0 -3

    rectangle:
        material: rectangle
        transform:
            scale: 3 3 3
            rotation: 1 0 0 90
            translation: 0 -0.5 0

    point_light:
        position: 0 5 0
        intensity: 5 5 5

    camera:
        position: 5 2 0
        look_at: 0 0 0
        f-number: 0.5

WDF parsing is implemented in `wdf_parser.h` and `wdf_parser.c` files.
Parser creates a tree of nodes (:code:`wdf_node_t`). All WDF-related
functions have a :code:`wdf_` prefix.

Currently, WDF format is used only for deserializing worlds
(:code:`world_t`). Code for this is located in `wdf_world.c` file
and uses public API of the WDF module and public API of the World module.

Utility types and functions
~~~~~~~~~~~~~~~~~~~~~~~~~~~

A number of source files include implementation of utility functions used
throughout the code.

* `color.h`: this file defines structures representing RGB color values
  (either using floating point values, or 8 bit unsigned integers)
  and various functions for working with color values.

* `common.h` and `common.c`: these files include commonly used macros
  and a logging function which is used to report warnings and errors to a user.

  :code:`C_HEADER_BEGIN` and :code:`C_HEADER_END` macros are used in all
  C headers. When a header is compiled as C++ (that's the case when
  compiling tests), these macros make all functions in between have a
  C linkage.

  Visual Studio does not support :code:`inline` keyword from C99.
  In this case, `common.h` defines a macro :code:`inline`, which is
  translated to VS-specific :code:`__inline` keyword.

  The header defines a :code:`VECCALL` macro, which is expanded to
  :code:`__vectorcall` when compiling with Visual Studio.
  :code:`__vectorcall` calling convention uses more SIMD registers
  and for this reason is used for math utility functions.

* `console.h` and `console.c`: these files implement platform-specific
  utilities for manipulating console output. These functions are
  used by the CLI application to report render progress as an
  animated "prgress bar" in a console.

* `containers.h` and `container.c`: these files implement
  :code:`array_t`, which is a generic, dynamically resized array type,
  similar to :code:`std::vector<>` in C++.

* `prng.h`: this header implements a simple and fast pseudo-random number
  generator.

* `vec_math.h` and `vec_math.c`: these files implement a number of
  linear algebra functions used throughout the code.

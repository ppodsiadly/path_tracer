Directory structure
===================

``cli/``
--------

This dicrectory contains implementation of the command
line interface application.

CLI application parses arguments, loads data from files
and calls Path Tracer's API to trigger the render.

It is also responsible for reporting render progress
to user.

``lib/``
--------

This directory contains implementation of the actual path
tracing algorithm, data input and output and various
utilities used by them. Source code in this directory
is built as a static library which is linked to
the CLI executable (``render_cli``) and test executables.

``tests/``
----------

This directory contains imlementation of automatic tests.
These tests are not built by default and depend on:

  - ``path_tracer`` static library,

  - doctest_ unit testing framework,

  - ApprovalTests_ testing framework,

  - `Google Benchmark`_ library

Third-party dependencies are obtained using `Conan Package Manager`_
(see ``tests/CMakeLists.txt`` and ``tests/conan.cmake`` files).

Source code inside ``tests//`` is built as three executables:

  - ``pt_unit_tests`` which runs simple unit tests for
    ``path_tracer`` library

  - ``pt_integration_tests`` which runs integration tests.
    Each integration test renders a simple scene and
    compares rendered image with a reference image
    from ``tests/integration/approval_tests/`` directory.
    When images are identical, the test passes.
    Otherwise, test failure is reported.

  - ``pt_performance_tests`` which runs benchmarks. Each
    benchmark renders a scene (multiple times) and measures
    average time of the render process. These tests are
    used to verify impact of changes on performance.

``wdf_convert.py`` script
-------------------------

Python 3 script which can be used to convert files
between ``.wdf`` file format and XML-based file format
used by `Mitsuba renderer`_. This allows to quickly compare
images rendered by Path Tracer with images rendered by Mitsuba,
for the same scene and therefore validate correctness of
the path tracing algorithm.

.. _doctest: https://github.com/onqtam/doctest

.. _ApprovalTests: https://github.com/approvals/ApprovalTests.cpp

.. _`Google Benchmark`: https://github.com/google/benchmark

.. _`Conan Package Manager`: https://conan.io/

.. _`Mitsuba renderer`: https://www.mitsuba-renderer.org/

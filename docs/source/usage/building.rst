Building
========

Requirements
------------

- CMake_ 3.10 or newer

- C compiler (tested with Visual Studio 2017+ and gcc/clang)

- For automatic tests (not built by default):

  - `Python 3`_

  - `Conan Package Manager`_

Steps to build Path Tracer from command line
--------------------------------------------

1. Download source code into a conventient location

  - ``> git clone https://ppodsiadly@bitbucket.org/ppodsiadly/path_tracer.git``

2. Create a build folder inside the source folder and ``cd`` into it:

  - ``> mkdir build/``

  - ``> cd build/``

  - Directory structure should look like this:

    ::

      .
      |-path_tracer/
        | cli/
        | build/
        | docs/
        | lib/
        | tests/
        | CMakeLists.txt
        |-...

3. Configure build using CMake inside ``path_tracer/build/`` directory:

  - ``> cmake ../ -DCMAKE_BUILD_TYPE=Release``

4. Build Path Tracer using CMake:

  - ``> cmake --build . --config Release``

5. Path Tracer's command line interface will be built:

  - On Windows, with Visual Studio, the program will be located
    in ``path_tracer/build/cli/Release/render_cli.exe``

  - On Linux, the program will be located in
    ``path_tracer/build/cli/render_cli``

Building with Visual Studio 2017 or 2019
----------------------------------------

Starting with Visual Studio 2017, CMake projects can be opened directly
from the IDE. After starting VS, choose ``File > Open > CMake...` and
select ``CMakeLists.txt`` file in the root project folder.

Once the project is loaded, it can be built using ``CMake > Build all`` menu
option.

Enabling automatic tests
------------------------

In order to build automatic tests, it is necessary to pass ``ENABLE_TESTS=ON``
option to CMake:

  - when building from command line, pass the option when configuring the
    build:
    ``> cmake ../pathtracer -DCMAKE_BUILD_TYPE=Release -DENABLE_TESTS=ON``

  - when building from inside Visual Studio 2017/2019, it is necessary to
    set the opition inside ``CMakeCache.txt`` file:

    - click on ``CMake > Cache > View CMakeCache.txt > path_tracer`` menu
      option

    - replace line ``ENABLE_TESTS:BOOL=OFF`` with ``ENABLE_TESTS:BOOL=ON``

.. _CMake: https://cmake.org/
.. _Python 3: https://www.python.org/
.. _Conan Package Manager: https://conan.io/

-----
Usage
-----

This section describes steps necessary to build and run
Path Tracer's CLI (command-line interface) application.

.. toctree::
  building
  running

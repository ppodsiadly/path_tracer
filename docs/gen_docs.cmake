macro(find_command var_name cmd_name)
    find_program(${var_name} "${cmd_name}")

    if("${${var_name}}" STREQUAL "${var_name}-NOTFOUND")
        message(FATAL_ERROR "Could not find \"${cmd_name}\"")
    else()
        message(STATUS "Found \"${cmd_name}\": \"${${var_name}}\"")
    endif()
endmacro()

find_command(doxygen_cmd "doxygen")
find_command(sphinx_build_cmd "sphinx-build")
find_command(git_cmd "git")

if("${OUT_DIR}" STREQUAL "")
    set(OUT_DIR "build")
endif()

if(NOT IS_ABSOLUTE "${OUT_DIR}")
    set(OUT_DIR "${CMAKE_CURRENT_SOURCE_DIR}/${OUT_DIR}")
endif()

if(NOT VERBOSE)
    set(quiet_mode OUTPUT_QUIET)
endif()

file(MAKE_DIRECTORY "${OUT_DIR}/doxygen")
file(MAKE_DIRECTORY "${OUT_DIR}/sphinx")

#########################
# Get HEAD SHA from Git #
#########################

execute_process(
    COMMAND "${git_cmd}" log -n 1
    WORKING_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/../"
    OUTPUT_VARIABLE git_log_output
)

string(REGEX MATCH "commit ([a-z0-9]+)" git_commit_line "${git_log_output}")
string(REGEX REPLACE "commit ([a-z0-9]+)" "\\1" git_head_hash "${git_commit_line}")

message(STATUS "Git HEAD: ${git_head_hash}")

###########
# Doxygen #
###########

set(doxygen_src_dir "${CMAKE_CURRENT_LIST_DIR}")
set(src_doxyfile "${doxygen_src_dir}/Doxyfile")
set(doxyfile "${OUT_DIR}/doxygen/Doxyfile")

file(READ "${src_doxyfile}" doxyfile_content)

string(
    REPLACE
    "OUTPUT_DIRECTORY = build/doxygen"
    "OUTPUT_DIRECTORY = ${OUT_DIR}/doxygen"
    doxyfile_content
    "${doxyfile_content}"
)

string(
    REPLACE
    "PROJECT_NUMBER ="
    "PROJECT_NUMBER = ${git_head_hash}"
    doxyfile_content
    "${doxyfile_content}"
)

file(WRITE "${doxyfile}" "${doxyfile_content}")

message(STATUS "Running Doxygen...")
message(STATUS "  - Doxygen command: \"${doxygen_cmd}\"")
message(STATUS "  - Doxyfile: \"${doxyfile}\"")
message(STATUS "  - OUTPUT_DIRECTORY: \"${OUT_DIR}/doxygen\"")
message(STATUS "  - Working dir: \"${doxygen_src_dir}\"")
execute_process(
    COMMAND "${doxygen_cmd}" "${doxyfile}"
    WORKING_DIRECTORY "${doxygen_src_dir}"
    RESULT_VARIABLE doxygen_result
    ${quiet_mode}
)

if(NOT doxygen_result EQUAL 0)
    message(FATAL_ERROR "Doxygen exited with ${doxygen_result}")
else()
    message(STATUS "Doxygen exited with 0")
endif()

##########
# Sphinx #
##########

set(sphinx_src_dir "${CMAKE_CURRENT_LIST_DIR}/source")
set(sphinx_build_dir "${OUT_DIR}/sphinx")

message(STATUS "Running Sphinx...")
message(STATUS "  - Sphinx command: \"${sphinx_build_cmd}\"")
message(STATUS "  - Source dir: \"${sphinx_src_dir}\"")
message(STATUS "  - Build dir: \"${sphinx_build_dir}\"")
message(STATUS "  - Working dir: \"${OUT_DIR}\"")

execute_process(
    COMMAND "${sphinx_build_cmd}" -M html "${sphinx_src_dir}" "${sphinx_build_dir}" -D "version=${git_head_hash}"
    WORKING_DIRECTORY "${OUT_DIR}"
    RESULT_VARIABLE sphinx_result
    ${quiet_mode}
)

if(NOT sphinx_result EQUAL 0)
    message(FATAL_ERROR "Sphinx exited with ${sphinx_result}")
else()
    message(STATUS "Sphinx exited with 0")
endif()
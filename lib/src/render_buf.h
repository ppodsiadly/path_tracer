#ifndef RENDER_bUF_H_INCLUDED
#define RENDER_bUF_H_INCLUDED

#include "color.h"
#include "image.h"

C_HEADER_BEGIN

/** \addtogroup render_buf Render buffers
 * @{
 */

typedef struct render_buf_t render_buf_t;

render_buf_t* render_buf_create(uint32_t width, uint32_t height);
void render_buf_free(render_buf_t* rb);

void render_buf_set_pixel(render_buf_t* rb, uint32_t x, uint32_t y, const color_t color);

uint32_t render_buf_get_width(const render_buf_t* rb);
uint32_t render_buf_get_height(const render_buf_t* rb);
color_t render_buf_get_pixel(const render_buf_t* rb, uint32_t x, uint32_t y);

image_t* render_buf_to_image(const render_buf_t* rb);

typedef struct
{
    uint32_t min_x, min_y;
    uint32_t max_x, max_y;
} rb_tile_bounds_t;

uint32_t render_buf_get_num_tiles(const render_buf_t* rb);
void render_buf_get_tile_bounds(const render_buf_t* rb, uint32_t tile_index, rb_tile_bounds_t* bounds);

/** @} */

C_HEADER_END

#endif /* RENDER_bUF_H_INCLUDED */
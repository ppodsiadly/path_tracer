#include "world.h"
#include "trimesh.h"
#include "containers.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct scene_node_t scene_node_t;

typedef bool(*node_visibility_test_fn_t)(const scene_node_t* node, const vec3_t from, const vec3_t to);
typedef bool(*node_intersection_fn_t)(const scene_node_t* node, const ray_t* ray, surf_point_t* point, float* t);
typedef void(*node_destructor_fn_t)(scene_node_t* node);

struct scene_node_t
{
    node_visibility_test_fn_t visibility_test_fn;
    node_intersection_fn_t intersection_fn;
    node_destructor_fn_t destructor_fn;

    mat4_t obj_to_world;
    mat4_t world_to_obj;
    bbox_t world_bbox;
    material_t* material;
    color_t surface_luminance;
};

static void _set_node_tform(scene_node_t* node, const mat4_t* tform)
{
    if(tform)
    {
        memcpy(&node->obj_to_world, tform, sizeof(mat4_t));
    }
    else
    {
        mat4_identity(&node->obj_to_world);
    }

    mat4_invert(&node->obj_to_world, &node->world_to_obj);
}

typedef struct
{
    scene_node_t base;
    float radius;
} sphere_node_t;

static bool _sphere_node_visibility_test(const scene_node_t* base, const vec3_t from, const vec3_t to)
{
    const sphere_node_t* node = (const sphere_node_t*)base;
    return segment_sphere_test(from, to, node->radius);
}

static bool _sphere_node_intersection(const scene_node_t* base, const ray_t* ray, surf_point_t* point, float* t)
{
    const sphere_node_t* node = (const sphere_node_t*)base;
    return ray_sphere_intersection(ray, node->radius, point, t);
}

typedef struct
{
    scene_node_t base;
    vec2_t half_size;
} rect_node_t;

static bool _rect_node_visibility_test(const scene_node_t* base, const vec3_t from, const vec3_t to)
{
    const rect_node_t* node = (const rect_node_t*)base;
    return segment_xy_plane_test(from, to, node->half_size);
}

static bool _rect_node_intersection(const scene_node_t* base, const ray_t* ray, surf_point_t* point, float* t)
{
    const rect_node_t* node = (const rect_node_t*)base;
    return ray_xy_plane_intersection(ray, node->half_size, point, t);
}

typedef struct
{
    scene_node_t base;
    trimesh_t* trimesh;
} trimesh_node_t;

static bool _trimesh_node_visibility_test(const scene_node_t* base, const vec3_t from, const vec3_t to)
{
    const trimesh_node_t* node = (const trimesh_node_t*)base;
    return segment_trimesh_test(from, to, node->trimesh);
}

static bool _trimesh_node_intersection(const scene_node_t* base, const ray_t* ray, surf_point_t* point, float* t)
{
    const trimesh_node_t* node = (const trimesh_node_t*)base;
    return ray_trimesh_intersect(ray, node->trimesh, point, t);
}

static void _trimesh_node_destructor(scene_node_t* base)
{
    trimesh_node_t* node = (trimesh_node_t*)base;
    trimesh_release(node->trimesh);
}

typedef struct point_light_node_t
{
    vec3_t position;
    color_t color;
} point_light_node_t;

struct world_t
{
    material_cache_t* material_cache;

    array_t* scene_nodes;
    array_t* point_lights;
};

world_t* world_create(void)
{
    world_t* world = (world_t*)malloc(sizeof(world_t));
    if(!world)
    {
        return NULL;
    }

    world->material_cache = material_cache_create();

    world->scene_nodes = array_create(sizeof(scene_node_t*));

    world->point_lights = array_create(sizeof(point_light_node_t));

    return world;
}

void world_free(world_t* w)
{
    if(!w)
    {
        return;
    }

    material_cache_free(w->material_cache);

    ARRAY_FOREACH(w->scene_nodes, scene_node_t*, n)
    {
        if((*n)->destructor_fn)
        {
            (*n)->destructor_fn(*n);
        }

        material_release((*n)->material);
        free(*n);
    }

    array_free(w->scene_nodes);
    
    array_free(w->point_lights);

    free(w);
}

material_cache_t* world_get_material_cache(world_t* world)
{
    if (!world)
    {
        return NULL;
    }

    return world->material_cache;
}

void world_add_sphere(world_t* world,
                      float radius,
                      material_t* material,
                      color_t surf_luminance,
                      const mat4_t* tform)
{
    if(!world || !material)
    {
        return;
    }

    sphere_node_t* sphere = (sphere_node_t*)malloc(sizeof(sphere_node_t));

    sphere->base.visibility_test_fn = &_sphere_node_visibility_test;
    sphere->base.intersection_fn = &_sphere_node_intersection;
    sphere->base.destructor_fn = NULL;
    sphere->base.material = material;
    sphere->base.surface_luminance = surf_luminance;
    material_add_ref(material);
    _set_node_tform(&sphere->base, tform);
    sphere->base.world_bbox = mat4_bbox_tform(&sphere->base.obj_to_world, bbox_from_sphere(radius));

    sphere->radius = radius;

    array_append(world->scene_nodes, &sphere);
}

void world_add_rectangle(world_t* world,
                         material_t* material,
                         color_t surf_luminance,
                         const mat4_t* tform)
{
    if(!world || !material)
    {
        return;
    }

    const vec2_t half_size = make_vec2(1.0f, 1.0f);

    rect_node_t* r = (rect_node_t*)malloc(sizeof(rect_node_t));

    r->base.visibility_test_fn = &_rect_node_visibility_test;
    r->base.intersection_fn = &_rect_node_intersection;
    r->base.destructor_fn = NULL;
    r->base.material = material;
    r->base.surface_luminance = surf_luminance;
    material_add_ref(material);
    _set_node_tform(&r->base, tform);
    r->base.world_bbox = mat4_bbox_tform(&r->base.obj_to_world, bbox_from_xy_rect(half_size));

    r->half_size = half_size;

    array_append(world->scene_nodes, &r);
}

void world_add_point_light(world_t* world,
                           const vec3_t position,
                           const color_t color)
{
    if(!world)
    {
        return;
    }

    point_light_node_t light;
    light.position = position;
    light.color = color;

    array_append(world->point_lights, &light);
}

void world_add_trimesh(world_t* world, trimesh_t* tm, material_t* mtrl, color_t surf_luminance, const mat4_t* tform)
{
    if (!world || !tm || !mtrl)
    {
        return;
    }

    trimesh_node_t* node = (trimesh_node_t*)malloc(sizeof(trimesh_node_t));

    node->base.visibility_test_fn = &_trimesh_node_visibility_test;
    node->base.intersection_fn = &_trimesh_node_intersection;
    node->base.destructor_fn = &_trimesh_node_destructor;
    node->base.material = mtrl;
    node->base.surface_luminance = surf_luminance;
    material_add_ref(mtrl);
    _set_node_tform(&node->base, tform);
    node->base.world_bbox = mat4_bbox_tform(&node->base.obj_to_world, trimesh_bbox(tm));

    node->trimesh = tm;
    trimesh_add_ref(tm);

    array_append(world->scene_nodes, &node);
}

bool world_cast_ray(const world_t* world, const ray_t* ray, surf_point_t* point, mat4_t* out_obj_to_world, const material_t** material, color_t* surface_luminance)
{
    assert(world && ray && point);

    float closest_t_squared = -1.0f;
    const scene_node_t* closest_node = NULL;

    ARRAY_FOREACH(world->scene_nodes, const scene_node_t*, pp_node)
    {
        const scene_node_t* node = *pp_node;

        if(!bbox_ray_test(&node->world_bbox, ray, NULL))
        {
            continue;
        }

        surf_point_t this_isect;
        float this_t;

        ray_t obj_space_ray;
        mat4_ray_tform(&node->world_to_obj, ray, &obj_space_ray);
        if(node->intersection_fn(node, &obj_space_ray, &this_isect, &this_t))
        {
            /* this_t is distance from the origin to the intersection point int the *object* space.
             * Below, ray (object-space origin -> object-space hit) is transformed to world space.
             * this_t_squared is squared distance from the origin to the intersection in *world* space
             * and can be compared with closest_t_squared (also in world space).
             */

            obj_space_ray.direction = vec3_mul(vec3_normalize(obj_space_ray.direction), this_t);
            ray_t world_space_hit_ray;
            mat4_ray_tform(&node->obj_to_world, &obj_space_ray, &world_space_hit_ray);
            
            const float this_t_squared = vec3_squared_length(world_space_hit_ray.direction);

            if(!closest_node || (closest_t_squared > this_t_squared))
            {
                *point = this_isect;
                closest_node = node;
                closest_t_squared = this_t_squared;
            }
        }
    }

    if(closest_node)
    {
        if(out_obj_to_world)
        {
            memcpy(out_obj_to_world, &closest_node->obj_to_world, sizeof(mat4_t));
        }

        if(material)
        {
            *material = closest_node->material;
        }

        if(surface_luminance)
        {
            *surface_luminance = closest_node->surface_luminance;
        }
    }

    return (closest_t_squared >= 0.0f);
}

bool world_test_visibility(const world_t* world, const vec3_t from, const vec3_t to)
{
    assert(world);

    ray_t ray;
    ray.origin = from;
    ray.direction = vec3_sub(to, from);

    ARRAY_FOREACH(world->scene_nodes, const scene_node_t*, pp_node)
    {
        const scene_node_t* node = *pp_node;

        if(!bbox_ray_test(&node->world_bbox, &ray, NULL))
        {
            continue;
        }

        const vec3_t obj_space_from = mat4_point_tform(&node->world_to_obj, from);
        const vec3_t obj_space_to = mat4_point_tform(&node->world_to_obj, to);
        if(node->visibility_test_fn(node, obj_space_from, obj_space_to))
        {
            return false;
        }
    }

    return true;
}

color_t world_compute_direct_lighting(const world_t* world, const surf_point_t* point, const material_t* material, const vec3_t wo)
{
    const float offset = 1e-4f;

    color_t result = make_color(0.0f, 0.0f, 0.0f);

    ARRAY_FOREACH(world->point_lights, const point_light_node_t, pl)
    {
        const vec3_t shadow_ray_dir = vec3_normalize(vec3_sub(pl->position, point->position));
        const vec3_t offset_point = vec3_add(point->position, vec3_mul(shadow_ray_dir, offset));

        if (world_test_visibility(world, offset_point, pl->position))
        {
            const float p2l_distance = vec3_distance(pl->position, point->position);

            const color_t f = material_evaluate_brdf(material, point, vec3_mul(shadow_ray_dir, -1.0f), wo);

            color_add_scaled(&result,
                color_mul_rgb(f, pl->color),
                fabsf(vec3_dot(point->normal, shadow_ray_dir) / (p2l_distance * p2l_distance)));
        }
    }

    return result;
}
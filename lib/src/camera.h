#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

/** \file
 */

#include "vec_math.h"

C_HEADER_BEGIN

typedef struct camera_t camera_t;

camera_t* camera_create(void);

void camera_free(camera_t* camera);

void camera_set_aperture(camera_t* camera,
						 float f_number,
						 float iso,
						 float shutter_speed);

void camera_look_at(camera_t* camera,
                    const vec3_t look_at,
                    const vec3_t up_vec,
                    const vec3_t cam_position);

ray_t camera_pixel_ray(const camera_t* camera, float aspect_ratio, float u, float v);

C_HEADER_END

#endif /* CAMERA_H_INCLUDED */
#include "wdf_parser.h"
#include "containers.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <assert.h>
#include <stdarg.h>

#ifdef _MSC_VER
# define strdup _strdup
#endif

static void log_warn(const char* file, uint32_t line, const char* format, ...)
{
	va_list list;
	va_start(list, format);

	char buffer[256];
	vsnprintf(buffer, 256, format, list);
	buffer[255] = '\0';

	va_end(list);

	log_message(LOG_WARN, "WDF Parser", "%s:%u: %s", file, line, buffer);
}

char* trim_string(const char* str, size_t length)
{
    if(length == 0)
    {
        return NULL;
    }

    size_t first_non_ws = 0;
    size_t last_non_ws = length - 1;

    for (; first_non_ws < length; ++first_non_ws)
    {
        const char ch = str[first_non_ws];
        if (!isspace(ch) && (ch != '\0'))
        {
            break;
        }
    }

    for (; last_non_ws > first_non_ws; --last_non_ws)
    {
        const char ch = str[last_non_ws];
        if (!isspace(ch) && (ch != '\0'))
        {
            break;
        }
    }

    if (first_non_ws >= length)
    {
        return NULL;
    }

    const size_t trimmed_len = last_non_ws - first_non_ws + 1;

    char* trimmed_str = (char*)malloc(trimmed_len + 1);

	if (trimmed_str)
	{
		memcpy(trimmed_str, str + first_non_ws, trimmed_len);
		trimmed_str[trimmed_len] = '\0';
	}

    return trimmed_str;
}

typedef struct
{
	uint32_t base_indent;
    int indent_char;
    uint32_t count;
} wdf_indent_t;

static bool _check_consistent_indent(const wdf_indent_t* parent, const wdf_indent_t* indent)
{
	if (!parent || parent->count == 0 || indent->count == 0)
	{
		return true;
	}

	if (parent->base_indent != indent->base_indent)
	{
		return true;
	}

	return (parent->indent_char == indent->indent_char);
}

static bool _is_sibling_indent(const wdf_indent_t* a, const wdf_indent_t* b)
{
	// assumes that indentation is consistent (checked with _check_consistent_indent)

    if(a->base_indent != b->base_indent)
    {
        return false;
    }

	return (a->base_indent + a->count) == (b->base_indent + b->count);
}

static bool _is_parent_indent(const wdf_indent_t* a, const wdf_indent_t* b)
{
	// assumes that indentation is consistent (checked with _check_consistent_indent)

    if(a->base_indent > b->base_indent)
    {
        return false;
    }

	return (a->base_indent + a->count) < (b->base_indent + b->count);
}

static bool _parse_indent(FILE* file, wdf_indent_t* out_indent)
{
    assert(file && out_indent);

    wdf_indent_t indent;
    indent.indent_char = ' ';
    indent.count = 0;

    while (!feof(file))
    {
        const int ch = fgetc(file);
        if (ch == ' ' || ch == '\t')
        {
            if (indent.count == 0)
            {
                indent.indent_char = ch;
                indent.count = 1;
            }
            else if(indent.indent_char == ch)
            {
                ++indent.count;
            }
            else
            {
                return false;
            }
        }
        else
        {
            ungetc(ch, file);
            *out_indent = indent;
            return true;
        }
    }

    return false;
}

typedef struct
{
	uint32_t line_number;
    wdf_indent_t indent;
    char* name;
    char* value;
} wdf_line_t;

static void _init_line(wdf_line_t* line)
{
	line->indent.base_indent = 0;
    line->indent.indent_char = 0;
    line->indent.count = 0;
    line->name = NULL;
    line->value = NULL;
}

typedef enum
{
    NEW_NODE,
    EMPTY_LINE,
    PARSE_ERROR
} line_type_t;

static line_type_t _parse_line(FILE* file, wdf_line_t* line, uint32_t line_number)
{
	line->line_number = line_number;

    if (!_parse_indent(file, &line->indent))
    {
        return PARSE_ERROR;
    }

    char line_buf[256];
    if (!fgets(line_buf, 256, file))
    {
        if (feof(file))
        {
            return EMPTY_LINE;
        }
        else
        {
            return PARSE_ERROR;
        }
    }

    const size_t line_len = strlen(line_buf);
    if (line_len <= 1 || strncmp(line_buf, "\r\n", 2) == 0)
    {
        return EMPTY_LINE;
    }

    size_t colon_pos = line_len;
    for (size_t i = 0; i < line_len; ++i)
    {
        if (line_buf[i] == ':')
        {
            colon_pos = i;
            break;
        }
    }

    if (colon_pos >= line_len)
    {
        return PARSE_ERROR;
    }

    line->name = trim_string(line_buf, colon_pos);
    line->value = trim_string(line_buf + colon_pos + 1, line_len - colon_pos - 1);

    return NEW_NODE;
}

static void _free_line(wdf_line_t* line)
{
    if (line->name)
    {
        free(line->name);
    }

    if (line->value)
    {
        free(line->value);
    }
}

struct wdf_node_t
{
    wdf_line_t line; 
    wdf_node_t* parent;
    array_t* children;

    char* file_path;
    bool owns_file_path;
};

static wdf_node_t* _alloc_node(void)
{
    wdf_node_t* node = (wdf_node_t*)malloc(sizeof(wdf_node_t));
    if (!node)
    {
        return NULL;
    }

    _init_line(&node->line);

    node->parent = NULL;

    node->children = array_create(sizeof(wdf_node_t*));
    if (!node->children)
    {
        wdf_free(node);
        return NULL;
    }

    node->file_path = NULL;
    node->owns_file_path = false;

    return node;
}

typedef struct wdf_parse_ctx_t
{
	struct wdf_parse_ctx_t* parent_ctx;
	FILE* file;
	char* file_path;
	uint32_t line_number;
	uint32_t base_indent;
} wdf_parse_ctx_t;

static wdf_parse_ctx_t* _parse_ctx_free(wdf_parse_ctx_t* ctx)
{
	wdf_parse_ctx_t* parent = NULL;

	if (ctx)
	{
		parent = ctx->parent_ctx;

		if (ctx->file_path)
		{
			free(ctx->file_path);
		}

		if (ctx->file)
		{
			fclose(ctx->file);
		}

		free(ctx);
	}

	return parent;
}

static wdf_parse_ctx_t* _parse_ctx_create(wdf_parse_ctx_t* parent, char* file_path, uint32_t base_indent)
{
	wdf_parse_ctx_t* ctx = (wdf_parse_ctx_t*)malloc(sizeof(wdf_parse_ctx_t));
	if (!ctx)
	{
		return NULL;
	}

	ctx->parent_ctx = parent;
	ctx->file = NULL;
	ctx->file_path = file_path;
	ctx->line_number = 1;
	ctx->base_indent = base_indent;

	if (!(ctx->file = fopen(file_path, "r")))
	{
		goto failure;
	}

	return ctx;

failure:
	log_warn(file_path, -1, "failed to create wdf parse context");

	if (ctx)
	{
		if (ctx->file_path)
		{
			free(ctx->file_path);
		}

		if (ctx->file)
		{
			fclose(ctx->file);
		}
	}

	free(ctx);
	
	return NULL;
}

wdf_node_t* wdf_parse(const char* root_file_path)
{
	wdf_parse_ctx_t* parse_ctx = _parse_ctx_create(NULL, strdup(root_file_path), 0);
	if (!parse_ctx)
	{
		return NULL;
	}

    wdf_node_t* root = NULL;
    wdf_node_t* parent = NULL;

    while (parse_ctx)
    {
		while (parse_ctx && feof(parse_ctx->file))
		{
			parse_ctx = _parse_ctx_free(parse_ctx);
		}

        if(!parse_ctx)
        {
            break;
        }

        wdf_line_t line;
        _init_line(&line);

        const line_type_t parse_res = _parse_line(parse_ctx->file, &line, parse_ctx->line_number++);
        if (parse_res == PARSE_ERROR)
        {
            log_warn(parse_ctx->file_path, line.line_number, "failed to parse a line");
            _free_line(&line);
            goto failure;
        }
        else if (parse_res == EMPTY_LINE)
        {
            continue;
        }

		line.indent.base_indent = parse_ctx->base_indent;

		if (strcmp(line.name, "include") == 0)
		{
			if (!line.value)
			{
				log_warn(parse_ctx->file_path, line.line_number, "\"include\" node does not specify file path!");
				_free_line(&line);
				goto failure;
			}

			char* file_dir = path_parent(parse_ctx->file_path);
			char* included_file_path = path_join(file_dir, line.value);
			free(file_dir);

			wdf_parse_ctx_t* new_ctx = _parse_ctx_create(parse_ctx, included_file_path, line.indent.count + parse_ctx->base_indent);
			if (!new_ctx)
			{
				goto failure;
			}

			parse_ctx = new_ctx;
			continue;
		}

		wdf_node_t* node = _alloc_node();
		if (!node)
		{
			goto failure;
		}

		node->line = line;

        if (parent)
        {
            if (!_check_consistent_indent(&parent->line.indent, &node->line.indent))
            {
                log_warn(parse_ctx->file_path, line.line_number, "incorrect indentation character");
                wdf_free(node);
                goto failure;
            }
            else if(_is_sibling_indent(&parent->line.indent, &node->line.indent))
            {
                parent = parent->parent;
            }
            else if (!_is_parent_indent(&parent->line.indent, &node->line.indent))
            {
                while (parent && !_is_parent_indent(&parent->line.indent, &node->line.indent))
                {
                    parent = parent->parent;
                }
            }

            if (parent == NULL)
            {
                log_warn(parse_ctx->file_path, line.line_number, "only one root node allowed");
                wdf_free(node);
                goto failure;
            }

            node->parent = parent;

			if (strcmp(parent->file_path, parse_ctx->file_path) == 0)
			{
				node->file_path = parent->file_path;
				node->owns_file_path = false;
			}
			else
			{
				node->file_path = strdup(parse_ctx->file_path);
				node->owns_file_path = true;
			}

            array_append(parent->children, &node);
            parent = node;
        }
        else if (!root)
        {
            node->file_path = strdup(parse_ctx->file_path);
            node->owns_file_path = true;
            root = parent = node;
        }
        else
        {
            log_warn(parse_ctx->file_path, line.line_number, "only one root node allowed");
            wdf_free(node);
            goto failure;
        }
    }

    return root;

failure:
	
	while (parse_ctx)
	{
		parse_ctx = _parse_ctx_free(parse_ctx);
	}

    wdf_free(root);
    return NULL;
}

void wdf_free(wdf_node_t* root_node)
{
    if (!root_node)
    {
        return;
    }

    _free_line(&root_node->line);

    wdf_node_t** children = (wdf_node_t**)array_data(root_node->children);
    for (uint32_t i = 0; i < array_size(root_node->children); ++i)
    {
        wdf_free(children[i]);
    }

    array_free(root_node->children);

    if(root_node->file_path && root_node->owns_file_path)
    {
        free(root_node->file_path);
    }

    free(root_node);
}

const char* wdf_node_name(const wdf_node_t* node)
{
    if (!node)
    {
        return NULL;
    }

    return node->line.name;
}

const char* wdf_node_value(const wdf_node_t* node)
{
    if (!node)
    {
        return NULL;
    }

    return node->line.value;
}

const char* wdf_node_file_path(const wdf_node_t* node)
{
    if(!node)
    {
        return NULL;
    }

    return node->file_path;
}

uint32_t wdf_node_line_number(const wdf_node_t* node)
{
	if (!node)
	{
		return 0;
	}

	return node->line.line_number;
}

const wdf_node_t* wdf_node_child_by_index(const wdf_node_t* node, uint32_t index)
{
    if (!node || index >= array_size(node->children))
    {
        return NULL;
    }

    return *(const wdf_node_t**)array_get(node->children, index);
}

const wdf_node_t* wdf_node_child_by_name(const wdf_node_t* node, const char* name)
{
    if (!node)
    {
        return NULL;
    }

    const wdf_node_t** children = (const wdf_node_t**)array_data(node->children);
    for (uint32_t i = 0; i < array_size(node->children); ++i)
    {
        if (strcmp(wdf_node_name(children[i]), name) == 0)
        {
            return children[i];
        }
    }

    return NULL;
}

static bool is_path_sep(char ch)
{
    return (ch == '/') || (ch == '\\');
}

char* path_parent(const char* path)
{
    if (!path)
    {
        return strdup("../");
    }
    
    size_t len = strlen(path);

    if (len == 0)
    {
        return strdup("../");
    }

    /* ignore the trailing all (back-)slashes */
    while (len > 0)
    {
        if (is_path_sep(path[len - 1]))
        {
            len -= 1;
        }
        else
        {
            break;
        }
    }

    if (len == 0 || strncmp(path, ".", len) == 0)
    {
        return strdup("../");
    }

    size_t prev_separator = len - 1;
    while (prev_separator > 0)
    {
        if (is_path_sep(path[prev_separator]))
        {
            break;
        }
        else
        {
            prev_separator -= 1;
        }
    }

    if (prev_separator == 0)
    {
        return strdup("./");
    }

    char* parent = (char*)malloc(prev_separator + 2);
    memcpy(parent, path, prev_separator);
    parent[prev_separator] = '/';
    parent[prev_separator + 1] = '\0';
    return parent;
}

char* path_join(const char* left, const char* right)
{
    if (!left && !right)
    {
        return strdup(".");
    }
    else if (!left)
    {
        return strdup(right);
    }
    else if (!right)
    {
        return strdup(left);
    }

    size_t left_len = strlen(left);
    size_t right_len = strlen(right);

    while (left_len > 0 && is_path_sep(left[left_len - 1]))
    {
        left_len -= 1;
    }

    while (right_len > 0 && is_path_sep(right[0]))
    {
        right_len -= 1;
        right += 1;
    }

    char* result = (char*)malloc(left_len + right_len + 2);
    memcpy(result, left, left_len);
    result[left_len] = '/';
    memcpy(result + left_len + 1, right, right_len);
    result[left_len + right_len + 1] = '\0';

    return result;
}
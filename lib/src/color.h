#ifndef COLOR_H_INCLUDED
#define COLOR_H_INCLUDED

/** \file
 * 
 * Utility types and functions for working with colors.
 */

#include "common.h"
#include <stdint.h>
#include <math.h>

C_HEADER_BEGIN

/** \addtogroup Utilities
 * @{
 */

/** \brief RGB color, with floating point values.
 * 
 * Members represent intensities of red, green and blue.
 * 
 * When used for surface reflectance, each value should
 * be in range [0, 1].
 * 
 * When used for light intensity, each values can be (and,
 * usually, is) greater than 1.
 */ 
typedef struct
{
    float r, g, b;
} color_t;

/** \brief RGB color, with 8 bit values.
 * 
 * Members represent intensities of red green and blue.
 * Each value can be in range [0, 255].
 * 
 * This type is used for pixels in output images.
 */
typedef struct
{
    uint8_t r, g, b;
} u8_color_t;

//! One-liner for initializing floating-point RGB color value.
static inline color_t make_color(float r, float g, float b)
{
    color_t res;
    res.r = r;
    res.g = g;
    res.b = b;
    return res;
}

//! One-liner for initializing 8 bit RGB color value.
static inline u8_color_t make_u8_color(uint8_t r, uint8_t g, uint8_t b)
{
    u8_color_t res;
    res.r = r;
    res.g = g;
    res.b = b;
    return res;
}

//! Converts 8 bit color to floating point color value.
static inline color_t color_from_u8(const u8_color_t c)
{ return make_color(c.r / 255.0f, c.g / 255.0f, c.b / 255.0f); }

/** Converts floating point color to 8 bit color value.
 * 
 * Values in range [0, 1] are mapped to [0, 255].
 * 
 * Values outside [0, 1] are clamped to [0, 1] and
 * then mapped to [0, 255].
 */
static inline u8_color_t color_to_u8(const color_t c)
{
    return make_u8_color(
        (uint8_t)(fminf(1.0f, fmaxf(0.0f, c.r)) * 255.0f),
        (uint8_t)(fminf(1.0f, fmaxf(0.0f, c.g)) * 255.0f),
        (uint8_t)(fminf(1.0f, fmaxf(0.0f, c.b)) * 255.0f)
    );
}

//! Returns channel-wise sum of two color values.
static inline color_t color_add(const color_t lhs, const color_t rhs)
{
    return make_color(lhs.r + rhs.r, lhs.g + rhs.g, lhs.b + rhs.b);
}

//! Returns color with each channel multiplied by scale.
static inline color_t color_mul(const color_t color, float scale)
{
    return make_color(color.r * scale, color.g * scale, color.b * scale);
}

//! Multiplies respective channels of two color values.
static inline color_t color_mul_rgb(const color_t lhs, const color_t rhs)
{
    return make_color(lhs.r * rhs.r, lhs.g * rhs.g, lhs.b * rhs.b);
}

static inline void color_add_scaled(color_t* lhs, const color_t rhs, float rhs_scale)
{
    lhs->r += rhs.r * rhs_scale;
    lhs->g += rhs.g * rhs_scale;
    lhs->b += rhs.b * rhs_scale;
}

/** @} */

C_HEADER_END

#endif /* COLOR_H_INCLUDED */

#include "trimesh.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

struct trimesh_t
{
    uint32_t ref_count;
    bool is_compiled;
    uint32_t num_vertices;
    uint32_t num_faces;
    tm_vertex_t* vertices;
    tm_face_t* faces;
    tri_bvh_t* bvh_accel;
};

trimesh_t* trimesh_create()
{
    trimesh_t* tm = (trimesh_t*)malloc(sizeof(trimesh_t));
    if (!tm)
    {
        return NULL;
    }

    tm->ref_count = 1;
    tm->is_compiled = false;
    tm->num_vertices = 0;
    tm->num_faces = 0;
    tm->vertices = NULL;
    tm->faces = NULL;
    tm->bvh_accel = NULL;

    return tm;
}

void trimesh_add_ref(trimesh_t* tm)
{
    if(tm)
    {
        ++tm->ref_count;
    }
}

static void _trimesh_free(trimesh_t* tm)
{
    if (tm)
    {
        if (tm->vertices)
        {
            free(tm->vertices);
        }

        if (tm->faces)
        {
            free(tm->faces);
        }

        tri_bvh_free(tm->bvh_accel);

        free(tm);
    }
}

void trimesh_release(trimesh_t* tm)
{
    if(tm && --tm->ref_count == 0)
    {
        _trimesh_free(tm);
    }
}

uint32_t trimesh_add_vertex(trimesh_t* tm, const tm_vertex_t* v)
{
    if (!tm || !v || tm->is_compiled)
    {
        abort();
    }

    tm->vertices = (tm_vertex_t*)realloc(tm->vertices, (tm->num_vertices + 1) * sizeof(tm_vertex_t));
    if (!tm->vertices)
    {
        abort();
    }

    tm->vertices[tm->num_vertices] = *v;

    return tm->num_vertices++;
}

void trimesh_add_face(trimesh_t* tm, const tm_face_t* f)
{
    if (!tm || !f || tm->is_compiled)
    {
        abort();
    }

    tm->faces = (tm_face_t*)realloc(tm->faces, (tm->num_faces + 1) * sizeof(tm_face_t));
    if (!tm->faces)
    {
        abort();
    }

    tm->faces[tm->num_faces++] = *f;
}

void trimesh_compile(trimesh_t* tm)
{
    tm->is_compiled = true;

    tm->bvh_accel = tri_bvh_create(tm->num_vertices, tm->vertices, tm->num_faces, tm->faces);
}

bbox_t trimesh_bbox(const trimesh_t* tm)
{
    assert(tm && tm->is_compiled);
    return tri_bvh_bbox(tm->bvh_accel);
}

bool ray_trimesh_intersect(const ray_t* ray, const trimesh_t* tm,
                           surf_point_t* point, float* out_t)
{
    assert(ray && tm && point && out_t);

    if (!tm->is_compiled)
    {
        abort();
    }

    uint32_t face_index = UINT32_MAX;
    if (!tri_bvh_closest_intersection(tm->bvh_accel, ray, &face_index))
    {
        return false;
    }
    
    const uint32_t* tri_inds = tm->faces[face_index].indices;

    const vec3_t positions[] = {
        tm->vertices[tri_inds[0]].position,
        tm->vertices[tri_inds[1]].position,
        tm->vertices[tri_inds[2]].position
    };

    vec3_t tuv;
    if (!ray_triangle_intersection(ray, positions, &tuv))
    {
        return false;
    }

    const vec3_t weights = make_vec3(1.0f - tuv.y - tuv.z, tuv.y, tuv.z);
    
    point->position = vec3_blend3(positions[0], positions[1], positions[2], weights);

    point->normal = vec3_blend3(tm->vertices[tri_inds[0]].normal,
                                tm->vertices[tri_inds[1]].normal,
                                tm->vertices[tri_inds[2]].normal,
                                weights);

    point->uv = vec2_blend3(tm->vertices[tri_inds[0]].uv,
                            tm->vertices[tri_inds[1]].uv,
                            tm->vertices[tri_inds[2]].uv,
                            weights);

    *out_t = tuv.x;

    return true;
}

bool segment_trimesh_test(const vec3_t from, const vec3_t to, const trimesh_t* tm)
{
    if(!tm || !tm->is_compiled)
    {
        abort();
    }

    return tri_bvh_any_intersection(tm->bvh_accel, from, to);
}
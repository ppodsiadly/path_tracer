#ifndef CONTAINERS_H_INCLUDED
#define CONTAINERS_H_INCLUDED

/** \file
 * 
 * Basic data structures.
 * 
 * Currently, only generic, dynamic array is implemented.
 */

#include "common.h"
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

C_HEADER_BEGIN

/** \addtogroup Utilities
 * @{
 */

typedef struct array_t array_t;

array_t* array_create(uint32_t elem_size);
void array_free(array_t* array);

void array_reserve(array_t* array, uint32_t new_capacity);

void array_resize(array_t* array, uint32_t new_size);

void array_append(array_t* array, const void* new_elem);

uint32_t array_size(const array_t* array);
void* array_data(array_t* array);

void* array_get(array_t* arr, uint32_t index);
void array_set(array_t* arr, uint32_t index, const void* elem);

/** \cond FALSE */
#define CONCAT_IMPL(a, b) a##b
#define CONCAT(a, b) CONCAT_IMPL(a, b)
/** \endcond */

/** Helper macro for iterating over \ref array_t
 * 
 * \param arr The array.
 * \param elem_type Type of array elements.
 * \param var Name of the variable which will be assigned a value of an element.
 * 
 * Example: 
 * \code
 * array_t* array_of_ints = array_create(sizeof(int));
 * 
 * int value = 1;
 * array_append(array_of_ints, &value);
 * 
 * value = 2;
 * array_append(array_of_ints, &value);
 * 
 * value = 3;
 * array_append(array_of_ints, &value);
 * 
 * ARRAY_FOREACH(array_of_ints, int, elem)
 * {
 *     // print all elements of the array
 *     printf("%d ", elem);
 * }
 * \endcode
 */
#define ARRAY_FOREACH(arr, elem_type, var) \
    const uint32_t CONCAT(_arr_size, __LINE__) = array_size(arr);\
    elem_type* const CONCAT(_arr_data, __LINE__) = (elem_type*)array_data(arr); \
    uint32_t CONCAT(_arr_foreach_i, __LINE__) = 0; \
    for(elem_type* var = CONCAT(_arr_data, __LINE__); \
        CONCAT(_arr_foreach_i, __LINE__) < CONCAT(_arr_size, __LINE__); \
        var = &CONCAT(_arr_data, __LINE__)[++CONCAT(_arr_foreach_i, __LINE__)])

/** @} */

C_HEADER_END

#endif /* CONTAINERS_H_INCLUDED */
#ifndef PRNG_H_INCLUDED
#define PRNG_H_INCLUDED

#include "vec_math.h"
#include <stdint.h>

#ifdef _MSC_VER
# include <intrin.h>
#endif

/** \addtogroup Utilities
 * @{
 */

typedef struct
{
    uint64_t state;
} prng_t;

static inline prng_t prng_init(uint64_t seed)
{
    prng_t prng;
    prng.state = seed;
    return prng;
}

static inline uint64_t _wymum(uint64_t a, uint64_t b)
{
#if defined(_MSC_VER)
    a = _umul128(a, b, &b);
    return a ^ b;
#elif defined(__GNUC__)
    __uint128_t	r = a;
    r *= b;
    return (uint64_t)((r >> 64) ^ r);
#endif
}

static inline uint64_t prng_uint64(prng_t* prng)
{
    prng->state += UINT64_C(0xa0761d6478bd642f);
    return _wymum(prng->state ^ UINT64_C(0xe7037ed1a0b428db), prng->state);
}

static inline float prng_float(prng_t* prng)
{
    return ((float)prng_uint64(prng)) / UINT64_MAX;
}

static vec2_t prng_vec2(prng_t* prng)
{
    const float x = prng_float(prng);
    const float y = prng_float(prng);
    return make_vec2(x, y);
}

static inline vec2_t sample_disk(const vec2_t u)
{
    const float r = sqrtf(u.x);
    const float theta = 2.0f * PI_f * u.y;
    return make_vec2(r * cosf(theta), r * sinf(theta));
}

static inline vec3_t sample_cos_hemisphere(const vec2_t u)
{
    const vec2_t d = sample_disk(u);
    const float y = sqrtf(fmaxf(0.0f, 1.0f - d.x * d.x + d.y * d.y));
    return make_vec3(d.x, y, d.y);
}

/** @} */

#endif /* PRNG_H_INCLUDED */

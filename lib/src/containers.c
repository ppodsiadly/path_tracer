#include "containers.h"
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

struct array_t
{
    uint32_t size;
    uint32_t capacity;
    uint32_t elem_size;
    uint8_t* data;
};

array_t* array_create(uint32_t elem_size)
{
    array_t* arr = (array_t*)malloc(sizeof(array_t));
    if (!arr)
    {
        return NULL;
    }

    arr->size = 0;
    arr->capacity = 0;
    arr->elem_size = elem_size;
    arr->data = NULL;

    return arr;
}

void array_free(array_t* array)
{
    if (array)
    {
        if (array->data)
        {
            free(array->data);
        }

        free(array);
    }
}

static uint32_t _compute_new_capacity(uint32_t current_capacity)
{
    if (current_capacity == 0)
    {
        return 8;
    }
    else if (current_capacity < 1024)
    {
        return current_capacity * 2;
    }
    else
    {
        return current_capacity + 1024;
    }
}

void array_reserve(array_t* array, uint32_t new_capacity)
{
    if (!array || (array->capacity >= new_capacity))
    {
        return;
    }

    while (array->capacity < new_capacity)
    {
        array->capacity = _compute_new_capacity(array->capacity);
    }

    void* new_data = realloc(array->data, array->capacity * array->elem_size);
    if (!new_data)
    {
        abort();
    }

    array->data = new_data;
}

void array_resize(array_t* array, uint32_t new_size)
{
    if (!array || (new_size == array->size))
    {
        return;
    }

    if (new_size > array->size)
    {
        array_reserve(array, new_size);
        memset(array->data + (array->size * array->elem_size),
               0,
               (new_size - array->size) * array->elem_size);
        array->size = new_size;
    }
    else
    {
        array->size = new_size;
    }
}

void array_append(array_t* array, const void* new_elem)
{
    if (!array || !new_elem)
    {
        return;
    }

    array_reserve(array, array->size + 1);
    memcpy(array->data + array->size * array->elem_size, new_elem, array->elem_size);
    array->size += 1;
}

uint32_t array_size(const array_t* array)
{
    if (array)
    {
        return array->size;
    }

    return 0;
}

void* array_data(array_t* array)
{
    return array->data;
}

void* array_get(array_t* arr, uint32_t index)
{
    if (!arr || arr->size <= index)
    {
        return NULL;
    }

    return arr->data + (index * arr->elem_size);
}

void array_set(array_t* arr, uint32_t index, const void* elem)
{
    if (!arr || !elem || arr->size <= index)
    {
        return;
    }

    memcpy(arr->data + (index * arr->elem_size), elem, arr->elem_size);
}
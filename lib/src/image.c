#include "image.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct image_t
{
    uint32_t width;
    uint32_t height;
    u8_color_t* rgb_pixels;
};

image_t* image_create(uint32_t width, uint32_t height)
{
    image_t* img = (image_t*)malloc(sizeof(image_t));

    if(!img)
    {
        return NULL;
    }

    img->width = width;
    img->height = height;
    img->rgb_pixels = (u8_color_t*)malloc(sizeof(u8_color_t) * width * height);

    if(!img->rgb_pixels)
    {
        free(img);
        return NULL;
    }

    memset(img->rgb_pixels, 0, width * height * sizeof(u8_color_t));

    return img;
}

void image_free(image_t* img)
{
    if(img)
    {
        if(img->rgb_pixels)
        {
            free(img->rgb_pixels);
        }

        free(img);
    }
}

uint32_t image_get_width(const image_t* img)
{
    return img->width;
}

uint32_t image_get_height(const image_t* img)
{
    return img->height;
}

const u8_color_t* image_get_image_data(const image_t* img)
{
    return img->rgb_pixels;
}

void image_set_pixel(image_t* img,
                     uint32_t x, uint32_t y,
                     const u8_color_t color)
{
    u8_color_t* dest = &img->rgb_pixels[y * img->width + x];
    *dest = color;
}

#ifndef BVH_H_INCLUDED
#define BVH_H_INCLUDED

/** \file
 * 
 * Implementation of Bounding Volume Hieratchy data structure.
 * 
 * Bounding volume hierarchy is a spatial data structure which
 * accelerates 3D intersection tests.
 */

#include "vec_math.h"

C_HEADER_BEGIN

/** \addtogroup bvh Bounding Volume Hierarchy
 * @{
 */

typedef struct bvh_t bvh_t;

typedef bool(*bvh_isect_test_fn)(const ray_t* ray, uint32_t prim_index, float* out_t, void* param);

bvh_t* bvh_create(uint32_t num_primitives, const bbox_t* prim_boxes,
                  bvh_isect_test_fn test_fn, void* test_fn_param);

void bvh_free(bvh_t* bvh);

bbox_t bvh_root_bbox(const bvh_t* bvh);

bool bvh_any_intersection(const bvh_t* bvh, const vec3_t from, const vec3_t to);

bool bvh_closest_intersection(const bvh_t* bvh, const ray_t* ray, uint32_t* out_index, float* out_t);

/** @} */

C_HEADER_END

#endif /* BVH_H_INCLUDED */
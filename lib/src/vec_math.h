#ifndef VEC_MATH_H_INCLUDED
#define VEC_MATH_H_INCLUDED

#include "common.h"
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <float.h>

C_HEADER_BEGIN

/** \addtogroup Utilities
 * @{
 */

//! Pi constant, as a float literal.
#define PI_f     3.14159265359f

//! 1/pi constant as a float literal.
#define INV_PI_f 0.31830988618f

static inline float deg_to_rad(float degrees)
{
    return PI_f * degrees / 180.0f;
}

inline float clamp(float x, float min_x, float max_x)
{
    return fmaxf(min_x, fminf(max_x, x));
}

typedef struct
{
    float x, y;
} vec2_t;

static inline vec2_t make_vec2(float x, float y)
{
    vec2_t v;
    v.x = x;
    v.y = y;
    return v;
}

typedef struct
{
    float x, y, z;
} vec3_t;

static inline vec3_t make_vec3(float x, float y, float z)
{
    vec3_t res;
    res.x = x;
    res.y = y;
    res.z = z;
    return res;
}

//! Returns value of (v1 + v2).
static inline vec3_t VECCALL vec3_add(const vec3_t lhs, const vec3_t rhs)
{ return make_vec3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }

//! Returns value of (v1 - v2).
static inline vec3_t VECCALL vec3_sub(const vec3_t lhs, const vec3_t rhs)
{ return make_vec3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); }

//! Returns vector with each component multiplied by a scalar.
static inline vec3_t VECCALL vec3_mul(const vec3_t lhs, float rhs)
{ return make_vec3(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs); }

//! Returns [x1 * x2, y1 * y2, z1 * z2].
static inline vec3_t VECCALL vec3_mul_xyz(const vec3_t lhs, const vec3_t rhs)
{ return make_vec3(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z); }

//! Returns [1/x, 1/y, 1/z].
static inline vec3_t VECCALL vec3_inv(const vec3_t v)
{ return make_vec3(1.0f / v.x, 1.0f / v.y, 1.0f / v.z); }

//! Dot product of two vectors.
static inline float VECCALL vec3_dot(const vec3_t lhs, const vec3_t rhs)
{ return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z; }

//! Squared length of a vector.
static inline float VECCALL vec3_squared_length(const vec3_t v)
{ return v.x * v.x + v.y * v.y + v.z * v.z; }

//! Length of a vector.
static inline float VECCALL vec3_length(const vec3_t v)
{ return sqrtf(vec3_squared_length(v)); }

//! Distance between two points.
static inline float VECCALL vec3_distance(const vec3_t v0, const vec3_t v1)
{ return vec3_length(vec3_sub(v0, v1)); }

//! Returns normalized vector.
static inline vec3_t VECCALL vec3_normalize(const vec3_t v)
{
    const float inv_length = 1.0f / vec3_length(v);
    return vec3_mul(v, inv_length);
}

//! Cross product of two vectors.
static inline vec3_t VECCALL vec3_cross(const vec3_t lhs, const vec3_t rhs)
{
    return make_vec3(
        lhs.y * rhs.z - lhs.z * rhs.y,
        lhs.z * rhs.x - lhs.x * rhs.z,
        lhs.x * rhs.y - lhs.y * rhs.x);
}

/** \brief Linearly interpolated between two vectors.
 * 
 * \param v0 The first vector.
 * \param v1 The second vector.
 * \param t Parameter, in range [0, 1].
 */
static inline vec3_t VECCALL vec3_lerp(const vec3_t v0, const vec3_t v1, float t)
{
    return vec3_add(vec3_mul(v0, 1.0f - t), vec3_mul(v1, t));
}

/** \brief Returns vector reflected from a surface with given normal.
 * 
 * \arg v Incident vector.
 * \arg n Normal vector of a surface.
 */
static inline vec3_t VECCALL vec3_reflect(const vec3_t v, const vec3_t n)
{
    return vec3_sub(v, vec3_mul(n, 2.0f * vec3_dot(v, n)));
}

static inline vec2_t vec2_blend3(const vec2_t x, const vec2_t y, const vec2_t z, const vec3_t w)
{
	const vec3_t xxx = make_vec3(x.x, y.x, z.x);
	const vec3_t yyy = make_vec3(x.y, y.y, z.y);

	return make_vec2(vec3_dot(xxx, w), vec3_dot(yyy, w));
}

static inline vec3_t VECCALL vec3_blend3(const vec3_t x, const vec3_t y, const vec3_t z, const vec3_t w)
{
    return vec3_add(vec3_mul(x, w.x), vec3_add(vec3_mul(y, w.y), vec3_mul(z, w.z)));
}

static inline vec3_t VECCALL vec3_min(const vec3_t a, const vec3_t b)
{
    return make_vec3(get_min(a.x, b.x), get_min(a.y, b.y), get_min(a.z, b.z));
}

static inline vec3_t VECCALL vec3_max(const vec3_t a, const vec3_t b)
{
    return make_vec3(get_max(a.x, b.x), get_max(a.y, b.y), get_max(a.z, b.z));
}

/** \brief Returns index of component with the largest absolute value.
 * 
 */
int vec3_max_extent(const vec3_t v);

typedef struct
{
    vec3_t origin;
    vec3_t direction;
} ray_t;

static inline ray_t VECCALL make_ray_s(const vec3_t origin, const vec3_t direction)
{
    ray_t res;
    res.origin = origin;
    res.direction = vec3_normalize(direction);
    return res;
}

/** \brief Properties of a geometric surface at a point.
 *
 * This structure is typically set by intersection test
 * functions, such as \ref ray_sphere_intersection and
 * contains parameters needed when evaluating light
 * reflected from a surface at given point.
 */
typedef struct
{
    //! Position of the point.
    vec3_t position;
    //! Normal of a surface at the point.
    vec3_t normal;
    /** \brief Parameteric coordinates of the point.
     * 
     * Currently not used directly. Can be used, for example,
     * for texturing.
     */
    vec2_t uv;
    //! Derivative of position over U paramter.
    vec3_t dpdu;
    //! Derivative of position over V parameter.
    vec3_t dpdv;
} surf_point_t;

bool ray_project(const ray_t* r, const vec3_t* p, vec3_t* pp);

/** \brief Checks for an intersection of a ray with a sphere centered at [0, 0, 0].
 * 
 * \param ray The ray.
 * \param radius Radius of the sphere.
 * \param point Output, describes the point of intersection.
 *              Modified only if an intersection is found.
 * \param isect_t Distance, along the ray, from ray's origin to the intersection point.
 *                Modified only if an intersection is found.
 * 
 * \return True if the ray intersects the sphere, false otherwise.
 */
bool ray_sphere_intersection(const ray_t* ray,
                             float radius,
                             surf_point_t* point, float* isect_t);

/** \brief Checks for intersection of a ray with a rectangle in the XY plane, centered at [0, 0, 0].
 * 
 * \param ray The ray.
 * \param half_size Half of the size of the rectangle.
 * \param point Output, describes the point of intersection.
 *              Modified only if an intersection is found.
 * \param isect_t Distance, along the ray, from ray's origin to the intersection point.
 *                Modified only if an intersection is found.
 * 
 * \return True if an intersection exists, false otherwise.
 */
bool ray_xy_plane_intersection(const ray_t* ray,
                               const vec2_t half_size,
                               surf_point_t* point, float* isect_t);

/** \brief Returns determinant of a 3x3 matrix.
 * 
 * \param c0 The first column.
 * \param c1 The second column.
 * \param c2 The third column.
 */
static inline float VECCALL mat3_determinant(const vec3_t c0, const vec3_t c1, const vec3_t c2)
{
    return vec3_dot(c0, vec3_cross(c1, c2));
}

/** \brief Checks for intersection of a ray with a triangle.
 * 
 * \param ray The ray.
 * \param tri Array of three points of the triangle.
 * \param tuv Parametric cooridnates of the intersection.
 *            `tuv->x` is set to distance, along the ray, from ray's origin to the intersection.
 *            `tuv->y` is set to the value of U at the intersection.
 *            `tuv->z` is set to the value of V at the intersection.
 * 
 * \return True if an intersection exists, false otherwise.
 */
static inline bool ray_triangle_intersection(const ray_t* ray, const vec3_t* tri, vec3_t* tuv)
{
    const vec3_t e1 = vec3_sub(tri[1], tri[0]);
    const vec3_t e2 = vec3_sub(tri[2], tri[0]);
    const vec3_t o = vec3_sub(ray->origin, tri[0]);

    const float d = -mat3_determinant(ray->direction, e1, e2);

    if (d == 0.0f)
    {
        return false;
    }

    const vec3_t dXYZ = make_vec3(mat3_determinant(o, e1, e2),
        -mat3_determinant(ray->direction, o, e2),
        -mat3_determinant(ray->direction, e1, o));

    *tuv = vec3_mul(dXYZ, 1.0f / d);

    return (tuv->x >= 0.0f)
        && (tuv->y >= 0.0f)
        && (tuv->z >= 0.0f)
        && (tuv->y + tuv->z <= 1.0f);
}

/** \brief Quick intersection test between a segment and a sphere centered at [0, 0, 0].
 * 
 * This function is used to determine visibility between two points in \ref world_test_visibility.
 * It is faster than \ref ray_sphere_intersection, since it does not compute intersection parameters.
 * 
 * \param from The first point of the segment.
 * \param to The second point of the segment.
 * \param radius Radius of the sphere.
 * 
 * \return True if an intersection exists.
 */
bool segment_sphere_test(const vec3_t from, const vec3_t to, float radius);

/** \brief Quick intersection test between a segment and a rectangle in the XY plane, centered at [0, 0, 0].
 * 
 * This function is used to determine visibility between two points in \ref world_test_visibility.
 * 
 * \param from The first point of the segment.
 * \param to The second point of the segment.
 * \param half_size Half of the size of the rectangle.
 * 
 * \return True if an intersection exists.
 */
static inline bool VECCALL segment_xy_plane_test(const vec3_t from, const vec3_t to, const vec2_t half_size)
{
    if(signbit(from.z) == signbit(to.z))
    {
        return false;
    }

    const float t = -to.z / (from.z - to.z);

    const vec2_t xy = make_vec2(fabsf(from.x * t + to.x * (1.0f - t)), fabsf(from.y * t + to.y * (1.0f - t)));

    return xy.x <= half_size.x && xy.y <= half_size.y;
}

typedef struct
{
    vec3_t tangent;
    vec3_t normal;
    vec3_t bitangent;
} basis_t;

static inline void VECCALL basis_init(const vec3_t normal, const vec3_t tangent, basis_t* basis)
{
    basis->tangent = tangent;
    basis->normal = normal;
    basis->bitangent = vec3_cross(normal, tangent);
}

static inline vec3_t VECCALL basis_map_from(const basis_t* b, const vec3_t v)
{
    const vec3_t x = make_vec3(b->tangent.x, b->normal.x, b->bitangent.x);
    const vec3_t y = make_vec3(b->tangent.y, b->normal.y, b->bitangent.y);
    const vec3_t z = make_vec3(b->tangent.z, b->normal.z, b->bitangent.z);

    return make_vec3(vec3_dot(x, v), vec3_dot(y, v), vec3_dot(z, v));
}

static inline vec3_t VECCALL basis_map_to(const basis_t* b, const vec3_t v)
{
    return make_vec3(vec3_dot(b->tangent, v),
                     vec3_dot(b->normal, v),
                     vec3_dot(b->bitangent, v));
}

typedef struct {
    float m[4][4];
} mat4_t;

void mat4_identity(mat4_t* m);

void mat4_from_translation(mat4_t* result, const vec3_t vector);

void mat4_from_axis_angle(mat4_t* result, const vec3_t axis, float angle);

void mat4_from_scale(mat4_t* m, const vec3_t scale);

void mat4_multiply(mat4_t* result, const mat4_t* lhs, const mat4_t* rhs);

float mat4_det(const mat4_t* m);

void mat4_invert(const mat4_t* m, mat4_t* inv_m);

vec3_t mat4_point_tform(const mat4_t* m, const vec3_t v);

vec3_t mat4_vector_tform(const mat4_t* m, const vec3_t v);

void mat4_ray_tform(const mat4_t* m, const ray_t* ray, ray_t* out_ray);

/** \brief Axis-aligned bounding box.
 */
typedef struct
{
    vec3_t center;
    vec3_t half_extent;
} bbox_t;

static inline bbox_t bbox_from_points(uint32_t num_points, const vec3_t* points)
{    
    vec3_t min_pos = points[0];
    vec3_t max_pos = points[0];

    for (uint32_t i = 1; i < num_points; ++i)
    {
        min_pos = vec3_min(min_pos, points[i]);
        max_pos = vec3_max(max_pos, points[i]);
    }

    bbox_t bb;
    bb.center = vec3_mul(vec3_add(min_pos, max_pos), 0.5f);
    bb.half_extent = vec3_sub(max_pos, bb.center);
    return bb;
}

static inline bbox_t bbox_union(const bbox_t* a, const bbox_t* b)
{
    const vec3_t min_pos = vec3_min(vec3_sub(a->center, a->half_extent), vec3_sub(b->center, b->half_extent));
    const vec3_t max_pos = vec3_max(vec3_add(a->center, a->half_extent), vec3_add(b->center, b->half_extent));
    
    bbox_t bb;
    bb.center = vec3_mul(vec3_add(min_pos, max_pos), 0.5f);
    bb.half_extent = vec3_sub(max_pos, bb.center);
    return bb;
}

static inline float fp_gamma(int n)
{
    return (n * FLT_EPSILON) / (1.0f - n * FLT_EPSILON);
}

static inline bool bbox_ray_test(const bbox_t* bb, const ray_t* r, vec2_t* opt_out_t)
{
    const vec3_t center = vec3_sub(bb->center, r->origin);    
    const vec3_t min_p = vec3_sub(center, bb->half_extent);
    const vec3_t max_p = vec3_add(center, bb->half_extent);

    const vec3_t inv_d = vec3_inv(r->direction);

    const vec3_t t_0 = vec3_mul_xyz(min_p, inv_d);
    const vec3_t t_1 = vec3_mul_xyz(max_p, inv_d);

    const vec3_t t_min = vec3_min(t_0, t_1);
    const vec3_t t_max = vec3_max(t_0, t_1);

    float near_t = 0.0f, far_t = FLT_MAX;
    near_t = get_max(t_min.x, get_max(t_min.y, get_max(t_min.z, near_t)));
    far_t = get_min(t_max.x, get_min(t_max.y, get_min(t_max.z, far_t))) * (1.0f + 2.0f * fp_gamma(3));

    if (opt_out_t)
    {
        opt_out_t->x = near_t;
        opt_out_t->y = far_t;
    }

    return near_t <= far_t;
}

bbox_t bbox_from_sphere(float radius);
bbox_t bbox_from_xy_rect(const vec2_t half_size);

bbox_t mat4_bbox_tform(const mat4_t* m, const bbox_t bbox);

/** @} */

C_HEADER_END

#endif /* VEC_MATH_H_INCLUDED */

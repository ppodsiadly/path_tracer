#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#ifdef _WIN32
# include <Windows.h>
#endif

static void _output_debug_str(const char* level_str, const char* tag, const char* msg)
{
#ifdef _WIN32

    char buffer[256];
    snprintf(buffer, 255, "[%s @ %s] %s\n", level_str, tag, msg);
    OutputDebugStringA(buffer);

#else
    ((void)level_str);
    ((void)tag);
    ((void)msg);
#endif
}

void log_message(log_level_t level, const char* tag, const char* msg, ...)
{
    const char* level_str = NULL;
    switch (level)
    {
        case LOG_WARN:
            level_str = "Warn";
            break;
        case LOG_INFO:
            level_str = "Info";
            break;
        case LOG_ERROR:
            level_str = "Error";
            break;
    }

    if (!level_str || !tag || !msg)
    {
        fprintf(stderr, "log_message was called with invalid arguments, aborting!\n");
        abort();
    }

    char buffer[256];

    va_list list;
    va_start(list, msg);
    vsnprintf(buffer, 255, msg, list);
    buffer[255] = '\0';
    va_end(list);

    fprintf(stderr, "[%s @ %s] %s\n", level_str, tag, buffer);
    fflush(stderr);

    _output_debug_str(level_str, tag, buffer);
}
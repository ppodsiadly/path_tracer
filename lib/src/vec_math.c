#include "vec_math.h"
#include <string.h>
#include <assert.h>

int vec3_max_extent(const vec3_t v)
{
    const vec3_t av = make_vec3(fabsf(v.x), fabsf(v.y), fabsf(v.z));

    if ((av.x > av.y) && (av.x > av.z))
    {
        return 0;
    }
    else if (av.y > av.z)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

bool ray_project(const ray_t* r, const vec3_t* p, vec3_t* pp)
{
    const float proj_len = vec3_dot(vec3_sub(*p, r->origin), r->direction);

    if(proj_len >= 0.0f)
    {
        *pp = vec3_add(r->origin, vec3_mul(r->direction, proj_len));
        return true;
    }
    else
    {
        return false;
    }    
}

bool ray_sphere_intersection(const ray_t* ray,
                             float radius,
                             surf_point_t* point, float* isect_t)
{
    const float a = vec3_dot(ray->direction, ray->direction);
    const float b = 2.0f * vec3_dot(ray->direction, ray->origin);
    const float c = vec3_dot(ray->origin, ray->origin) - radius * radius;

    const float d = b * b - 4.0f * a * c;

    if(d >= 0.0f)
    {
        const float t0 = (-b - sqrtf(d)) / 2.0f * a;
        const float t1 = (-b + sqrtf(d)) / 2.0f * a;

        float t;

        if(t0 >= 0.0f && t1 >= 0.0f)
        {
            t = fminf(t0, t1);
        }
        else if(fmaxf(t0, t1) >= 0.0f)
        {
            t = fmaxf(t0, t1);
        }
        else
        {
            return false;
        }
        
        point->position = vec3_add(ray->origin, vec3_mul(ray->direction, t));

        const vec3_t n = vec3_normalize(point->position);
        point->normal = n;

        const float phi = atan2f(n.z, n.x);
        const float theta = acosf(n.z);

        point->uv = make_vec2(phi / (2.0f * PI_f), theta / PI_f + 0.5f);

        point->dpdu = make_vec3(-2.0f * PI_f * point->position.z, 0.0f, PI_f / 2.0f * point->position.x);

        const float cos_phi = cosf(phi);
        const float sin_phi = sinf(phi);
        point->dpdv = make_vec3(point->position.y * cos_phi * PI_f, -radius * sinf(theta), point->position.y * sin_phi);

        *isect_t = t;

        return true;
    }

    return false;
}

bool ray_xy_plane_intersection(const ray_t* ray, const vec2_t half_size, surf_point_t* point, float* isect_t)
{
    /*
     * (rx + t*dx) * a + (ry + t*dy) * b + (rz + t*dz) * c + d = 0
     * a*rx + t*a*dx + b*ry + t*b*dy + c*rz + t*c*dz = 0
     * t * (a*dx + b*dy + c*dz) = -a*rx - b*ry - c*rz - d
     * t = (-a*rx - b*ry - c*rz - d) / (a*dx + b*dy + c*dz)
     */

    if(ray->direction.z != 0.0f)
    {
        const float t = -ray->origin.z / ray->direction.z;

        if(t >= 0.0f)
        {
            const vec3_t p = vec3_add(ray->origin, vec3_mul(ray->direction, t));
            if(fabsf(p.x) > half_size.x || fabsf(p.y) > half_size.y)
            {
                return false;
            }

            point->position = p;
            point->normal = make_vec3(0.0f, 0.0f, (ray->direction.z < 0.0f) ? 1.0f : -1.0f);
            point->uv = make_vec2(0.0f, 0.0f); /* TODO */
            point->dpdu = make_vec3(1.0f, 0.0f, 0.0f);
            point->dpdv = make_vec3(0.0f, 1.0f, 0.0f);

            *isect_t = t;

            return true;
        }
    }

    return false;
}

bool segment_sphere_test(const vec3_t from, const vec3_t to,
                         float radius)
{
    const vec3_t o = from;
    const vec3_t d = vec3_sub(to, from);

    const float o_dot_d = vec3_dot(o, d);
    const float d_dot_d = vec3_dot(d, d);

    const float delta_over_4 = o_dot_d * o_dot_d - d_dot_d * (vec3_dot(o, o) - radius * radius);
    
    if (delta_over_4 < 0.0f)
    {
        return false;
    }

    const float half_sqrt_delta = sqrtf(delta_over_4);

    const float t0 = (-o_dot_d - half_sqrt_delta) / d_dot_d;
    const float t1 = (-o_dot_d + half_sqrt_delta) / d_dot_d;

    return (t0 >= 0.0f && t0 <= 1.0f) || (t1 >= 0.0f && t1 <= 1.0f);
}

void mat4_identity(mat4_t* m)
{
    for(int row = 0; row < 4; ++row)
    {
        for(int col = 0; col < 4; ++col)
        {
            m->m[row][col] = (row == col) ? 1.0f : 0.0f;
        }
    }
}

void mat4_from_translation(mat4_t* result, const vec3_t vector)
{
    mat4_identity(result);
    result->m[0][3] = vector.x;
    result->m[1][3] = vector.y;
    result->m[2][3] = vector.z;
}

void mat4_from_axis_angle(mat4_t* m, const vec3_t axis, float angle)
{
    const float st = sinf(angle);
    const float ct = cosf(angle);

    const float x = axis.x;
    const float y = axis.y;
    const float z = axis.z;
    
    m->m[0][0] = ct + x * x * (1.0f - ct);
    m->m[0][1] = x * y * (1.0f - ct) -  z * st;
    m->m[0][2] = x * z * (1.0f - ct) + y * st;
    m->m[0][3] = 0.0f;

    m->m[1][0] = y * x * (1.0f - ct) + z * st; 
    m->m[1][1] = ct + y * y * (1.0f - ct);
    m->m[1][2] = y * z * (1.0f - ct) - x * st;
    m->m[1][3] = 0.0f;

    m->m[2][0] = z * x * (1.0f - ct) - y * st;
    m->m[2][1] = z * y * (1.0f - ct) + x * st;
    m->m[2][2] = ct + z * z * (1.0f - ct);
    m->m[2][3] = 0.0f;

    m->m[3][0] = m->m[3][1] = m->m[3][2] = 0.0f;
    m->m[3][3] = 1.0f;
}

void mat4_from_scale(mat4_t* m, const vec3_t scale)
{
    mat4_identity(m);

    m->m[0][0] = scale.x;
    m->m[1][1] = scale.y;
    m->m[2][2] = scale.z;
}

void mat4_multiply(mat4_t* result, const mat4_t* lhs, const mat4_t* rhs)
{
    mat4_t r;

    for(int i = 0; i < 4; ++i)
    {
        for(int j = 0; j < 4; ++j)
        {
            r.m[i][j] = 0.0f;

            for(int k = 0; k < 4; ++k)
            {
                r.m[i][j] += lhs->m[i][k] * rhs->m[k][j];
            }
        }
    }

    memcpy(result, &r, sizeof(mat4_t));
}

static float mat4_minor(const mat4_t* m, uint32_t row, uint32_t col)
{
    float m3[3][3];

    uint32_t m3_row = 0;
    for(uint32_t i = 0; i < 4; ++i)
    {
        if(row != i)
        {
            uint32_t m3_col = 0;
            for(uint32_t j = 0; j < 4; ++j)
            {
                if(col != j)
                {
                    m3[m3_row][m3_col++] = m->m[i][j];
                }
            }
            
            ++m3_row;
        }
    }

    return mat3_determinant(
        make_vec3(m3[0][0], m3[0][1], m3[0][2]),
        make_vec3(m3[1][0], m3[1][1], m3[1][2]),
        make_vec3(m3[2][0], m3[2][1], m3[2][2]));
}

float mat4_det(const mat4_t* m)
{
    float det = 0.0f;

    det += m->m[0][0] * mat4_minor(m, 0, 0);
    det -= m->m[1][0] * mat4_minor(m, 1, 0);
    det += m->m[2][0] * mat4_minor(m, 2, 0);
    det -= m->m[3][0] * mat4_minor(m, 3, 0);

    return det;
}

void mat4_invert(const mat4_t* m, mat4_t* inv_m)
{
    const float d = mat4_det(m);
    assert(d != 0.0f);

    const float inv_d = 1.0f / d;

    for(uint32_t r = 0; r < 4; ++r)
    {
        for(uint32_t c = 0; c < 4; ++c)
        {
            inv_m->m[c][r] = inv_d * powf(-1.0f, (float)(r + c)) * mat4_minor(m, r, c);
        }
    }
}

vec3_t mat4_point_tform(const mat4_t* m, const vec3_t v)
{
    const float coords[4] = { v.x, v.y, v.z, 1.0f };
    float t_coords[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	for (int row = 0; row < 4; ++row)
    {
		for (int col = 0; col < 4; ++col)
        {
            t_coords[row] += m->m[row][col] * coords[col];
        }
    }

    return vec3_mul(make_vec3(t_coords[0], t_coords[1], t_coords[2]), 1.0f / t_coords[3]);
}

vec3_t mat4_vector_tform(const mat4_t* m, const vec3_t v)
{
    const float coords[3] = { v.x, v.y, v.z };
    float t_coords[3] = { 0.0f, 0.0f, 0.0f };

    for(int col = 0; col < 3; ++col)
    {
        for(int row = 0; row < 3; ++row)
        {
            t_coords[row] += m->m[row][col] * coords[col];
        }
    }

    return make_vec3(t_coords[0], t_coords[1], t_coords[2]);
}

void mat4_ray_tform(const mat4_t* m, const ray_t* ray, ray_t* out_ray)
{
    out_ray->origin = mat4_point_tform(m, ray->origin);
    out_ray->direction = mat4_vector_tform(m, ray->direction);
}

bbox_t bbox_from_sphere(float radius)
{
    bbox_t bb;
    bb.center = make_vec3(0.0f, 0.0f, 0.0f);
    bb.half_extent = make_vec3(radius, radius, radius);
    return bb;
}

bbox_t bbox_from_xy_rect(const vec2_t half_size)
{
    bbox_t bb;
    bb.center = make_vec3(0.0f, 0.0f, 0.0f);
    bb.half_extent = make_vec3(half_size.x, half_size.y, 0.0f);
    return bb;
}

bbox_t mat4_bbox_tform(const mat4_t* m, const bbox_t bbox)
{
    vec3_t points[8] = {
         vec3_mul_xyz(bbox.half_extent, make_vec3(1.0f, 1.0f, 1.0f)),
         vec3_mul_xyz(bbox.half_extent, make_vec3(-1.0f, 1.0f, 1.0f)),
         vec3_mul_xyz(bbox.half_extent, make_vec3(-1.0f, 1.0f, -1.0f)),
         vec3_mul_xyz(bbox.half_extent, make_vec3(1.0f, 1.0f, -1.0f)),

         vec3_mul_xyz(bbox.half_extent, make_vec3(1.0f, -1.0f, 1.0f)),
         vec3_mul_xyz(bbox.half_extent, make_vec3(-1.0f, -1.0f, 1.0f)),
         vec3_mul_xyz(bbox.half_extent, make_vec3(-1.0f, -1.0f, -1.0f)),
         vec3_mul_xyz(bbox.half_extent, make_vec3(1.0f, -1.0f, -1.0f))
    };

    for(int i = 0; i < 8; ++i)
    {
        points[i] = mat4_point_tform(m, vec3_add(bbox.center, points[i]));
    }

    return bbox_from_points(8, points);
}
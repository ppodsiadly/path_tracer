#include "render_buf.h"
#include <stdlib.h>
#include <string.h>

struct render_buf_t
{
    uint32_t width;
    uint32_t height;
    color_t* pixels;
};

render_buf_t* render_buf_create(uint32_t width, uint32_t height)
{
    if(width < 1 || height < 1)
    {
        return NULL;
    }

    render_buf_t* rb = (render_buf_t*)malloc(sizeof(render_buf_t));
    if(!rb)
    {
        return NULL;
    }

    rb->width = width;
    rb->height = height;

    rb->pixels = (color_t*)malloc(width * height * sizeof(color_t));
    if(!rb->pixels)
    {
        free(rb);
        return NULL;
    }

    memset(rb->pixels, 0, width * height * sizeof(color_t));

    return rb;
}

void render_buf_free(render_buf_t* rb)
{
    if(rb)
    {
        if(rb->pixels)
        {
            free(rb->pixels);
        }

        free(rb);
    }
}

void render_buf_set_pixel(render_buf_t* rb, uint32_t x, uint32_t y, const color_t color)
{
    rb->pixels[y * rb->width + x] = color;
}

uint32_t render_buf_get_width(const render_buf_t* rb)
{
    return rb->width;
}

uint32_t render_buf_get_height(const render_buf_t* rb)
{
    return rb->height;
}

color_t render_buf_get_pixel(const render_buf_t* rb, uint32_t x, uint32_t y)
{
    return rb->pixels[y * rb->width + x];
}

image_t* render_buf_to_image(const render_buf_t* rb)
{
    image_t* img = image_create(rb->width, rb->height);
    if(!img)
    {
        return NULL;
    }

    for(uint32_t y = 0; y < rb->height; ++y)
    {
        for(uint32_t x = 0; x < rb->width; ++x)
        {
            const color_t c = render_buf_get_pixel(rb, x, y);
            image_set_pixel(img, x, y, color_to_u8(c));
        }
    }

    return img;
}

static const uint32_t tile_dim = 32;

uint32_t render_buf_get_num_tiles(const render_buf_t* rb)
{
    if (!rb)
    {
        return 0;
    }

    const uint32_t n_tiles_x = (rb->width + tile_dim - 1) / tile_dim;
    const uint32_t n_tiles_y = (rb->height + tile_dim - 1) / tile_dim;

    return n_tiles_x * n_tiles_y;
}

void render_buf_get_tile_bounds(const render_buf_t* rb, uint32_t tile_index, rb_tile_bounds_t* bounds)
{
    if (!rb || !bounds)
    {
        return;
    }

    const uint32_t n_tiles_x = (rb->width + tile_dim - 1) / tile_dim;

    const uint32_t x_index = tile_index % n_tiles_x;
    const uint32_t y_index = tile_index / n_tiles_x;

    bounds->min_x = x_index * tile_dim;
    bounds->min_y = y_index * tile_dim;

    bounds->max_x = get_min(rb->width, bounds->min_x + tile_dim) - 1;
    bounds->max_y = get_min(rb->height, bounds->min_y + tile_dim) - 1;
}
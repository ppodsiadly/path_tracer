#include "bvh.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

typedef struct bvh_node_t
{
    bbox_t bbox;

    struct bvh_node_t* left_child;
    struct bvh_node_t* right_child;

    uint32_t prim_index;
} bvh_node_t;

static bvh_node_t* _bvh_node_create(const bbox_t* bbox, uint32_t prim_index)
{
    bvh_node_t* node = (bvh_node_t*)malloc(sizeof(bvh_node_t));
    node->bbox = *bbox;
    node->left_child = node->right_child = NULL;
    node->prim_index = prim_index;
    return node;
}

static void _bvh_node_free(bvh_node_t* node)
{
    if(node->left_child)
    {
        _bvh_node_free(node->left_child);
        _bvh_node_free(node->right_child);
    }

    free(node);
}

typedef bool(*vec3_cmp_fn)(const vec3_t*, const vec3_t*);

static bool _vec3_cmp_x(const vec3_t* lhs, const vec3_t* rhs)
{ return lhs->x < rhs->x; }

static bool _vec3_cmp_y(const vec3_t* lhs, const vec3_t* rhs)
{ return lhs->y < rhs->y; }

static bool _vec3_cmp_z(const vec3_t* lhs, const vec3_t* rhs)
{ return lhs->z < rhs->z; }

typedef struct
{
    bbox_t bbox;
    uint32_t prim_index;
} bvh_item_t;

static void _partition_bboxes(bvh_item_t* items, uint32_t num_items, const vec3_t ref, vec3_cmp_fn cmp_fn)
{
    bvh_item_t* left = items;
    bvh_item_t* right = items + num_items - 1;

    while(left < right)
    {
        if(!cmp_fn(&left->bbox.center, &ref))
        {
            const bvh_item_t prev_left = *left;
            *left = *right;
            *right = prev_left;

            --right;
        }
        else
        {
            ++left;
        }
    }
}

static bvh_node_t* _bvh_build_tree(bvh_item_t* items, uint32_t first_item, uint32_t num_items)
{
    assert(num_items > 0);

    if(num_items == 1)
    {
        const bvh_item_t* item = &items[first_item];
        return _bvh_node_create(&item->bbox, item->prim_index);
    }

    bbox_t node_bbox = items[first_item].bbox;
    for(uint32_t i = first_item + 1; i < first_item + num_items; ++i)
    {
        node_bbox = bbox_union(&node_bbox, &items[i].bbox);
    }

    const int max_axis = vec3_max_extent(node_bbox.half_extent);
    vec3_cmp_fn cmp_fn = NULL;

    switch(max_axis)
    {
        case 0:
            cmp_fn = &_vec3_cmp_x;
            break;
        case 1:
            cmp_fn = &_vec3_cmp_y;
            break;
        case 2:
            cmp_fn = &_vec3_cmp_z;
            break;
    }

    _partition_bboxes(&items[first_item], num_items, node_bbox.center, cmp_fn);

    bvh_node_t* node = _bvh_node_create(&node_bbox, 0);
    node->left_child = _bvh_build_tree(items, first_item, num_items / 2);
    node->right_child = _bvh_build_tree(items, first_item + num_items / 2, num_items - num_items / 2);

    return node;
}

struct bvh_t
{
    bvh_isect_test_fn test_fn;
    void* test_fn_param;

    bvh_node_t* root_node;
};

bvh_t* bvh_create(uint32_t num_primitives, const bbox_t* prim_boxes,
                  bvh_isect_test_fn test_fn, void* test_fn_param)
{
    bvh_t* bvh = (bvh_t*)malloc(sizeof(bvh_t));
    bvh->test_fn = test_fn;
    bvh->test_fn_param = test_fn_param;

    bvh_item_t* items = (bvh_item_t*)malloc(sizeof(bvh_item_t) * num_primitives);
    for(uint32_t i = 0; i < num_primitives; ++i)
    {
        items[i].bbox = prim_boxes[i];
        items[i].prim_index = i;
    }

    bvh->root_node = _bvh_build_tree(items, 0, num_primitives);

    free(items);

    return bvh;
}

void bvh_free(bvh_t* bvh)
{
    if(bvh)
    {
        _bvh_node_free(bvh->root_node);
        free(bvh);
    }
}

bbox_t bvh_root_bbox(const bvh_t* bvh)
{
    return bvh->root_node->bbox;
}

static bool _bvh_any_isect(const bvh_t* bvh, const ray_t* ray, const bvh_node_t* node)
{
    vec2_t t;
    if(!bbox_ray_test(&node->bbox, ray, &t))
    {
        return false;
    }

    if(get_max(t.x, t.y) < 0.0f || get_min(t.x, t.y) > 1.0f)
    {
        return false;
    }

    if(!node->left_child)
    {
        float prim_t;
        return bvh->test_fn(ray, node->prim_index, &prim_t, bvh->test_fn_param);
    }
    else
    {
        if(_bvh_any_isect(bvh, ray, node->left_child))
        {
            return true;
        }

        if(_bvh_any_isect(bvh, ray, node->right_child))
        {
            return true;
        }

        return false;
    }
}

bool bvh_any_intersection(const bvh_t* bvh, const vec3_t from, const vec3_t to)
{
    ray_t ray;
    ray.origin = from;
    ray.direction = vec3_sub(to, from);

    return _bvh_any_isect(bvh, &ray, bvh->root_node);
}

static bool _bvh_closest_isect(const bvh_t* bvh, const ray_t* ray, float* out_t, uint32_t* out_index, const bvh_node_t* node)
{
    vec2_t t;
    if(!bbox_ray_test(&node->bbox, ray, &t))
    {
        return false;
    }

    if(get_max(t.x, t.y) < 0.0f)
    {
        return false;
    }

    if(!node->left_child)
    {
        *out_index = node->prim_index;
        return bvh->test_fn(ray, node->prim_index, out_t, bvh->test_fn_param);
    }
    else
    {
        uint32_t left_index = 0;
        float left_t = -1.0f;
        uint32_t right_index = 0;
        float right_t = -1.0f;

        const bool left_hit = _bvh_closest_isect(bvh, ray, &left_t, &left_index, node->left_child);
        const bool right_hit = _bvh_closest_isect(bvh, ray, &right_t, &right_index, node->right_child);

        if(!left_hit && !right_hit)
        {
            return false;
        }

        if(left_hit && right_hit)
        {
            if(left_t < right_t)
            {
                *out_index = left_index;
                *out_t = left_t;
            }
            else
            {
                *out_index = right_index;
                *out_t = right_t;
            }
        }
        else if(left_hit)
        {
            *out_index = left_index;
            *out_t = left_t;
        }
        else
        {
            *out_index = right_index;
            *out_t = right_t;
        }

        return true;
    }
}

bool bvh_closest_intersection(const bvh_t* bvh, const ray_t* ray, uint32_t* out_index, float* out_t)
{
    return _bvh_closest_isect(bvh, ray, out_t, out_index, bvh->root_node);
}
#include "camera.h"
#include <stdlib.h>
#include <string.h>

struct camera_t
{
	float f_number;
	float iso;
	float shutter_speed;

    mat4_t camera_to_world;
};

camera_t* camera_create(void)
{
    camera_t* cam = (camera_t*)malloc(sizeof(camera_t));
    if (!cam)
    {
        return cam;
    }

	cam->f_number = 0.5f;
	cam->iso = 100.0f;
	cam->shutter_speed = 1 / 60.0f;

    mat4_identity(&cam->camera_to_world);

    return cam;
}

void camera_free(camera_t* camera)
{
    if (camera)
    {
        free(camera);
    }
}

void camera_set_aperture(camera_t* camera,
						 float f_number,
						 float iso,
						 float shutter_speed)
{
	if (!camera)
	{
		return;
	}

	camera->f_number = f_number;
	camera->iso = iso;
	camera->shutter_speed = shutter_speed;
}

void camera_look_at(camera_t* camera,
                    const vec3_t look_at,
                    const vec3_t up_vec,
                    const vec3_t cam_position)
{
    const vec3_t fwd = vec3_normalize(vec3_sub(look_at, cam_position));
    const vec3_t right = vec3_normalize(vec3_cross(up_vec, fwd));
    const vec3_t up = vec3_normalize(vec3_cross(fwd, right));

    mat4_t m;
    m.m[0][0] = right.x; m.m[0][1] = up.x; m.m[0][2] = fwd.x; m.m[0][3] = cam_position.x;
    m.m[1][0] = right.y; m.m[1][1] = up.y; m.m[1][2] = fwd.y; m.m[1][3] = cam_position.y;
    m.m[2][0] = right.z; m.m[2][1] = up.z; m.m[2][2] = fwd.z; m.m[2][3] = cam_position.z;
    m.m[3][0] = 0.0f;    m.m[3][1] = 0.0f; m.m[3][2] = 0.0f;  m.m[3][3] = 1.0f;

    memcpy(&camera->camera_to_world, &m, sizeof(mat4_t));
}

ray_t camera_pixel_ray(const camera_t* camera, float aspect_ratio, float u, float v)
{
	vec3_t corner_rays[2][2];

	const float r = 1.0f / (2.0f * camera->f_number);
	const float rx = (3.0f * r / sqrtf(13.0f));
	const float ry = (2.0f * r / sqrtf(13.0f)) / (aspect_ratio / (3.0f / 2.0f));


	corner_rays[0][0] = make_vec3(rx, ry, 1.0f);
	corner_rays[0][1] = make_vec3(-rx, ry, 1.0f);
	corner_rays[1][0] = make_vec3(rx, -ry, 1.0f);
	corner_rays[1][1] = make_vec3(-rx, -ry, 1.0f);

    /* camera space ray */
    vec3_t cs_ray_dir = vec3_lerp(
		vec3_lerp(corner_rays[0][0], corner_rays[0][1], u),
        vec3_lerp(corner_rays[1][0], corner_rays[1][1], u),
        v);

    ray_t cs_ray = make_ray_s(make_vec3(0.0f, 0.0f, 0.0f), cs_ray_dir);

    ray_t ray;
    mat4_ray_tform(&camera->camera_to_world, &cs_ray, &ray);

    return ray;
}
#ifndef CONSOLE_H_INCLUDED
#define CONSOLE_H_INCLUDED

typedef struct
{
	int row;
	int column;
} cursor_pos_t;

cursor_pos_t console_cursor_pos(void);
void console_set_cursor_pos(int row, int column);

void console_write(const char* fmt, ...);

#endif /* CONSOLE_H_INCLUDED */
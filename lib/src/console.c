#include "console.h"
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#ifdef _WIN32

#include <Windows.h>

static HANDLE _get_handle(void)
{
	return GetStdHandle(STD_OUTPUT_HANDLE);
}

cursor_pos_t console_cursor_pos(void)
{
	CONSOLE_SCREEN_BUFFER_INFO info;

	GetConsoleScreenBufferInfo(_get_handle(), &info);

	cursor_pos_t pos;
	pos.row = info.dwCursorPosition.Y;
	pos.column = info.dwCursorPosition.X;

	return pos;
}

void console_set_cursor_pos(int row, int column)
{
	COORD pos;
	pos.X = (SHORT)column;
	pos.Y = (SHORT)row;

	SetConsoleCursorPosition(_get_handle(), pos);
}

static void _console_write_impl(const char* str)
{
	const DWORD len = (DWORD)strlen(str);
	DWORD num_written = 0;

	WriteConsoleA(_get_handle(), str, len, &num_written, NULL);
}


#elif defined(__unix__)

cursor_pos_t console_cursor_pos(void)
{
    cursor_pos_t pos;
    pos.row = 0;
    pos.column = 0;
    return pos;
}

void console_set_cursor_pos(int row, int column)
{
    
}

static void _console_write_impl(const char* str)
{
    printf("%s ", str);
}

#endif

void console_write(const char* fmt, ...)
{
	va_list list;
	va_start(list, fmt);

	char buffer[256];
	vsnprintf(buffer, 256, fmt, list);
	buffer[255] = '\0';

	va_end(list);

	_console_write_impl(buffer);
}

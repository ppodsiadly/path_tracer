#include "trimesh.h"
#include "containers.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#define log_warn(...) \
    log_message(LOG_WARN, "OBJ loader", __VA_ARGS__)

typedef struct
{
    uint32_t v, vt, vn;
} obj_index_t;

typedef struct
{
    obj_index_t indices[3];
} obj_face_t;

typedef struct
{
    array_t* positions;
    array_t* tex_coords;
    array_t* normals;
    array_t* faces;
} obj_data_t;

static void _init_obj_data(obj_data_t* d)
{
    d->positions = array_create(sizeof(vec3_t));
    d->tex_coords = array_create(sizeof(vec2_t));
    d->normals = array_create(sizeof(vec3_t));
    d->faces = array_create(sizeof(obj_face_t));
}

static void _free_obj_data(obj_data_t* d)
{
    array_free(d->positions);
    d->positions = NULL;

    array_free(d->tex_coords);
    d->tex_coords = NULL;

    array_free(d->normals);
    d->normals = NULL;

    array_free(d->faces);
    d->faces = NULL;
}

static void _skip_line(FILE* file)
{
    while (!feof(file))
    {
        int ch = fgetc(file);
        if (ch == '\n')
        {
            return;
        }
    }
}

static bool _parse_vec3(FILE* file, array_t* array)
{
    vec3_t pos;
    if (fscanf(file, " %f %f %f ", &pos.x, &pos.y, &pos.z) != 3)
    {
        return false;
    }

    array_append(array, &pos);

    return true;
}

static bool _parse_tex_coord(FILE* file, obj_data_t* d)
{
    vec2_t tc;
    if (fscanf(file, " %f %f ", &tc.x, &tc.y) != 2)
    {
        return false;
    }

    array_append(d->tex_coords, &tc);

    return true;
}

static bool _parse_face(FILE* file, obj_data_t* d)
{
    obj_face_t face;

    for (int i = 0; i < 3; ++i)
    {
        int v, vt, vn;
        if (fscanf(file, " %d/%d/%d ", &v, &vt, &vn) != 3)
        {
            log_warn("OBJ file must include texture coordinates and normals");
            return false;
        }

        if (v <= 0 || vt <= 0 || vn <= 0)
        {
            return false;
        }

        face.indices[i].v = (uint32_t)(v - 1);
        face.indices[i].vt = (uint32_t)(vt - 1);
        face.indices[i].vn = (uint32_t)(vn - 1);
    }

    array_append(d->faces, &face);

    return true;
}

static bool _parse_line(FILE* file, obj_data_t* d, int* current_line)
{
    char buf[7];
    if (fscanf(file, " %6s", buf) != 1)
    {
        return false;
    }

    bool result = false;

    if (strcmp(buf, "#") == 0 || strcmp(buf, "mtllib") == 0
        || strcmp(buf, "usemtl") == 0 || strcmp(buf, "o") == 0
        || strcmp(buf, "s") == 0 || strcmp(buf, "g") == 0)
    {
        /* comment or ignored line*/
        _skip_line(file);
        result = true;
    }
    else if (strcmp(buf, "v") == 0)
    {
        result = _parse_vec3(file, d->positions);
    }
    else if (strcmp(buf, "vt") == 0)
    {
        result = _parse_tex_coord(file, d);
    }
    else if (strcmp(buf, "vn") == 0)
    {
        result = _parse_vec3(file, d->normals);
    }
    else if (strcmp(buf, "f") == 0)
    {
        result = _parse_face(file, d);
    }
    else
    {
        log_warn("unexpected line type \"%s\" at line %d", buf, *current_line);
        result = false;
    }

    ++(*current_line);
    return result;
}

static trimesh_t* _build_trimesh_from_obj_data(obj_data_t* d)
{
    /* TODO: deduplicate vertices */

    const vec3_t* positions = (const vec3_t*)array_data(d->positions);
    const vec2_t* tex_coords = (const vec2_t*)array_data(d->tex_coords);
    const vec3_t* normals = (const vec3_t*)array_data(d->normals);

    const obj_face_t* faces = (const obj_face_t*)array_data(d->faces);

    trimesh_t* tm = trimesh_create();
    if (!tm)
    {
        log_warn("Failed to allocate a trimesh");
        return NULL;
    }

    for (uint32_t fi = 0; fi < array_size(d->faces); ++fi)
    {
        tm_face_t face;

        for (int i = 0; i < 3; ++i)
        {
            tm_vertex_t v;
            v.position = positions[faces[fi].indices[i].v];
            v.normal = vec3_normalize(normals[faces[fi].indices[i].vn]);
            v.uv = tex_coords[faces[fi].indices[i].vt];
            face.indices[i] = trimesh_add_vertex(tm, &v);
        }

        trimesh_add_face(tm, &face);
    }

    trimesh_compile(tm);

    return tm;
}

trimesh_t* trimesh_load_obj(const char* file_name)
{
    log_message(LOG_INFO, "OBJ", "reading OBJ from \"%s\"", file_name);

    FILE* file = fopen(file_name, "r");
    if (!file)
    {
        log_warn("failed to open OBJ file \"%s\"", file_name);
        return NULL;
    }

    obj_data_t obj_data;
    _init_obj_data(&obj_data);

    int current_line = 1;
    while (!feof(file))
    {
        if (!_parse_line(file, &obj_data, &current_line))
        {
            goto failure;
        }
    }

    trimesh_t* tm = _build_trimesh_from_obj_data(&obj_data);
    if (tm)
    {
        fclose(file);
        return tm;
    }

failure:
    fclose(file);
    _free_obj_data(&obj_data);
    log_warn("failed to parse OBJ file \"%s\", returning NULL", file_name);

    return NULL;
}
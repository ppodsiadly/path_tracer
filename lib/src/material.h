#ifndef MATERIAL_H_INCLUDED
#define MATERIAL_H_INCLUDED

#include "color.h"
#include "vec_math.h"
#include "prng.h"

C_HEADER_BEGIN

typedef struct material_t material_t;

material_t* material_create_lambertian(const color_t reflectance);

material_t* material_create_fresnel_metal(void);

void material_add_ref(material_t* m);

void material_release(material_t* m);

color_t material_evaluate_brdf(const material_t* m, const surf_point_t* point, const vec3_t wi, const vec3_t wo);

vec3_t material_sample_ray(const material_t* m, prng_t* prng, const surf_point_t* point, const vec3_t wi);

typedef struct material_cache_t material_cache_t;

material_cache_t* material_cache_create(void);
void material_cache_free(material_cache_t* mc);

material_t* material_cache_add(material_cache_t* mc, const char* name, material_t* material);
material_t* material_cache_get(material_cache_t* mc, const char* name);

C_HEADER_END

#endif /* MATERIAL_H_INCLUDED */
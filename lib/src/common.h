#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

/** \addtogroup Utilities
 * @{
 */

#ifdef __cplusplus
# define C_HEADER_BEGIN extern "C" {
# define C_HEADER_END }
#else
# define C_HEADER_BEGIN
# define C_HEADER_END
#ifndef __GNUC__
# define inline __inline
#endif
#endif

#ifdef _MSC_VER
# define VECCALL __vectorcall
#else
# define VECCALL 
#endif

#define get_max(a, b) (((a) > (b)) ? (a) : (b))
#define get_min(a, b) (((a) < (b)) ? (a) : (b))

C_HEADER_BEGIN

typedef enum
{
    LOG_INFO,
    LOG_WARN,
    LOG_ERROR
} log_level_t;

void log_message(log_level_t level, const char* tag, const char* msg, ...);

C_HEADER_END

/** @} */

#endif /* COMMON_H_INCLUDED */
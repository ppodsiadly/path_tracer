#include "trimesh.h"
#include "bvh.h"
#include <stdlib.h>
#include <string.h>

struct tri_bvh_t
{
    uint32_t num_vertices;
    vec3_t* vertices;

    uint32_t num_faces;
    const tm_face_t* faces;

    bvh_t* bvh;
};

static bool _tri_bvh_isect_test(const ray_t* ray, uint32_t prim_index, float* out_t, void* param)
{
    const tri_bvh_t* tri_bvh = (const tri_bvh_t*)param;

    const uint32_t* face_indices = tri_bvh->faces[prim_index].indices;

    const vec3_t vertices[3] = {
        tri_bvh->vertices[face_indices[0]],
        tri_bvh->vertices[face_indices[1]],
        tri_bvh->vertices[face_indices[2]]
    };

    vec3_t tuv;
    if(ray_triangle_intersection(ray, vertices, &tuv))
    {
        *out_t = tuv.x;
        return true;
    }

    return false;
}

tri_bvh_t* tri_bvh_create(uint32_t num_vertices, const tm_vertex_t* vertices,
                          uint32_t num_faces, const tm_face_t* faces)
{
    tri_bvh_t* tri_bvh = (tri_bvh_t*)malloc(sizeof(tri_bvh_t));
    
    tri_bvh->num_vertices = num_vertices;
    tri_bvh->vertices = (vec3_t*)malloc(sizeof(vec3_t) * num_vertices);
    for(uint32_t i = 0; i < num_vertices; ++i)
    {
        tri_bvh->vertices[i] = vertices[i].position;
    }

    tri_bvh->num_faces = num_faces;
    tri_bvh->faces = faces;

    bbox_t* face_bboxes = (bbox_t*)malloc(sizeof(bbox_t) * num_faces);
    for(uint32_t fi = 0; fi < num_faces; ++fi)
    {
        const uint32_t* tri_indices = faces[fi].indices;

        const vec3_t tri_vertices[3] = {
            tri_bvh->vertices[tri_indices[0]],
            tri_bvh->vertices[tri_indices[1]],
            tri_bvh->vertices[tri_indices[2]]
        };

        face_bboxes[fi] = bbox_from_points(3, tri_vertices);
    }

    tri_bvh->bvh = bvh_create(num_faces, face_bboxes, &_tri_bvh_isect_test, tri_bvh);

    free(face_bboxes);

    return tri_bvh;
}

void tri_bvh_free(tri_bvh_t* bvh)
{
    if(bvh)
    {
        bvh_free(bvh->bvh);
        free(bvh->vertices);
        free(bvh);
    }
}

bbox_t tri_bvh_bbox(const tri_bvh_t* bvh)
{
    return bvh_root_bbox(bvh->bvh);
}

bool tri_bvh_any_intersection(const tri_bvh_t* bvh, const vec3_t from, const vec3_t to)
{
    return bvh_any_intersection(bvh->bvh, from, to);
}

bool tri_bvh_closest_intersection(const tri_bvh_t* bvh, const ray_t* ray, uint32_t* out_face_index)
{
    float t = -1.0f;
    return bvh_closest_intersection(bvh->bvh, ray, out_face_index, &t);
}
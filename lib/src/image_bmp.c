#include "image.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

typedef struct
{
    /* uint16_t bfType; */
    uint32_t bfSize;
    uint16_t bfReserved1;
    uint16_t bfReserved2;
    uint32_t bfOffBits;
} bmp_file_header_t;

/* constants for biCompression in bmp_info_header_t, taken from wingdi.h*/
#define BI_RGB        0L
#define BI_RLE8       1L
#define BI_RLE4       2L
#define BI_BITFIELDS  3L
#define BI_JPEG       4L
#define BI_PNG        5L

typedef struct
{
    uint32_t x;
    uint32_t y;
    uint32_t z;
} ciexyz_t;

typedef struct
{
    ciexyz_t ciexyzRed;
    ciexyz_t ciexyzGreen;
    ciexyz_t ciexyzBlue;
} ciexyz_triple_t;

typedef struct
{
    uint32_t        bV4Size;
    int32_t         bV4Width;
    int32_t         bV4Height;
    uint16_t        bV4Planes;
    uint16_t        bV4BitCount;
    uint32_t        bV4V4Compression;
    uint32_t        bV4SizeImage;
    int32_t         bV4XPelsPerMeter;
    int32_t         bV4YPelsPerMeter;
    uint32_t        bV4ClrUsed;
    uint32_t        bV4ClrImportant;
    uint32_t        bV4RedMask;
    uint32_t        bV4GreenMask;
    uint32_t        bV4BlueMask;
    uint32_t        bV4AlphaMask;
    uint32_t        bV4CSType;
    ciexyz_triple_t bV4Endpoints;
    uint32_t        bV4GammaRed;
    uint32_t        bV4GammaGreen;
    uint32_t        bV4GammaBlue;
} bmp_info_header_v4_t;

static uint32_t _align4(uint32_t sz)
{
    return ((sz + 3) / 4) * 4;
}

static void _apply_inv_gamma(uint8_t* values, uint32_t num_values)
{
    for (uint32_t i = 0; i < num_values; ++i)
    {
        const float linear_x = values[i] / 255.0f;
        const float gamma_x = powf(linear_x, 1.0f / 2.2f);
        values[i] = (uint8_t)(gamma_x * 255.0f);
    }
}

bool bmp_write_pixels(const char* file_name,
                      uint32_t width, uint32_t height,
                      const u8_color_t* pixels)
{
    FILE* file = fopen(file_name, "wb");
    if(!file)
    {
        return false;
    }

    const uint32_t hdrs_size = 2 + sizeof(bmp_file_header_t) + sizeof(bmp_info_header_v4_t);

    const uint16_t bfType = UINT16_C(0x4d42);
    if(fwrite(&bfType, 2, 1, file) != 1)
    {
        goto failure;
    }

    bmp_file_header_t file_hdr;
    file_hdr.bfSize = hdrs_size + _align4(width * 3) * height;
    file_hdr.bfReserved1 = 0;
    file_hdr.bfReserved2 = 0;
    file_hdr.bfOffBits = hdrs_size;
    if(fwrite(&file_hdr, sizeof(file_hdr), 1, file) != 1)
    {
        goto failure;
    }

    bmp_info_header_v4_t info_hdr;
    info_hdr.bV4Size = sizeof(bmp_info_header_v4_t);
    info_hdr.bV4Width = (int32_t)width;
    info_hdr.bV4Height = (int32_t)height;
    info_hdr.bV4Planes = 1;
    info_hdr.bV4BitCount = 24;
    info_hdr.bV4V4Compression = BI_RGB;
    info_hdr.bV4SizeImage = _align4(width * 3) * height;
    info_hdr.bV4XPelsPerMeter = 3779;
    info_hdr.bV4YPelsPerMeter = 3779;
    info_hdr.bV4ClrUsed = 0;
    info_hdr.bV4ClrImportant = 0;
    info_hdr.bV4RedMask = 0;
    info_hdr.bV4GreenMask = 0;
    info_hdr.bV4BlueMask = 0;
    info_hdr.bV4AlphaMask = 0;
    info_hdr.bV4CSType = 1934772034; /* LCS_sRGB */
    memset(&info_hdr.bV4Endpoints, 0, sizeof(ciexyz_triple_t));
    info_hdr.bV4GammaRed = 0;
    info_hdr.bV4GammaGreen = 0;
    info_hdr.bV4GammaBlue = 0;
    if(fwrite(&info_hdr, sizeof(info_hdr), 1, file) != 1)
    {
        goto failure;
    }

    const uint32_t row_stride = _align4(width * 3);
    uint8_t* row_buffer = (uint8_t*)malloc(row_stride);
    memset(row_buffer, 0, row_stride);

    for(int32_t y = height - 1; y >= 0; --y)
    {
        memcpy(row_buffer, pixels + (y * width), width * 3);
        for(uint32_t x = 0; x < width; ++x)
        {
            uint8_t* pixel = row_buffer + x * 3;
            uint8_t r = pixel[0];
            pixel[0] = pixel[2];
            pixel[2] = r;
        }

        _apply_inv_gamma(row_buffer, width * 3);

        fwrite(row_buffer, row_stride, 1, file);
    }

    free(row_buffer);

    fclose(file);
    return true;

failure:
    fclose(file);
    return false;
}

bool bmp_write_image(const char* file_name, const image_t* image)
{
    return bmp_write_pixels(file_name,
                            image_get_width(image), image_get_height(image),
                            image_get_image_data(image));
}
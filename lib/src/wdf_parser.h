#ifndef WDF_PARSER_H_INCLUDED
#define WDF_PARSER_H_INCLUDED

#include "world.h"
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

C_HEADER_BEGIN

/** \addtogroup Utilities
 * @{
 */

/** \brief Returns trimmed copy of the string.
 * 
 * The result is a copy of the input string
 * with leading and trailing white spaces removed.
 * 
 * \param str Input string.
 * \param length Length of the input string.
 * 
 * \return Pointer to the trimmed copy, allocated with `malloc`.
 */
char* trim_string(const char* str, size_t length);

char* path_parent(const char* path);

char* path_join(const char* left, const char* right);

/** @} */

/** \addtogroup wdf WDF parser
 * @{
 */

typedef struct wdf_node_t wdf_node_t;

/** \brief Parses a WDF tree from a file.
 * 
 * In case of a parsing error, the function returns
 * NULL and reports the error using \ref log_message.
 * 
 * \return Pointer to the root node, or NULL if an error ocurred. 
 */
wdf_node_t* wdf_parse(const char* file_name);

/** \brief Deallocates the node and all of its descendants.
 */
void wdf_free(wdf_node_t* root_node);

/** \brief Returns name of a WDF node.
 * 
 * \return Name string, or NULL when node is NULL.
 */
const char* wdf_node_name(const wdf_node_t* node);

/** \brief Returns value of a WDF node.
 * 
 * \return Value string, or NULL if the node does not
 *         have a value, or the node is NULL.
 */
const char* wdf_node_value(const wdf_node_t* node);

/** \brief Returns path of a file from which the node was read.
 * 
 * This function is used for more descriptive error messages.
 */
const char* wdf_node_file_path(const wdf_node_t* node);

/** \brief Returns line number in a file from which the node was read.
 * 
 * This function is used for more descriptive error messages. 
 */
uint32_t wdf_node_line_number(const wdf_node_t* node);

/** \brief Returns pointer to the n-th child of the node.
 * 
 * \return Pointer to the child node, or NULL if index is out or range.
 */
const wdf_node_t* wdf_node_child_by_index(const wdf_node_t* node, uint32_t index);

/** \brief Returns pointer to the first node with given name.
 * 
 * \return Pointer to the node, or NULL if the node does not have a child with given name.
 */
const wdf_node_t* wdf_node_child_by_name(const wdf_node_t* node, const char* name);

/** \brief Constructs a world from a WDF tree.
 */
world_t* wdf_create_world(const wdf_node_t* root);

/** \brief Constructs a camera from a `camera` node,
 *         which should be a child of the root node.
 * 
 * If the root node does not have a `camera` child node,
 * this function returns a camera initialized with default
 * values.
 */
camera_t* wdf_create_camera(const wdf_node_t* root);

/** @} */

C_HEADER_END

#endif /* WDF_PARSER_H_INCLUDED */
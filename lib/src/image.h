#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED

/** \file
 * 
 * Utilities for manipulating images and image I/O.
 */

#include "color.h"
#include <stdint.h>
#include <stdbool.h>

C_HEADER_BEGIN

/** \addtogroup image_io Image I/O
 * @{
 */

typedef struct image_t image_t;

/** \brief Allocates an image. 
 * 
 * \param width  Width, in pixels, of the image.
 * \param height Height, in pixels of the image.
 * 
 * \return Pointer to the image, or NULL on error.
 */
image_t* image_create(uint32_t width, uint32_t height);

/** \brief Frees memory allocated for the image.
 */
void image_free(image_t* img);

/** \brief Returns width, in pixels, of the image.
 * 
 * \return Width of the image, or 0 if img is NULL.
 */
uint32_t image_get_width(const image_t* img);

/** \brief Returns height, in pixels, of the image.
 * 
 * \return Width of the image, or 0 if img is NULL.
 */
uint32_t image_get_height(const image_t* img);

/** \brief Returns pointer to array of pixels.
 *
 * Pixels are stored in rows, starting with the top
 * row. Rows are placed continously in memory.
 * 
 * Each pixel consists of three 8 bit values: red, green, blue.
 * 
 * Examples:
 * 
 * \code{.c}
 * const uint8_t* pixels = image_get_image_data(img);
 * const uint32_t img_w = image_get_width(img);
 * 
 * pixels[1]; // value of green component of the top left pixel.
 * pixels[img_w * 3]; // First component (red) of the second row, from top.
 * \endcode
 */
const u8_color_t* image_get_image_data(const image_t* img);

/** \brief Sets a pixel.
 */
void image_set_pixel(image_t* img,
                     uint32_t x, uint32_t y,
                     const u8_color_t color);

bool bmp_write_pixels(const char* file_name,
                      uint32_t width, uint32_t height,
                      const u8_color_t* pixels);

/** \brief Writes an image to a BMP file.
 */
bool bmp_write_image(const char* file_name, const image_t* image);

/** @} */

C_HEADER_END

#endif /* IMAGE_H_INCLUDED */
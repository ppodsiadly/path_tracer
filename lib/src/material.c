#include "material.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#ifdef _MSC_VER
#define strdup _strdup
#endif

struct material_t
{
    uint32_t ref_count;
    color_t(*brdf)(const material_t*, const vec3_t wi, const vec3_t wo);
    vec3_t(*sample_ray)(const material_t*, prng_t* prng, const vec3_t wi);
};

typedef struct
{
    material_t base;
    color_t reflectance;
} lambertian_material_t;

static color_t _lambertian_brdf(const material_t* m, const vec3_t wi, const vec3_t wo)
{
    ((void)wi);
    ((void)wo);

    const lambertian_material_t* lm = (const lambertian_material_t*)m;
    return color_mul(lm->reflectance, INV_PI_f);
}

static vec3_t _uniform_sample_ray(const material_t* m, prng_t* prng, const vec3_t wi)
{
    ((void)m);
    ((void)wi);

    return sample_cos_hemisphere(prng_vec2(prng));
}

material_t* material_create_lambertian(const color_t reflectance)
{
    lambertian_material_t* m = (lambertian_material_t*)malloc(sizeof(lambertian_material_t));
    if (!m)
    {
        return NULL;
    }

    m->base.ref_count = 1;
    m->base.brdf = &_lambertian_brdf;
    m->base.sample_ray = &_uniform_sample_ray;
    m->reflectance = reflectance;

    return &m->base;
}

typedef struct {
    material_t base;
} fresnel_metal_material_t;

static color_t _fresnel_metal_brdf(const material_t* base, const vec3_t wi, const vec3_t wo)
{
    ((void)base);
    ((void)wi);
    ((void)wo);

    return make_color(0.6f, 0.6f, 0.6f);
}

static vec3_t _fresnel_metal_sample_ray(const material_t* base, prng_t* prng, const vec3_t wi)
{
    ((void)base);
    ((void)prng);
    ((void)wi);

    return vec3_reflect(wi, make_vec3(0.0f, 1.0f, 0.0f));
}

material_t* material_create_fresnel_metal(void)
{
    fresnel_metal_material_t* m = (fresnel_metal_material_t*)malloc(sizeof(fresnel_metal_material_t));
    if (!m)
    {
        return NULL;
    }

    m->base.ref_count = 1;
    m->base.brdf = &_fresnel_metal_brdf;
    m->base.sample_ray = &_fresnel_metal_sample_ray;

    return &m->base;
}

void material_add_ref(material_t* m)
{
    if (m)
    {
        ++m->ref_count;
    }
}

void material_release(material_t* m)
{
    if (m && (--m->ref_count) == 0)
    {
        free(m);
    }
}

color_t material_evaluate_brdf(const material_t* m, const surf_point_t* point, const vec3_t wi, const vec3_t wo)
{
    assert(m && point);

    basis_t b;
    b.tangent = point->dpdu;
    b.normal = point->normal;
    b.bitangent = point->dpdv;

    /* wi and wo vectors in tangent space */
    const vec3_t t_wi = basis_map_to(&b, wi);
    const vec3_t t_wo = basis_map_to(&b, wo);

    return m->brdf(m, t_wi, t_wo);
}

vec3_t material_sample_ray(const material_t* m, prng_t* prng, const surf_point_t* point, const vec3_t wi)
{
    assert(m && point);

    basis_t b;
    b.tangent = point->dpdu;
    b.normal = point->normal;
    b.bitangent = point->dpdv;

    const vec3_t t_wi = basis_map_to(&b, wi);
    const vec3_t t_wo = m->sample_ray(m, prng, t_wi);

    return basis_map_from(&b, t_wo);
}

typedef struct
{
    char* name;
    material_t* material;
} mc_entry_t;

static void _init_mc_entry(const char* name, material_t* m, mc_entry_t* mce)
{
    mce->name = strdup(name);
    mce->material = m;
}

static void _deinit_mc_entry(mc_entry_t* mce)
{
    free(mce->name);
    mce->name = NULL;

    material_release(mce->material);
    mce->material = NULL;
}

struct material_cache_t
{
    size_t num_materials;
    mc_entry_t* entries;
};

material_cache_t* material_cache_create(void)
{
    material_cache_t* mc = (material_cache_t*)malloc(sizeof(material_cache_t));
    if (!mc)
    {
        return NULL;
    }

    mc->num_materials = 0;
    mc->entries = NULL;

    return mc;
}

void material_cache_free(material_cache_t* mc)
{
    if (mc)
    {
        for (size_t i = 0; i < mc->num_materials; ++i)
        {
            _deinit_mc_entry(&mc->entries[i]);
        }

        free(mc->entries);
        free(mc);
    }
}

material_t* material_cache_add(material_cache_t* mc, const char* name, material_t* material)
{
    if (!mc || !name || !material)
    {
        return NULL;
    }

    material_t* existing_mat = material_cache_get(mc, name);
    if (existing_mat)
    {
        if (existing_mat != material)
        {
            material_release(material);
        }

        return existing_mat;
    }

    mc->entries = realloc(mc->entries, (mc->num_materials + 1) * sizeof(mc_entry_t));
    _init_mc_entry(name, material, &mc->entries[mc->num_materials]);
    mc->num_materials += 1;

    material_add_ref(material);

    return material;
}

material_t* material_cache_get(material_cache_t* mc, const char* name)
{
    if (!mc || !name)
    {
        return NULL;
    }

    for (size_t i = 0; i < mc->num_materials; ++i)
    {
        if (strcmp(name, mc->entries[i].name) == 0)
        {
            return mc->entries[i].material;
        }
    }

    return NULL;
}
#ifndef TRIMESH_H_INCLUDED
#define TRIMESH_H_INCLUDED

#include "vec_math.h"
#include <stdint.h>

C_HEADER_BEGIN

/** \addtogroup trimesh Triangle mesh
 * @{
 */

typedef struct trimesh_t trimesh_t;

trimesh_t* trimesh_create(void);
void trimesh_add_ref(trimesh_t* tm);
void trimesh_release(trimesh_t* tm);

typedef struct
{
    vec3_t position;
    vec3_t normal;
    vec2_t uv;
} tm_vertex_t;

typedef struct
{
    uint32_t indices[3];
} tm_face_t;

uint32_t trimesh_add_vertex(trimesh_t* tm, const tm_vertex_t* v);
void trimesh_add_face(trimesh_t* tm, const tm_face_t* f);

void trimesh_compile(trimesh_t* tm);

bbox_t trimesh_bbox(const trimesh_t* tm);

bool ray_trimesh_intersect(const ray_t* ray, const trimesh_t* tm,
                           surf_point_t* point, float* out_t);

bool segment_trimesh_test(const vec3_t from, const vec3_t to,
                          const trimesh_t* tm);

trimesh_t* trimesh_load_obj(const char* file_name);

typedef struct tri_bvh_t tri_bvh_t;

tri_bvh_t* tri_bvh_create(uint32_t num_vertices, const tm_vertex_t* vertices,
                          uint32_t num_faces, const tm_face_t* faces);

void tri_bvh_free(tri_bvh_t* bvh);

bbox_t tri_bvh_bbox(const tri_bvh_t* bvh);

bool tri_bvh_any_intersection(const tri_bvh_t* bvh, const vec3_t from, const vec3_t to);

bool tri_bvh_closest_intersection(const tri_bvh_t* bvh, const ray_t* ray, uint32_t* out_face_index);

/** @} */

C_HEADER_END

#endif /* TRIMESH_H_INCLUDED */
#ifndef WORLD_H_INCLUDED
#define WORLD_H_INCLUDED

#include "render_buf.h"
#include "camera.h"
#include "trimesh.h"
#include "material.h"
#include "vec_math.h"

C_HEADER_BEGIN

/** \addtogroup world World
 * @{
 */

typedef struct world_t world_t;

world_t* world_create(void);
void world_free(world_t* w);

material_cache_t* world_get_material_cache(world_t* world);

void world_add_sphere(world_t* world,
                      float radius,
                      material_t* material,
                      color_t surf_luminance,
                      const mat4_t* tform);

void world_add_rectangle(world_t* world,
                         material_t* material,
                         color_t surf_luminance,
                         const mat4_t* tform);

void world_add_point_light(world_t* world,
                           const vec3_t position,
                           const color_t color);

void world_add_trimesh(world_t* world, trimesh_t* tm, material_t* mtrl, color_t surf_luminance, const mat4_t* tform);

bool world_cast_ray(const world_t* world, const ray_t* ray, surf_point_t* point, mat4_t* obj_to_world, const material_t** material, color_t* surface_luminance);

bool world_test_visibility(const world_t* world, const vec3_t from, const vec3_t to);

color_t world_compute_direct_lighting(const world_t* world, const surf_point_t* point, const material_t* material, const vec3_t wo);

typedef struct
{
    void(*callback)(uint32_t done, uint32_t total, void* user_ptr);
    void* user_ptr;
} progress_callback_t;

void world_pathtrace(world_t* world, const camera_t* camera, prng_t* prng, uint32_t rays_per_pixel, render_buf_t* dest, const progress_callback_t* progress_callback);

/** @} */

C_HEADER_END

#endif /* WORLD_H_INCLUDED */
#include "world.h"
#include <stdlib.h>

#define MAX_PATH_LEN 10

static color_t _trace_path(const world_t* world, prng_t* prng, const ray_t* pixel_ray, uint32_t path_len)
{
    color_t result = make_color(0.0f, 0.0f, 0.0f);
    color_t path_w = make_color(1.0f, 1.0f, 1.0f);
    ray_t ray = *pixel_ray;

    while (path_len < MAX_PATH_LEN)
    {
        surf_point_t point;
        mat4_t obj_to_world;
        const material_t* mtrl = NULL;
        color_t surf_luminance;

        if (world_cast_ray(world, &ray, &point, &obj_to_world, &mtrl, &surf_luminance))
        {
            point.position = mat4_point_tform(&obj_to_world, point.position);
            point.normal = vec3_normalize(mat4_vector_tform(&obj_to_world, point.normal));

            const color_t li = world_compute_direct_lighting(world, &point, mtrl, vec3_mul(ray.direction, -1.0f));
            result = color_add(result, color_mul_rgb(color_add(li, surf_luminance), path_w));

            const vec3_t refl_dir = material_sample_ray(mtrl, prng, &point, ray.direction);
            const color_t f = material_evaluate_brdf(mtrl, &point, ray.direction, refl_dir);

            ray.origin = vec3_add(point.position, vec3_mul(refl_dir, 1e-4f));
            ray.direction = refl_dir;

            path_w = color_mul_rgb(path_w, f);

            path_len += 1;
        }
        else
        {
            break;
        }
    }
    
    return result;
}

static color_t _pathtrace_pixel(const world_t* world, const camera_t* camera, prng_t* prng, float aspect_ratio, float u, float v, uint32_t rays_per_pixel)
{
    const ray_t pixel_ray = camera_pixel_ray(camera, aspect_ratio, u, v);

    color_t result = make_color(0.0f, 0.0f, 0.0f);
    for (uint32_t i = 0; i < rays_per_pixel; ++i)
    {
        result = color_add(result, _trace_path(world, prng, &pixel_ray, 1));
    }

    return color_mul(result, 1.0f / rays_per_pixel);
}

static void _pathtrace_tile(const world_t* w, const camera_t* cam, render_buf_t* dest, uint32_t tile_index, prng_t prng, uint32_t rays_per_pixel)
{
    const uint32_t rb_width = render_buf_get_width(dest);
    const uint32_t rb_height = render_buf_get_height(dest);

    const float aspect_ratio = (float)rb_width / rb_height;

    rb_tile_bounds_t bounds;
    render_buf_get_tile_bounds(dest, tile_index, &bounds);

    for (uint32_t y = bounds.min_y; y <= bounds.max_y; ++y)
    {
        const float pixel_v = (float)y / rb_height;

        for (uint32_t x = bounds.min_x; x <= bounds.max_x; ++x)
        {
            const float pixel_u = (float)x / rb_width;

            const color_t color = _pathtrace_pixel(w, cam, &prng, aspect_ratio, pixel_u, pixel_v, rays_per_pixel);

            render_buf_set_pixel(dest, x, y, color);
        }
    }
}

void world_pathtrace(world_t* world, const camera_t* camera, prng_t* prng, uint32_t rays_per_pixel, render_buf_t* dest, const progress_callback_t* progress_callback)
{
    const int num_tiles = (int)render_buf_get_num_tiles(dest);

    prng_t* tile_prngs = (prng_t*)malloc(sizeof(prng_t) * num_tiles);
    for(int i = 0; i < num_tiles; ++i)
    {
        tile_prngs[i] = prng_init(prng_uint64(prng));
    }

    if(progress_callback && !progress_callback->callback)
    {
        progress_callback = NULL;
    }

    uint32_t num_done_tiles = 0;

    int tile_index;
    #pragma omp parallel for
    for (tile_index = 0; tile_index < num_tiles; ++tile_index)
    {
        const prng_t tile_prng = tile_prngs[tile_index];
        
        _pathtrace_tile(world, camera, dest, (uint32_t)tile_index, tile_prng, rays_per_pixel);

        if(progress_callback)
        {
            #pragma omp critical (progress_callback)
            {
                ++num_done_tiles;
                progress_callback->callback(num_done_tiles, (uint32_t)num_tiles, progress_callback->user_ptr);
            }
        }
    }

    free(tile_prngs);
}
#include "wdf_parser.h"
#include "world.h"
#include "trimesh.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef _MSC_VER
# define strdup _strdup
#endif

static bool _wdf_parse_vec3(const wdf_node_t* node, vec3_t* result)
{
    const char* value_str = wdf_node_value(node);

    if (!value_str)
    {
        return false;
    }

    float x, y, z;
    if (sscanf(value_str, "%f %f %f", &x, &y, &z) != 3)
    {
        return false;
    }

    *result = make_vec3(x, y, z);
    return true;
}

static bool _wdf_parse_vec4(const wdf_node_t* node, float* result)
{
    const char* value_str = wdf_node_value(node);

    if (!value_str)
    {
        return false;
    }

    float v[4];
    if (sscanf(value_str, "%f %f %f %f", &v[0], &v[1], &v[2], &v[3]) != 4)
    {
        return false;
    }

    memcpy(result, v, 4 * sizeof(float));
    return true;
}

static bool _wdf_parse_float(const wdf_node_t* node, float* result)
{
    const char* value_str = wdf_node_value(node);

    if (!value_str)
    {
        return false;
    }

    float value;
    if (sscanf(value_str, "%f", &value) != 1)
    {
        return false;
    }

    *result = value;
    return true;
}

static void _wdf_tform_translate(mat4_t* tform, const wdf_node_t* node)
{
    vec3_t tv = make_vec3(0.0f, 0.0f, 0.0f);
    _wdf_parse_vec3(node, &tv);

    mat4_t m;
	mat4_from_translation(&m, tv);

    mat4_multiply(tform, &m, tform);
}

static void _wdf_tform_rotation(mat4_t* tform, const wdf_node_t* node)
{
    float rv[4] = { 1.0f, 0.0f, 0.0f, 0.0f };
    _wdf_parse_vec4(node, rv);

    const vec3_t axis = vec3_normalize(make_vec3(rv[0], rv[1], rv[2]));
    const float angle = deg_to_rad(rv[3]);

    mat4_t m;
    mat4_from_axis_angle(&m, axis, angle);

    mat4_multiply(tform, &m, tform);
}

static void _wdf_tform_scale(mat4_t* tform, const wdf_node_t* node)
{
    vec3_t sv = make_vec3(0.0f, 0.0f, 0.0f);
    _wdf_parse_vec3(node, &sv);

    mat4_t m;
    mat4_from_scale(&m, sv);

    mat4_multiply(tform, &m, tform);
}

static void _wdf_tform_matrix(mat4_t* tform, const wdf_node_t* node)
{
    if(!wdf_node_value(node))
    {
        return;
    }
    
    float values[16];

    const char* delims = " \t";
    char* matrix_str = strdup(wdf_node_value(node));

    char* ptr = matrix_str;
    for(int i = 0; i < 16; ++i)
    {
        ptr = strtok(ptr, delims);
        if(!ptr || sscanf(ptr, "%f", &values[i]) != 1)
        {
            log_message(LOG_WARN, "[WDF]", "%s:%u: \"matrix\" has incorrect value",
                        wdf_node_file_path(node), wdf_node_line_number(node));
            return;
        }
    }

    free(matrix_str);
        
    mat4_t m;
    for(int row = 0; row < 4; ++row)
    {
        for(int col = 0; col < 4; ++col)
        {
            m.m[row][col] = values[row * 4 + col];
        }
    }

    mat4_multiply(tform, &m, tform);
}

static void _wdf_parse_tform(const wdf_node_t* node, mat4_t* tform)
{
    mat4_identity(tform);

    if(!node)
    {
        return;
    }

    uint32_t child_index = 0;
    const wdf_node_t* child;
    while((child = wdf_node_child_by_index(node, child_index++)))
    {
        const char* name = wdf_node_name(child);

        if(strcmp(name, "translation") == 0)
        {
            _wdf_tform_translate(tform, child);
        }
        else if(strcmp(name, "rotation") == 0)
        {
            _wdf_tform_rotation(tform, child);
        }
        else if(strcmp(name, "scale") == 0)
        {
            _wdf_tform_scale(tform, child);
        }
        else if(strcmp(name, "matrix") == 0)
        {
            _wdf_tform_matrix(tform, child);
        }
    }
}

static color_t vec3_to_color(const vec3_t v)
{
    return make_color(v.x, v.y, v.z);
}

typedef void(*wdf_node_visitor)(const wdf_node_t*, world_t*);

static void _wdf_visit_nodes_by_name(const wdf_node_t* root, const char* name, wdf_node_visitor visitor, world_t* w)
{
    uint32_t idx = 0;
    const wdf_node_t* n = wdf_node_child_by_index(root, idx++);
    while (n)
    {
        if (strcmp(wdf_node_name(n), name) == 0)
        {
            visitor(n, w);
        }

        n = wdf_node_child_by_index(root, idx++);
    }
}

static material_t* _wdf_create_lambertian_material(const wdf_node_t* n)
{
    vec3_t R = make_vec3(1.0f, 0.0f, 1.0f);
    _wdf_parse_vec3(wdf_node_child_by_name(n, "R"), &R);
    return material_create_lambertian(vec3_to_color(R));
}

static material_t* _wdf_create_fresnel_material(const wdf_node_t* n)
{
    ((void)n);
    return material_create_fresnel_metal();
}

static void _wdf_create_material(const wdf_node_t* node, world_t* w)
{
    const char* mtrl_name = wdf_node_value(node);

    const char* brdf = wdf_node_value(wdf_node_child_by_name(node, "brdf"));
    if (!brdf)
    {
        log_message(LOG_WARN, "WDF", "material does not specify brdf, ignoring.");
        return;
    }

    material_t* mtrl = NULL;

    if (strcmp(brdf, "lambert") == 0)
    {
        mtrl = _wdf_create_lambertian_material(node);
    }
    else if(strcmp(brdf, "fresnel") == 0)
    {
        mtrl = _wdf_create_fresnel_material(node);
    }
    else
    {
        log_message(LOG_WARN, "WDF", "invalid value brdf name (\"%s\"), ignoring.", brdf);
    }

    material_cache_t* mc = world_get_material_cache(w);
    material_cache_add(mc, mtrl_name, mtrl);
}

static void _wdf_create_point_light(const wdf_node_t* n, world_t* w)
{
    vec3_t position = make_vec3(0.0f, 0.0f, 0.0f);
    vec3_t intensity = make_vec3(100.0f, 0.0f, 100.0f);

    if(!_wdf_parse_vec3(wdf_node_child_by_name(n, "position"), &position))
    {
        mat4_t tform;
        _wdf_parse_tform(wdf_node_child_by_name(n, "transform"), &tform);
        position = mat4_point_tform(&tform, make_vec3(0.0f, 0.0f, 0.0f));
    }

    _wdf_parse_vec3(wdf_node_child_by_name(n, "intensity"), &intensity);

    world_add_point_light(w, position, vec3_to_color(intensity));
}

static material_t* _material_by_name(world_t* w, const char* name)
{
    material_cache_t* mc = world_get_material_cache(w);

    material_t* mtrl = material_cache_get(mc, name);
    if (!mtrl)
    {
        log_message(LOG_WARN, "WDF", "material with name \"%s\" does not exist!", name);
    }

    return mtrl;
}

static void _wdf_create_rect(const wdf_node_t* n, world_t* w)
{
    material_t* mtrl = _material_by_name(w, wdf_node_value(wdf_node_child_by_name(n, "material")));

    mat4_t tform;
    _wdf_parse_tform(wdf_node_child_by_name(n, "transform"), &tform);

    vec3_t luminance = make_vec3(0.0f, 0.0f, 0.0f);
    _wdf_parse_vec3(wdf_node_child_by_name(n, "luminance"), &luminance);

    world_add_rectangle(w, mtrl, vec3_to_color(luminance), &tform);
}

static void _wdf_create_sphere(const wdf_node_t* n, world_t* w)
{
    float radius = 1.0f;
    _wdf_parse_float(wdf_node_child_by_name(n, "radius"), &radius);

    material_t* mtrl = _material_by_name(w, wdf_node_value(wdf_node_child_by_name(n, "material")));

    mat4_t tform;
    _wdf_parse_tform(wdf_node_child_by_name(n, "transform"), &tform);

    vec3_t luminance = make_vec3(0.0f, 0.0f, 0.0f);
    _wdf_parse_vec3(wdf_node_child_by_name(n, "luminance"), &luminance);

    world_add_sphere(w, radius, mtrl, vec3_to_color(luminance), &tform);
}

static void _wdf_create_trimesh(const wdf_node_t* n, world_t* w)
{
    const char* obj_file = wdf_node_value(wdf_node_child_by_name(n, "obj_file"));
    if(!obj_file)
    {
        log_message(LOG_WARN, "WDF", "trimesh does not specify OBJ file name, ignoring!");
        return;
    }

    material_t* mtrl = _material_by_name(w, wdf_node_value(wdf_node_child_by_name(n, "material")));
    if(!mtrl)
    {
        return;
    }

    char* wdf_dir = path_parent(wdf_node_file_path(n));
    char* full_obj_path = path_join(wdf_dir, obj_file);

    trimesh_t* tm = trimesh_load_obj(full_obj_path);
    if(!tm)
    {
        log_message(LOG_WARN, "WDF", "Could not load OBJ file from \"%s\"", full_obj_path);
    }

    mat4_t tform;
    _wdf_parse_tform(wdf_node_child_by_name(n, "transform"), &tform);

    vec3_t luminance = make_vec3(0.0f, 0.0f, 0.0f);
    _wdf_parse_vec3(wdf_node_child_by_name(n, "luminance"), &luminance);

    world_add_trimesh(w, tm, mtrl, vec3_to_color(luminance), &tform);

    trimesh_release(tm);

    free(wdf_dir);
    free(full_obj_path);
}

world_t* wdf_create_world(const wdf_node_t* root)
{
    if (!root || strcmp(wdf_node_name(root), "world") != 0)
    {
        return NULL;
    }

    world_t* world = world_create();
    if (!world)
    {
        return NULL;
    }

    _wdf_visit_nodes_by_name(root, "material", &_wdf_create_material, world);

    _wdf_visit_nodes_by_name(root, "point_light", &_wdf_create_point_light, world);

    _wdf_visit_nodes_by_name(root, "rectangle", &_wdf_create_rect, world);
    _wdf_visit_nodes_by_name(root, "sphere", &_wdf_create_sphere, world);
    _wdf_visit_nodes_by_name(root, "trimesh", &_wdf_create_trimesh, world);

    return world;
}

camera_t* wdf_create_camera(const wdf_node_t* root)
{
    if (!root || strcmp(wdf_node_name(root), "world") != 0)
    {
        return NULL;
    }

    const wdf_node_t* camera_node = wdf_node_child_by_name(root, "camera");

    camera_t* camera = camera_create();

    vec3_t position;
    vec3_t look_at;

    if (!_wdf_parse_vec3(wdf_node_child_by_name(camera_node, "position"), &position))
    {
        position = make_vec3(0, 0, 1);
    }

    if (!_wdf_parse_vec3(wdf_node_child_by_name(camera_node, "look_at"), &look_at))
    {
        look_at = make_vec3(0, 0, 0);
    }

    camera_look_at(camera, look_at, make_vec3(0.0f, 1.0f, 0.0f), position);

	float f_number = 1.4f;
	float iso = 100.0f;
	float shutter_speed = 1 / 60.0f;

	_wdf_parse_float(wdf_node_child_by_name(camera_node, "f-number"), &f_number);

	camera_set_aperture(camera, f_number, iso, shutter_speed);

    return camera;
}
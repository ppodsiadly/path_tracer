#include <doctest/doctest.h>
#include <wdf_parser.h>
#include <string>
#include <memory>

using namespace std::string_literals;

namespace {

struct Deleter
{
    void operator ()(char* c_str) const
    {
        free(c_str);
    }
};

using c_str = std::unique_ptr<char, Deleter>;

}

TEST_CASE("path_parent")
{
    c_str result;

    result.reset(path_parent(""));
    REQUIRE_EQ(result.get(), "../"s);

    result.reset(path_parent(nullptr));
    REQUIRE_EQ(result.get(), "../"s);

    result.reset(path_parent("./"));
    REQUIRE_EQ(result.get(), "../"s);

    result.reset(path_parent("."));
    REQUIRE_EQ(result.get(), "../"s);

    result.reset(path_parent("some/path"));
    REQUIRE_EQ(result.get(), "some/"s);

    result.reset(path_parent("some\\path\\"));
    REQUIRE_EQ(result.get(), "some/"s);
}

TEST_CASE("path_join")
{
    c_str result;

    result.reset(path_join(nullptr, nullptr));
    REQUIRE_EQ(result.get(), "."s);

    result.reset(path_join(nullptr, "path"));
    REQUIRE_EQ(result.get(), "path"s);

    result.reset(path_join("some", nullptr));
    REQUIRE_EQ(result.get(), "some"s);

    result.reset(path_join("some", "path"));
    REQUIRE_EQ(result.get(), "some/path"s);

    result.reset(path_join("some\\other", "path"));
    REQUIRE_EQ(result.get(), "some\\other/path"s);
}
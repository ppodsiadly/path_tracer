#include <doctest/doctest.h>
#include <vec_math.h>

using namespace doctest;

TEST_CASE("Point on ray projection")
{
    const auto ray = make_ray_s({ 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f });
    vec3_t pp;

    SUBCASE("No projection")
    {
        const auto p = vec3_t{ -1.0f, 0.0f, 0.0f };

        REQUIRE_FALSE(ray_project(&ray, &p, &pp));
    }

    SUBCASE("Point on ray")
    {
        const auto p = vec3_t{ 1.0f, 0.0f, 0.0f };

        REQUIRE(ray_project(&ray, &p, &pp));

        REQUIRE_EQ(pp.x, Approx(1.0f));
        REQUIRE_EQ(pp.y, Approx(0.0f));
        REQUIRE_EQ(pp.z, Approx(0.0f));
    }

    SUBCASE("Point not on ray")
    {
        const auto p = vec3_t{ 1.0f, 5.0f, -3.0f };

        REQUIRE(ray_project(&ray, &p, &pp));

        REQUIRE_EQ(pp.x, Approx(1.0f));
        REQUIRE_EQ(pp.y, Approx(0.0f));
        REQUIRE_EQ(pp.z, Approx(0.0f));
    }
}

TEST_CASE("Ray-sphere intersection")
{
    const float radius = 2.0f;

    surf_point_t point;
    float t;

    SUBCASE("No intersection")
    {
        const auto ray = make_ray_s({ 0.0f, 0.0f, -4.0f }, { 0.0f, 4.0f, 4.0f });

        REQUIRE_FALSE(ray_sphere_intersection(&ray, radius, &point, &t));
    }

    SUBCASE("Intersection")
    {
        const auto ray = make_ray_s({ 0.0f, 0.0f, -4.0f }, { 0.0f, 0.0f, 1.0f });

        REQUIRE(ray_sphere_intersection(&ray, radius, &point, &t));

        REQUIRE_EQ(point.position.x, Approx(0.0f));
        REQUIRE_EQ(point.position.y, Approx(0.0f));
        REQUIRE_EQ(point.position.z, Approx(-2.0f));

        REQUIRE_EQ(point.normal.x, Approx(0.0f));
        REQUIRE_EQ(point.normal.y, Approx(0.0f));
        REQUIRE_EQ(point.normal.z, Approx(-1.0f));
    }
}

TEST_CASE("Ray-plane intersection")
{
    surf_point_t point;
    float t;

    SUBCASE("Perpendicular ray")
    {
        ray_t ray = make_ray_s({ 1.0f, 3.0f, -2.0f }, { 0.0f, 0.0f, 1.0f });

        REQUIRE(ray_xy_plane_intersection(&ray, make_vec2(4.0f, 4.0f), &point, &t));

        REQUIRE_EQ(point.position.x, Approx(1.0f));
        REQUIRE_EQ(point.position.y, Approx(3.0f));
        REQUIRE_EQ(point.position.z, Approx(0.0f));

        REQUIRE_EQ(point.normal.x, Approx(0.0f));
        REQUIRE_EQ(point.normal.y, Approx(0.0f));
        REQUIRE_EQ(point.normal.z, Approx(-1.0f));

        REQUIRE_EQ(t, Approx(2.0f));
    }

    SUBCASE("No hit, ray in the same direction as the plane")
    {
        ray_t ray = make_ray_s({ 0.0f, 0.0f, 1.0f }, { 0.0f, 0.0f, 1.0f });

        REQUIRE_FALSE(ray_xy_plane_intersection(&ray, make_vec2(4.0f, 4.0f), &point, &t));
    }

    SUBCASE("No hit, ray parallel to the plane")
    {
        ray_t ray = make_ray_s({ 1.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f });

        REQUIRE_FALSE(ray_xy_plane_intersection(&ray, make_vec2(4.0f, 4.0f), &point, &t));
    }
}

TEST_CASE("Segment-sphere test")
{
    const auto center = vec3_t{ 2.0f, 3.0f, 1.0f };
    const float radius = 1.0f;

    SUBCASE("Two intersections")
    {
        const vec3_t p0 = vec3_t{ 0.5f, -3.0f, 0.1f };
        const vec3_t p1 = vec3_t{ 0.5f, 1.5f, 0.0f };

        REQUIRE(segment_sphere_test(p0, p1, radius));
        REQUIRE(segment_sphere_test(p1, p0, radius));
    }

    SUBCASE("One intersection, p1 inside the sphere")
    {
        const vec3_t p0 = vec3_t{ 0.5f, -3.0f, 0.1f };
        const vec3_t p1 = vec3_t{ 0.5f, -0.1f, 0.0f };

        REQUIRE(segment_sphere_test(p0, p1, radius));
        REQUIRE(segment_sphere_test(p1, p0, radius));
    }

    SUBCASE("No intersection, sphere intersects the line")
    {
        const vec3_t p0 = vec3_t{ 0.5f, -3.0f, 0.1f };
        const vec3_t p1 = vec3_t{ 0.5f, -2.0f, 0.0f };

        REQUIRE_FALSE(segment_sphere_test(p0, p1, radius));
        REQUIRE_FALSE(segment_sphere_test(p1, p0, radius));
    }

    SUBCASE("No intersection")
    {
        const vec3_t p0 = vec3_t{ 3.0f, -3.0f, 0.1f };
        const vec3_t p1 = vec3_t{ 3.0f, 3.0f, 0.0f };

        REQUIRE_FALSE(segment_sphere_test(p0, p1, radius));
        REQUIRE_FALSE(segment_sphere_test(p1, p0, radius));
    }
}
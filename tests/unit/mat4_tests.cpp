#include <doctest/doctest.h>
#include <vec_math.h>

using namespace doctest;

TEST_CASE("mat4_t")
{
    SUBCASE("mat4_det")
    {
        mat4_t m;

        mat4_identity(&m);
        REQUIRE_EQ(mat4_det(&m), Approx(1.0f));

        m.m[1][1] = 0.0f;
        REQUIRE_EQ(mat4_det(&m), Approx(0.0f));

        m.m[1][1] = 2.0f;
        REQUIRE_EQ(mat4_det(&m), Approx(2.0f));
    }

    SUBCASE("mat4_det")
    {
        const mat4_t m{{
            { 3.0f, -1.0f, 2.0f, 4.0f },
            { 9.0f, -2.0f, 3.0f, 1.0f },
            { 0.0f, 1.0f, 2.0f, 3.0f },
            { -3.0f, -2.0f, -1.0f, 0.0f }
        }};

        const float d = mat4_det(&m);
        REQUIRE_EQ(d, Approx(-99.0f));
    }

    SUBCASE("mat4_invert")
    {
        const mat4_t m{{
            { 3.0f, -1.0f, 2.0f, 4.0f },
            { 9.0f, -2.0f, 3.0f, 1.0f },
            { 0.0f, 1.0f, 2.0f, 3.0f },
            { -3.0f, -2.0f, -1.0f, 0.0f }
        }};

        const mat4_t expected_inv_m{{
            { 0.2121212f, -0.030303f, -0.2727273f, -0.2121212f },
            { 0.0606061f, -0.1515152f, -0.030303f, -0.3939394f },
            { -0.7575758f, 0.3939394f, 0.8787879f, 0.4242424f },
            { 0.4848485f, -0.2121212f, -0.2424242f, -0.1515152f }
        }};

        mat4_t inv_m;
        mat4_invert(&m, &inv_m);

        for(int row = 0; row < 4; ++row)
        {
            for(int col = 0; col < 4; ++col)
            {
                REQUIRE_EQ(inv_m.m[row][col], Approx(expected_inv_m.m[row][col]));
            }
        }
    }

	SUBCASE("mat4_invert")
	{
		const mat4_t m{ {
			{ 0.0f, 0.0f, -1.0f, -15.0f },
			{ 0.0f, 1.0f, 0.0f, 0.0f },
			{ 1.0f, 0.0f, 0.0f, 0.0f },
			{ 0.0f, 0.0f, 0.0f, 1.0f }
		} };

		const mat4_t expected_inv_m{ {
			{ 0.0f, 0.0f, 1.0f, 0.0f },
			{ 0.0f, 1.0f, 0.0f, 0.0f },
			{ -1.0f, 0.0f, 0.0f, -15.0f },
			{ 0.0f, 0.0f, 0.0f, 1.0f }
		} };

		mat4_t inv_m;
		mat4_invert(&m, &inv_m);

		for (int row = 0; row < 4; ++row)
		{
			for (int col = 0; col < 4; ++col)
			{
				REQUIRE_EQ(inv_m.m[row][col], Approx(expected_inv_m.m[row][col]));
			}
		}
	}
}
#include <doctest/doctest.h>
#include <prng.h>
#include <vector>

using namespace doctest;

TEST_CASE("Uniform floats")
{
    const std::vector<uint64_t> seeds{
        0, 1537, (UINT64_C(1) << 53u) + 1
    };

    for (uint64_t seed : seeds)
    {
        float sum = 0.0f;
        const uint32_t num_iterations = 4096;

        prng_t prng = prng_init(seed);

        for (uint32_t i = 0; i < num_iterations; ++i)
        {
            const float rf = prng_float(&prng);

            sum += rf;
        }

        REQUIRE_EQ(sum / num_iterations, Approx(0.5).epsilon(0.01));
    }
}
#include <doctest/doctest.h>
#include <vec_math.h>

using namespace doctest;

TEST_CASE("vec2_blend3")
{
	const auto a = vec2_t{ 1.0f, 2.0f };
	const auto b = vec2_t{ 3.0f, 4.0f };
	const auto c = vec2_t{ -5.0f, -6.0f };

	const auto r = vec2_blend3(a, b, c, { 2.0f, 0.5f, 1.0f });

	REQUIRE_EQ(r.x, Approx(-1.5));
	REQUIRE_EQ(r.y, Approx(0.0));
}
#include <doctest/doctest.h>
#include <vec_math.h>

using namespace doctest;

TEST_CASE("Basis")
{
    basis_t basis;
    basis_init({ 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f }, &basis);

    SUBCASE("Map to")
    {
        const auto v = basis_map_to(&basis, { 0.0f, 0.0f, 1.0f });

        REQUIRE_EQ(v.x, Approx(0.0f));
        REQUIRE_EQ(v.y, Approx(1.0f));
        REQUIRE_EQ(v.z, Approx(0.0f));
    }

    SUBCASE("Map from")
    {
        const auto v = basis_map_from(&basis, { 0.0f, 1.0f, 0.0f });

        REQUIRE_EQ(v.x, Approx(0.0f));
        REQUIRE_EQ(v.y, Approx(0.0f));
        REQUIRE_EQ(v.z, Approx(1.0f));
    }
}
#include <doctest/doctest.h>
#include <containers.h>
#include <memory>

namespace {

struct Deleter
{
    void operator ()(array_t* arr) const
    {
        array_free(arr);
    }
};

using guarded_array_t = std::unique_ptr<array_t, Deleter>;

}

TEST_CASE("Array append")
{
    guarded_array_t arr;
    arr.reset(array_create(sizeof(int)));

    REQUIRE_EQ(array_size(arr.get()), 0);
    REQUIRE_EQ(array_data(arr.get()), nullptr);

    for (int i = 1; i <= 33; ++i)
    {
        array_append(arr.get(), &i);
    }

    REQUIRE_EQ(array_size(arr.get()), 33);

    const int* data = reinterpret_cast<const int*>(array_data(arr.get()));

    for (int i = 0; i < 33; ++i)
    {
        REQUIRE_EQ(data[i], i + 1);
    }
}

TEST_CASE("Array set & get")
{
    struct elem_t
    {
        int a;
        double b;
    };

    guarded_array_t arr;
    arr.reset(array_create(sizeof(elem_t)));

    auto get = [&](uint32_t idx) {
        auto e = reinterpret_cast<elem_t*>(array_get(arr.get(), idx));
        return *e;
    };

    auto set = [&](uint32_t idx, const elem_t& e) {
        array_set(arr.get(), idx, &e);
    };

    array_resize(arr.get(), 7);
    set(0, { 4, 16.0 });
    set(6, { 1, 20.0 });
    set(3, { 3, 3.0 });

    REQUIRE_EQ(get(0).a, 4);
    REQUIRE_EQ(get(0).b, 16.0);
    REQUIRE_EQ(get(3).a, 3);
    REQUIRE_EQ(get(3).b, 3.0);

    array_resize(arr.get(), 30);

    REQUIRE_EQ(get(0).a, 4);
    REQUIRE_EQ(get(0).b, 16.0);
    REQUIRE_EQ(get(3).a, 3);
    REQUIRE_EQ(get(3).b, 3.0);
}
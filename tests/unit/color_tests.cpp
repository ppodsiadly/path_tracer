#include <doctest/doctest.h>
#include <color.h>

using namespace doctest;

TEST_CASE("Color conversions")
{
    const auto in_col = color_t{ 0.5f, -0.5f, 1.5f };

    const auto col_u8 = color_to_u8(in_col);
    REQUIRE_EQ(col_u8.r, 127);
    REQUIRE_EQ(col_u8.g, 0);
    REQUIRE_EQ(col_u8.b, 255);

    const auto col_f = color_from_u8(col_u8);
    REQUIRE_EQ(col_f.r, 127.0f / 255.0f);
    REQUIRE_EQ(col_f.g, 0.0f);
    REQUIRE_EQ(col_f.b, 1.0f);
}
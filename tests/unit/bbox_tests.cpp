#include <doctest/doctest.h>
#include <vec_math.h>

using namespace doctest;

TEST_CASE("Ray-bbox intersection")
{
    SUBCASE("bbox 1")
    {
        const bbox_t bbox{
            vec3_t{ 3.0f, 3.0f, 3.0f },
            vec3_t{ 1.0f, 1.0f, 1.0f }
        };

        SUBCASE("Hits")
        {
            ray_t ray;
            vec2_t t;

            ray = { { 1.0f, 3.0f, 3.0f }, { 1.0f, 0.0f, 0.0f } };
            REQUIRE(bbox_ray_test(&bbox, &ray, &t));
            REQUIRE_EQ(t.x, Approx(1.0f));
            REQUIRE_EQ(t.y, Approx(3.0f));

            ray = { { 3.0f, 1.0f, 3.0f }, { 0.0f, 1.0f, 0.0f } };
            REQUIRE(bbox_ray_test(&bbox, &ray, &t));
            REQUIRE_EQ(t.x, Approx(1.0f));
            REQUIRE_EQ(t.y, Approx(3.0f));

            ray = { { 3.0f, 3.0f, 1.0f }, { 0.0f, 0.0f, 1.0f } };
            REQUIRE(bbox_ray_test(&bbox, &ray, &t));
            REQUIRE_EQ(t.x, Approx(1.0f));
            REQUIRE_EQ(t.y, Approx(3.0f));
        }

        SUBCASE("No hits")
        {
            ray_t ray;

            ray = { { 1.9f, 3.0f, 3.0f }, { 0.0f, 1.0f, 0.0f } };
            REQUIRE_FALSE(bbox_ray_test(&bbox, &ray, nullptr));
        }
    }

    SUBCASE("bbox 2")
    {
        const bbox_t bbox{
            { -0.5f, 0.0f, 0.0f },
            { 0.5f, 1.0f, 0.0f }
        };

        const ray_t ray{
            { -0.1f, 0.0f, -2.0f },
            { 0.0f, 0.0f, 10.0f }
        };

        vec2_t t;
        REQUIRE(bbox_ray_test(&bbox, &ray, &t));
    }
}
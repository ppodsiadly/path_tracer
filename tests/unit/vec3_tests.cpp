#include <doctest/doctest.h>
#include <vec_math.h>

using namespace doctest;

TEST_CASE("vec3 add/sub")
{
    const auto lhs = vec3_t{ 1.0f, -4.0f, 5.0f };
    const auto rhs = vec3_t{ -2.0f, 4.0f, -2.0f };

    SUBCASE("vec3_add")
    {
        const auto res = vec3_add(lhs, rhs);

        REQUIRE_EQ(res.x, -1.0f);
        REQUIRE_EQ(res.y, 0.0f);
        REQUIRE_EQ(res.z, 3.0f);
    }

    SUBCASE("vec3_sub")
    {
        const auto res = vec3_sub(lhs, rhs);

        REQUIRE_EQ(res.x, 3.0f);
        REQUIRE_EQ(res.y, -8.0f);
        REQUIRE_EQ(res.z, 7.0f);
    }
}

TEST_CASE("vec3_mul")
{
    const auto lhs = vec3_t{ 1.0f, -2.0f, 3.0f };

    const auto res = vec3_mul(lhs, 4.0f);

    REQUIRE_EQ(res.x, 4.0f);
    REQUIRE_EQ(res.y, -8.0f);
    REQUIRE_EQ(res.z, 12.0f);
}

TEST_CASE("vec3_length")
{
    const auto vec = vec3_t{ 2.0f, -3.0f, 1.0f };

    REQUIRE_EQ(vec3_length(vec), Approx(3.741657386773941f));

    REQUIRE_EQ(vec3_length(vec3_normalize(vec)), Approx(1.0f));
}

TEST_CASE("vec3_cross")
{
    const auto x = vec3_t{ 1.0f, 0.0f, 0.0f };
    const auto y = vec3_t{ 0.0f, 1.0f, 0.0f };

    const auto pos_z = vec3_cross(x, y);
    REQUIRE_EQ(pos_z.x, Approx(0.0f));
    REQUIRE_EQ(pos_z.y, Approx(0.0f));
    REQUIRE_EQ(pos_z.z, Approx(1.0f));

    const auto neg_z = vec3_cross(y, x);
    REQUIRE_EQ(neg_z.x, Approx(0.0f));
    REQUIRE_EQ(neg_z.y, Approx(0.0f));
    REQUIRE_EQ(neg_z.z, Approx(-1.0f));
}

TEST_CASE("vec3_reflect")
{
    const float sqrt_2 = 1.41421356237f;

    const auto v = vec3_t{ sqrt_2, -sqrt_2, 0.0f };
    const auto n = vec3_t{ 0.0f, 1.0f, 0.0 };

    const auto r = vec3_reflect(v, n);

    REQUIRE_EQ(r.x, Approx(sqrt_2));
    REQUIRE_EQ(r.y, Approx(sqrt_2));
    REQUIRE_EQ(r.z, Approx(0.0f));
}
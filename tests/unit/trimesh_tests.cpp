#include <doctest/doctest.h>
#include <trimesh.h>
#include <memory>

using namespace doctest;

namespace {

struct Deleter
{
    void operator ()(trimesh_t* tm) const
    {
        trimesh_release(tm);
    }
};

using guarded_trimesh_t = std::unique_ptr<trimesh_t, Deleter>;

}

TEST_CASE("Trimesh intersections")
{
    guarded_trimesh_t tm;
    tm.reset(trimesh_create());

    auto add_vertex = [&](const vec3_t& pos) {
        tm_vertex_t v;
        v.position = pos;
        v.normal = {};
        v.uv = {};
        trimesh_add_vertex(tm.get(), &v);
    };

    auto add_face = [&](uint32_t i0, uint32_t i1, uint32_t i2) {
        tm_face_t f{ { i0, i1, i2 } };
        trimesh_add_face(tm.get(), &f);
    };

    add_vertex({ -1.0f, -1.0f, 0.0f });
    add_vertex({ 0.0f, 1.0f, 0.0f });
    add_vertex({ 0.0f, -1.0f, 0.0f });
    add_face(0, 1, 2);

    add_vertex({ -1.0f, -1.0f, 2.0f });
    add_vertex({ 0.0f, 1.0f, 2.0f });
    add_vertex({ 0.0f, -1.0f, 2.0f });
    add_face(3, 4, 5);

    trimesh_compile(tm.get());

    SUBCASE("Hit, ray from -Z")
    {
        const auto ray = make_ray_s({ -1e-4f, 0.0f, -2.0f }, { 0.0f, 0.0f, 1.0f });

        REQUIRE(segment_trimesh_test(ray.origin, vec3_add(ray.origin, { 0.0f, 0.0f, 10.0f }), tm.get()));

        surf_point_t point;
        float t;
        REQUIRE(ray_trimesh_intersect(&ray, tm.get(), &point, &t));
        REQUIRE_EQ(point.position.x, Approx(-1e-4f));
        REQUIRE_EQ(point.position.y, Approx(0.0f));
        REQUIRE_EQ(point.position.z, Approx(0.0f));
    }

    SUBCASE("Hit, ray from -Z")
    {
        const auto ray = make_ray_s({ -1e-4f, 0.0f, -2.0f }, { 0.0f, 0.0f, 1.0f });

        REQUIRE(segment_trimesh_test(ray.origin, vec3_add(ray.origin, { 0.0f, 0.0f, 10.0f }), tm.get()));

        surf_point_t point;
        float t;
        REQUIRE(ray_trimesh_intersect(&ray, tm.get(), &point, &t));
        REQUIRE_EQ(point.position.x, Approx(-1e-4f));
        REQUIRE_EQ(point.position.y, Approx(0.0f));
        REQUIRE_EQ(point.position.z, Approx(0.0f));
    }

    SUBCASE("Hit, ray from +Z")
    {
        const auto ray = make_ray_s({ -0.1f, 0.0f, 3.0f }, { 0.0f, 0.0f, -1.0f });

        REQUIRE(segment_trimesh_test(ray.origin, vec3_add(ray.origin, { 0.0f, 0.0f, -10.0f }), tm.get()));

        surf_point_t point;
        float t;
        REQUIRE(ray_trimesh_intersect(&ray, tm.get(), &point, &t));
        REQUIRE_EQ(point.position.x, Approx(-0.1f));
        REQUIRE_EQ(point.position.y, Approx(0.0f));
        REQUIRE_EQ(point.position.z, Approx(2.0f));
    }

    SUBCASE("Not hit, ray towards +Z")
    {
        const auto ray = make_ray_s({ -0.1f, 0.0f, 3.0f }, { 0.0f, 0.0f, 1.0f });

        REQUIRE_FALSE(segment_trimesh_test(ray.origin, vec3_add(ray.origin, { 0.0f, 0.0f, 10.0f }), tm.get()));

        surf_point_t point;
        float t;
        REQUIRE_FALSE(ray_trimesh_intersect(&ray, tm.get(), &point, &t));
    }

    SUBCASE("Not hit, ray towards -Z")
    {
        const auto ray = make_ray_s({ -0.1f, 0.0f, -3.0f }, { 0.0f, 0.0f, -1.0f });

        REQUIRE_FALSE(segment_trimesh_test(ray.origin, vec3_add(ray.origin, { 0.0f, 0.0f, -10.0f }), tm.get()));

        surf_point_t point;
        float t;
        REQUIRE_FALSE(ray_trimesh_intersect(&ray, tm.get(), &point, &t));
    }

    SUBCASE("No hit, ray from +Z")
    {
        const auto ray = make_ray_s({ 0.1f, 0.0f, 3.0f }, { 0.0f, 0.0f, -1.0f });

        REQUIRE_FALSE(segment_trimesh_test(ray.origin, vec3_add(ray.origin, { 0.0f, 0.0f, -10.0f }), tm.get()));

        surf_point_t point;
        float t;
        REQUIRE_FALSE(ray_trimesh_intersect(&ray, tm.get(), &point, &t));
    }
}
#include <doctest/doctest.h>
#include <wdf_parser.h>
#include <tests_config.hpp>
#include <memory>
#include <string>

using namespace std::string_literals;

namespace {

struct Deleter
{
    void operator ()(wdf_node_t* n) const
    {
        wdf_free(n);
    }

    void operator ()(char* s) const
    {
        free(s);
    }
};

using guarded_wdf_node_t = std::unique_ptr<wdf_node_t, Deleter>;

using c_str_t = std::unique_ptr<char, Deleter>;

}

TEST_CASE("trim_string")
{
    const char* in_str = "  abc\t \n";
    c_str_t ts;

    SUBCASE("Empty string")
    {
        ts.reset(trim_string(in_str, 0));
        REQUIRE_EQ(ts, nullptr);
    }

    SUBCASE("Only whitespaces")
    {
        ts.reset(trim_string(in_str + 5, 3));
        REQUIRE_EQ(ts, nullptr);
    }

    SUBCASE("Whitespaces at the beginning")
    {
        ts.reset(trim_string("  abc", 5));
        REQUIRE_EQ(ts.get(), "abc"s);
    }

    SUBCASE("Whitespaces at the end")
    {
        ts.reset(trim_string(in_str + 2, 6));
        REQUIRE_EQ(ts.get(), "abc"s);
    }

    SUBCASE("Whitespaces at the beginning and end")
    {
        ts.reset(trim_string(in_str, 8));
        REQUIRE_EQ(ts.get(), "abc"s);
    }

    SUBCASE("Input includes nul terminator")
    {
        ts.reset(trim_string(in_str, 9));
        REQUIRE_EQ(ts.get(), "abc"s);
    }
}

TEST_CASE("parser_test.wdf")
{
    guarded_wdf_node_t tree;
    
    tree.reset(wdf_parse(TEST_DATA_FILE("parser_test.wdf")));
    REQUIRE_NE(tree, nullptr);

    REQUIRE_EQ(wdf_node_name(tree.get()), "root"s);
    REQUIRE_EQ(wdf_node_value(tree.get()), nullptr);
	REQUIRE_EQ(wdf_node_line_number(tree.get()), 1);
    REQUIRE_EQ(wdf_node_child_by_index(tree.get(), 2), nullptr);

    const wdf_node_t* n1 = wdf_node_child_by_index(tree.get(), 0);
    REQUIRE_NE(n1, nullptr);
    REQUIRE_EQ(wdf_node_name(n1), "node_1"s);
    REQUIRE_EQ(wdf_node_value(n1), "value_1"s);
	REQUIRE_EQ(wdf_node_line_number(n1), 2);

    const wdf_node_t* n2 = wdf_node_child_by_index(n1, 0);
    REQUIRE_NE(n2, nullptr);
    REQUIRE_EQ(wdf_node_name(n2), "node_2"s);
    REQUIRE_EQ(wdf_node_value(n2), nullptr);
	REQUIRE_EQ(wdf_node_line_number(n2), 3);
    REQUIRE_EQ(wdf_node_child_by_index(n2, 0), nullptr);

    const wdf_node_t* n3 = wdf_node_child_by_index(n1, 1);
    REQUIRE_NE(n3, nullptr);
    REQUIRE_EQ(wdf_node_name(n3), "node_3"s);
    REQUIRE_EQ(wdf_node_value(n3), nullptr);
	REQUIRE_EQ(wdf_node_line_number(n3), 4);
    REQUIRE_EQ(wdf_node_child_by_index(n3, 1), nullptr);

    const wdf_node_t* n4 = wdf_node_child_by_index(n3, 0);
    REQUIRE_NE(n4, nullptr);
    REQUIRE_EQ(wdf_node_name(n4), "node_4"s);
    REQUIRE_EQ(wdf_node_value(n4), "value_4"s);
	REQUIRE_EQ(wdf_node_line_number(n4), 5);

    const wdf_node_t* n5 = wdf_node_child_by_index(tree.get(), 1);
    REQUIRE_NE(n5, nullptr);
    REQUIRE_EQ(wdf_node_name(n5), "node_5"s);
    REQUIRE_EQ(wdf_node_value(n5), "value_5"s);
	REQUIRE_EQ(wdf_node_line_number(n5), 7);
    REQUIRE_EQ(wdf_node_child_by_index(n5, 0), nullptr);
}

TEST_CASE("wdf include node")
{
	const std::string including_file = TEST_DATA_FILE("include_test.wdf");
	const std::string included_file = TEST_DATA_FILE("include_test_included.wdf");

	guarded_wdf_node_t tree;
	tree.reset(wdf_parse(including_file.c_str()));

	REQUIRE_NE(tree.get(), nullptr);

	auto n1 = wdf_node_child_by_name(tree.get(), "node_1");
	REQUIRE_NE(n1, nullptr);
	REQUIRE_EQ(wdf_node_file_path(n1), including_file);
	REQUIRE_EQ(wdf_node_line_number(n1), 2);

	auto n2 = wdf_node_child_by_name(n1, "node_2");
	REQUIRE_NE(n2, nullptr);
	REQUIRE_EQ(wdf_node_file_path(n2), included_file);
	REQUIRE_EQ(wdf_node_line_number(n2), 1);

	auto n3 = wdf_node_child_by_name(n2, "node_3");
	REQUIRE_NE(n3, nullptr);
	REQUIRE_EQ(wdf_node_file_path(n3), included_file);
	REQUIRE_EQ(wdf_node_line_number(n3), 2);

	auto n4 = wdf_node_child_by_name(n1, "node_4");
	REQUIRE_NE(n4, nullptr);
	REQUIRE_EQ(wdf_node_file_path(n4), included_file);
	REQUIRE_EQ(wdf_node_line_number(n4), 3);

	n2 = wdf_node_child_by_name(tree.get(), "node_2");
	REQUIRE_NE(n2, nullptr);
	REQUIRE_EQ(wdf_node_file_path(n2), included_file);
	REQUIRE_EQ(wdf_node_line_number(n2), 1);
}
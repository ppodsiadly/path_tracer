#include <benchmark/benchmark.h>
#include <world.h>

namespace {

render_buf_t* pathtrace_lots_of_spheres(uint32_t w, uint32_t h, uint32_t rays_per_pixel)
{
    const uint32_t num_spheres_x = 32;
    const uint32_t num_spheres_z = 32;

    const uint32_t num_lights_x = 8;
    const uint32_t num_lights_z = 8;

    world_t* world = world_create();

    auto add_lambert_material = [&](const char* name, const color_t& color) {
        auto mtrl = material_create_lambertian(color);
        auto mc = world_get_material_cache(world);
        return material_cache_add(mc, name, mtrl);
    };

    auto sphere_mtrl = add_lambert_material("sphere", { 1.0f, 1.0f, 1.0f });

    for(uint32_t x = 0; x < num_spheres_x; ++x)
    {
        for(uint32_t z = 0; z < num_spheres_z; ++z)
        {
            mat4_t tform;
            mat4_from_translation(&tform, { x * 2.0f, 2.0f, z * 2.0f });

            world_add_sphere(world, 0.5f, sphere_mtrl, make_color(0.0f, 0.0f, 0.0f), &tform);
        }
    }

    for(uint32_t x = 0; x < num_lights_x; ++x)
    {
        for(uint32_t z = 0; z < num_lights_z; ++z)
        {
            world_add_point_light(world, { x * 4.0f, 5.0f, z * 4.0f }, { 0.2f, 0.2f, 0.2f });
        }
    }

    mat4_t plane_tform, plane_translation, plane_scale;
    mat4_from_scale(&plane_scale, { 256.0f, 256.0f, 256.0f });
    mat4_from_translation(&plane_translation, { 0.0f, -2.0f, 0.0f });
    mat4_multiply(&plane_tform, &plane_translation, &plane_scale);
    world_add_rectangle(world, add_lambert_material("plane", { 0.4f, 0.4f, 0.8f }), make_color(0.0f, 0.0f, 0.0f), &plane_tform);

    camera_t* camera = camera_create();
    camera_look_at(camera,
                   { 64.0f, 0.0f, 64.0f },
                   { 0.0f, 1.0f, 0.0f },
                   { -10.0f, 10.0f, -10.0f });

    auto render_buf = render_buf_create(w, h);

    prng_t prng = prng_init(1234);

    world_pathtrace(world, camera, &prng, rays_per_pixel, render_buf, nullptr);

    world_free(world);
    camera_free(camera);

    return render_buf;
}

void pathtrace_lots_of_spheres_test(benchmark::State& state)
{
    const auto max_bounces = static_cast<uint32_t>(state.range(0));

    for(auto _ : state)
    {
        auto rb = pathtrace_lots_of_spheres(64, 64, max_bounces);
        benchmark::DoNotOptimize(rb);
    }
}

}

BENCHMARK(pathtrace_lots_of_spheres_test)->Range(8, 32);

#include <benchmark/benchmark.h>
#include <world.h>
#include <camera.h>
#include <wdf_parser.h>
#include <tests_config.hpp>
#include <memory>

namespace {

struct Deleter
{
    void operator ()(camera_t* c) const { camera_free(c); }
    void operator ()(world_t* w) const { world_free(w); }
    void operator ()(render_buf_t* rb) const { render_buf_free(rb); }
};

template<typename T>
using guarded = std::unique_ptr<T, Deleter>;

struct scene_t
{
    guarded<world_t> world;
    guarded<camera_t> camera;
};

scene_t create_cornell_box()
{
    scene_t result;
    auto& world = result.world;
    auto& camera = result.camera;

    auto wdf_tree = wdf_parse(TEST_DATA_FILE("cornell_box+sphere.wdf"));
    world.reset(wdf_create_world(wdf_tree));
    camera.reset(wdf_create_camera(wdf_tree));
    wdf_free(wdf_tree);

    return result;
}

void test_pathtrace_openmp(benchmark::State& state)
{
    uint32_t rays_per_pixel = static_cast<uint32_t>(state.range(0));

    auto scene = create_cornell_box();

    for (auto _ : state)
    {
        guarded<render_buf_t> render_buf(render_buf_create(512, 512));

        prng_t prng = prng_init(1234567);
        world_pathtrace(scene.world.get(), scene.camera.get(), &prng, rays_per_pixel, render_buf.get(), nullptr);
    }
}

}

BENCHMARK(test_pathtrace_openmp)->Range(128, 2048);
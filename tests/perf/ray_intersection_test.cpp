#include <benchmark/benchmark.h>
#include <vec_math.h>
#include <random>

static float get_random_number(float min, float max)
{
    std::random_device rdev;
    std::mt19937 rgen(rdev());
    std::uniform_real_distribution<float> rdist(min, max);

    return rdist(rgen);
}

struct sphere_t
{
    vec3_t center;
    float radius;
};

static void test_ray_sphere_intersections(benchmark::State& state)
{
    ray_t ray;
    ray.origin = { 0.0f, 0.0f, 0.0f };
    ray.direction = { 1.0f, 0.0f, 0.0f };

    std::vector<sphere_t> spheres(static_cast<size_t>(state.range()));

    for(auto& sphere : spheres)
    {
        sphere.center = {
            get_random_number(0.0f, 1000.0f),
            get_random_number(-5.0f, 5.0f),
            get_random_number(-5.0f, 5.0f)
        };

        sphere.radius = get_random_number(1.0f, 5.0f);
    }

    for(auto _ : state)
    {
        for(const auto& sphere : spheres)
        {
            surf_point_t isect;
            float t;

            ray_t local_ray = ray;
            local_ray.origin = vec3_sub(ray.origin, sphere.center);

            benchmark::DoNotOptimize(ray_sphere_intersection(&local_ray, sphere.radius, &isect, &t));
            benchmark::DoNotOptimize(isect);
            benchmark::DoNotOptimize(t);
        }
    }
}

BENCHMARK(test_ray_sphere_intersections)->Range(100, 10000);

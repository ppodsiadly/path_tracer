#include <benchmark/benchmark.h>
#include <trimesh.h>
#include <tests_config.hpp>
#include <random>

namespace {

struct RandomRayGen
{
    std::mt19937 rgen;
    std::uniform_real_distribution<float> y_dist;
    std::uniform_real_distribution<float> z_dist;

    RandomRayGen()
        : rgen(std::random_device{}()),
          y_dist(-4.0f, 4.0f),
          z_dist(-4.0f, 4.0f)
    {}

    ray_t operator ()()
    {
        ray_t r;
        r.origin = { -5.0f, y_dist(rgen), z_dist(rgen) };
        r.direction = { 1.0f, 0.0f, 0.0f };
        return r;
    }
};

void trimesh_ray_intersections(const trimesh_t* tm, uint32_t num_rays)
{
    RandomRayGen ray_gen;

    for (uint32_t i = 0; i < num_rays; ++i)
    {
        const ray_t ray = ray_gen();

        surf_point_t point;
        float t;
        benchmark::DoNotOptimize(ray_trimesh_intersect(&ray, tm, &point, &t));
    }
}

void trimesh_segment_tests(const trimesh_t* tm, uint32_t num_rays)
{
    RandomRayGen ray_gen;

    for (uint32_t i = 0; i < num_rays; ++i)
    {
        const ray_t ray = ray_gen();

        const auto to = vec3_add(ray.origin, { 20.0f, 0.0f, 0.0f });
        benchmark::DoNotOptimize(segment_trimesh_test(ray.origin, to, tm));
    }
}

void test_trimesh_ray_intersections(benchmark::State& state)
{
    trimesh_t* tm = trimesh_load_obj(TEST_DATA_FILE("suzzy.obj"));

    for (auto _ : state)
    {
        trimesh_ray_intersections(tm, (uint32_t)state.range());
    }

    trimesh_release(tm);
}

void test_trimesh_segment_tests(benchmark::State& state)
{
    trimesh_t* tm = trimesh_load_obj(TEST_DATA_FILE("suzzy.obj"));

    for (auto _ : state)
    {
        trimesh_segment_tests(tm, (uint32_t)state.range());
    }

    trimesh_release(tm);
}

}

BENCHMARK(test_trimesh_ray_intersections)->Range(1000, 100000);

BENCHMARK(test_trimesh_segment_tests)->Range(1000, 100000);
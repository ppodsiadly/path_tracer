#include <ApprovalTests.hpp>
#include <doctest/doctest.h>
#include "world_fixture.hpp"
#include <tests_config.hpp>

using namespace ApprovalTests;

TEST_CASE_FIXTURE(WorldFixture, "XY plane")
{
    camera_look_at(camera,
                   /* look at */ { 0.0f, 0.0f, 0.0f},
                   /* up */ { 0.0f, 1.0f, 0.0f },
                   /* position */ { 0.0f, 0.0f, 10.0f });

    world_add_point_light(world, { 0.0f, 0.0f, 1.0f }, { 100.0f, 100.0f, 100.0f });

    auto mtrl = add_lambert_material("plane", { 1.0f, 0.0f, 0.0f });

    mat4_t tform;
    mat4_from_scale(&tform, { 20.0f, 20.0f, 20.0f });
    world_add_rectangle(world, mtrl, make_color(0.0f, 0.0f, 0.0f), &tform);

    auto image = do_pathtrace(64, 64);

    Approvals::verify(BmpImageWriter(image));
}

TEST_CASE_FIXTURE(WorldFixture, "XZ plane")
{
    camera_look_at(camera,
                   /* look at */ { 0.0f, 0.0f, 0.0f},
                   /* up */ { 0.0f, 0.0f, 1.0f },
                   /* position */ { 0.0f, 10.0f, 0.0f });

    world_add_point_light(world, { 0.0f, 1.0f, 0.0f }, { 100.0f, 100.0f, 100.0f });

    auto mtrl = add_lambert_material("plane", { 1.0f, 0.0f, 0.0f });

    mat4_t scale, rot, tform;
    mat4_from_scale(&scale, { 20.0f, 20.0f, 20.0f });
    mat4_from_axis_angle(&rot, { 1.0f, 0.0f, 0.0f }, deg_to_rad(90.0f));
    mat4_multiply(&tform, &rot, &scale);
    world_add_rectangle(world, mtrl, make_color(0.0f, 0.0f, 0.0f), &tform);

    auto image = do_pathtrace(64, 64);

    Approvals::verify(BmpImageWriter(image));
}

TEST_CASE_FIXTURE(WorldFixture, "YZ plane")
{
    camera_look_at(camera,
                   /* look at */ { 0.0f, 0.0f, 0.0f},
                   /* up */ { 0.0f, 1.0f, 0.0f },
                   /* position */ { 10.0f, 00.0f, 0.0f });

    world_add_point_light(world, { 1.0f, 0.0f, 0.0f }, { 100.0f, 100.0f, 100.0f });

    auto mtrl = add_lambert_material("plane", { 1.0f, 0.0f, 0.0f });

    mat4_t scale, rot, tform;
    mat4_from_scale(&scale, { 20.0f, 20.0f, 20.0f });
    mat4_from_axis_angle(&rot, { 0.0f, 1.0f, 0.0f }, deg_to_rad(90.0f));
    mat4_multiply(&tform, &rot, &scale);
    world_add_rectangle(world, mtrl, make_color(0.0f, 0.0f, 0.0f), &tform);

    auto image = do_pathtrace(64, 64);

    Approvals::verify(BmpImageWriter(image));
}
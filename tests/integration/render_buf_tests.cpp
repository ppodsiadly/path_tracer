#include "image_writer.hpp"
#include <doctest/doctest.h>
#include <render_buf.h>

using namespace ApprovalTests;

TEST_CASE("render_buf_to_image")
{
    const uint32_t w = 80;
    const uint32_t h = 50;

    render_buf_t* rb = render_buf_create(w, h);

    for(uint32_t y = 0; y < h; ++y)
    {
        for(uint32_t x = 0; x < w; ++x)
        {
            color_t c;
            c.r = static_cast<float>(x) / w;
            c.g = static_cast<float>(y) / h;
            c.b = 0.125f;

            render_buf_set_pixel(rb, x, y, c);
        }
    }

    guarded_image_t image(render_buf_to_image(rb));

    render_buf_free(rb);

    Approvals::verify(BmpImageWriter(image));
}
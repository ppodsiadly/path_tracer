#include <doctest/doctest.h>
#include "image_writer.hpp"
#include <image.h>
#include <memory>

using namespace ApprovalTests;

TEST_CASE("Writing image")
{
    SUBCASE("Red")
    {
        const uint32_t w = 61;
        const uint32_t h = 32;

        guarded_image_t image;
        image.reset(image_create(w, h));

        for(uint32_t y = 0; y < h; ++y)
        {
            for(uint32_t x = 0; x < w; ++x)
            {
                image_set_pixel(image.get(), x, y, make_u8_color(255, 0, 0));
            }
        }

        Approvals::verify(BmpImageWriter(image));
    }

    SUBCASE("Gradient")
    {
        const uint32_t w = 61;
        const uint32_t h = 32;

        guarded_image_t image;
        image.reset(image_create(w, h));

        for(uint32_t y = 0; y < h; ++y)
        {
            for(uint32_t x = 0; x < w; ++x)
            {
                image_set_pixel(image.get(), x, y,
                                { static_cast<uint8_t>(x * 256 / w),
                                  static_cast<uint8_t>(y * 256 / h),
                                  64 });
            }
        }

        Approvals::verify(BmpImageWriter(image));
    }
}
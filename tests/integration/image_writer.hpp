#ifndef IMAGE_WRITER_HPP_INCLUDED
#define IMAGE_WRITER_HPP_INCLUDED

#include <image.h>
#include <ApprovalTests.hpp>

struct ImageDeleter
{
    void operator ()(image_t* img) const
    {
        image_free(img);
    }
};

using guarded_image_t = std::unique_ptr<image_t, ImageDeleter>;

inline guarded_image_t make_image(uint32_t w, uint32_t h)
{
    return guarded_image_t(image_create(w, h));
}

struct BmpImageWriter : public ApprovalTests::ApprovalWriter
{
    const image_t* image = nullptr;

    explicit BmpImageWriter(const guarded_image_t& image)
        : image(image.get())
    {}

    std::string getFileExtensionWithDot() const override
    {
        return ".bmp";
    }

    void write(std::string path) const override
    {
        if(!bmp_write_image(path.c_str(), image))
        {
            throw std::runtime_error("Failed to write a BMP file to " + path);
        }
    }

    void cleanUpReceived(std::string receivedPath) const override
    {
        remove(receivedPath.c_str());
    }
};

#endif // IMAGE_WRITER_HPP_INCLUDED
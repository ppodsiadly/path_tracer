#ifndef WORLD_FIXTURE_HPP_INCLUDED
#define WORLD_FIXTURE_HPP_INCLUDED

#include <doctest/doctest.h>
#include <world.h>
#include <wdf_parser.h>
#include "image_writer.hpp"

struct WorldFixture
{
    world_t* world = nullptr;
    camera_t* camera = nullptr;

    WorldFixture()
    {
        world = world_create();
        camera = camera_create();

        camera_look_at(camera,
            { 0.0f, 0.0f, 0.0f },
            { 0.0f, 1.0f, 0.0f },
            { 0.0f, 0.0f, 5.0f });
    }

    ~WorldFixture()
    {
        world_free(world);
        camera_free(camera);
    }

    material_t* add_lambert_material(const char* name, const color_t& color)
    {
        return _add_material(name, material_create_lambertian(color));
    }

    material_t* add_fresnel_metal_material(const char* name)
    {
        return _add_material(name, material_create_fresnel_metal());
    }

	void world_from_wdf(const char* file_path)
	{
		using namespace std::string_literals;

		world_free(world);
		world = nullptr;

		camera_free(camera);
		camera = nullptr;

		wdf_node_t* world_desc = wdf_parse(file_path);
		if (!world_desc)
		{
			throw std::runtime_error("failed to load wdf from "s + file_path);
		}

		world = wdf_create_world(world_desc);
		if (!world)
		{
			throw std::runtime_error("failed to create world from wdf!");
		}

		camera = wdf_create_camera(world_desc);
		if (!camera)
		{
			throw std::runtime_error("failed to create camera from wdf!");
		}
	}

    guarded_image_t do_pathtrace(uint32_t img_w, uint32_t img_h, uint32_t rays_per_pixel = 256)
    {
        auto rb = render_buf_create(img_w, img_h);

        prng_t prng = prng_init(12345);
        world_pathtrace(world, camera, &prng, rays_per_pixel, rb, nullptr);

        guarded_image_t image(render_buf_to_image(rb));

        render_buf_free(rb);

        return image;
    }

private:
    material_t* _add_material(const char* name, material_t* mtrl)
    {
        auto mc = world_get_material_cache(world);
        auto result = material_cache_add(mc, name, mtrl);
        material_release(mtrl);
        return result;
    }
};

#endif // WORLD_FIXTURE_HPP_INCLUDED
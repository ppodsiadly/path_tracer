#include <ApprovalTests.hpp>
#include <doctest/doctest.h>
#include "world_fixture.hpp"
#include <tests_config.hpp>

using namespace ApprovalTests;

TEST_CASE_FIXTURE(WorldFixture, "Pathtracing: Empty world")
{
    auto image = do_pathtrace(64, 64);

    Approvals::verify(BmpImageWriter(image));
}

TEST_CASE_FIXTURE(WorldFixture, "Pathtracing: Single sphere")
{
	world_from_wdf(TEST_DATA_FILE("single_sphere.wdf"));
    
    auto image = do_pathtrace(64, 96);
    Approvals::verify(BmpImageWriter(image));
}

TEST_CASE_FIXTURE(WorldFixture, "Pathtracing: Cornell box")
{
	world_from_wdf(TEST_DATA_FILE("cornell_box+sphere.wdf"));

    auto image = do_pathtrace(128, 128, 32);
    Approvals::verify(BmpImageWriter(image));
}

TEST_CASE_FIXTURE(WorldFixture, "Pathtracing: Cornell box metal")
{
	world_from_wdf(TEST_DATA_FILE("cornell_box+metal_sphere.wdf"));

    auto image = do_pathtrace(128, 128, 32);
    Approvals::verify(BmpImageWriter(image));
}

TEST_CASE_FIXTURE(WorldFixture, "Pathtracing: Cornell box with emissive sphere" * doctest::should_fail())
{
	world_from_wdf(TEST_DATA_FILE("cornell_box+emissive_sphere.wdf"));

    auto image = do_pathtrace(128, 128, 32);
    Approvals::verify(BmpImageWriter(image));
}
#include <doctest/doctest.h>
#include <ApprovalTests.hpp>
#include "world_fixture.hpp"
#include <trimesh.h>
#include <tests_config.hpp>

using namespace ApprovalTests;

namespace {

struct Deleter
{
    void operator ()(trimesh_t* tm) const
    {
        trimesh_release(tm);
    }
};

using guarded_trimesh_t = std::unique_ptr<trimesh_t, Deleter>;

}

TEST_CASE_FIXTURE(WorldFixture, "simple.obj")
{
	world_from_wdf(TEST_DATA_FILE("cornell_box+simple.obj.wdf"));

    auto image = do_pathtrace(128, 128, 8);
    Approvals::verify(BmpImageWriter(image));
}

TEST_CASE_FIXTURE(WorldFixture, "suzzy.obj")
{
	world_from_wdf(TEST_DATA_FILE("suzzy.wdf"));

    auto image = do_pathtrace(128, 128, 8);
    Approvals::verify(BmpImageWriter(image));
}

TEST_CASE_FIXTURE(WorldFixture, "two_tris.obj")
{
	world_from_wdf(TEST_DATA_FILE("cornell_box+two_tris.obj.wdf"));

    auto image = do_pathtrace(128, 128, 8);
    Approvals::verify(BmpImageWriter(image));
}